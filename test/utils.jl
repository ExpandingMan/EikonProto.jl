using Colors
using KittyTerminalImages
using ImageIO, FileIO


macro usingall(pkg)
    nq = Expr(:$, :name)
    o = quote
        using $pkg
        for name ∈ names($pkg, all=true)
            name ∈ (:eval, :include) && continue
            startswith(string(name), "#") && continue
            @eval using $pkg: $nq
        end
    end
    esc(o)
end


function imview(img::AbstractMatrix{<:Colorant}, fname::AbstractString=tempname()*".png")
    save(fname, convert.(RGB, img))
    run(`imv $fname`)
    #TODO: if we don't wait it deletes before imv can load it
    rm(fname)
end
