using StaticArrays, LinearAlgebra
using DifferentialEquations, CommonSolve, SciMLBase
using ModelingToolkit
using Infiltrator
using Plots

using EikonSolving; const ES = EikonSolving
using EikonSolving.HamiltonianDiffEq
using EikonSolving: SchwarzschildPolar, SchwarzschildIsotropic
using EikonSolving: System, Solution
using EikonSolving: mass², mass²s
using EikonSolving: MinkowskiCartesian

#====================================================================================================
    Reference: G*M_sol ≈ 1500 m      
    1 AU ≈ 10^8 G*M_sol

#TODO: think need to work out some parameters for basic orbits and make plots before
#moving onto surrogate
====================================================================================================#



src1() = quote
    M = SchwarzschildPolar(1.0, 1.0)

    sys = System(M)

    (x0, p0) = ES.initcircular(M, 100.0)

    sol = Solution(sys, x0, p0, (0.0, 200.0))
end

src2() = quote
    M = SchwarzschildIsotropic(1.0, 1.0)

    sys = System(M)

    x0 = Float64[10.0, 0.0, 0.0, 0.0]
    p0 = Float64[0.0, 1.0, 0.0, -1.0]

    sol = Solution(sys, x0, p0, (0.0, 10.0))
end

src3() = quote
    M = MinkowskiCartesian()

    sys = System(M)

    x0 = Float64[0, 0, 0, 0]
    p0 = Float64[1, 0, 0, -1]

    sol = Solution(sys, x0, p0, (0.0, 10.0))
end
