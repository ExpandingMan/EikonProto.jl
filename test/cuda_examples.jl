using Transducers, LinearAlgebra, CUDA, StaticArrays
using BenchmarkTools

using Base.Threads: @threads

#====================================================================================================
Here I collect some examples for figuring out the parallelization model of EikonProto

## Example 1
Speedup of *at least* a factor of 4!  Using the kernel is about 1/3 faster for reasons
I don't understand

TODO: what is CUDA.@sync and where might we need to use it?
====================================================================================================#


function kernel1(A, x, b)
    j = threadIdx().x
    b[j] = A[j]*x[j]
    nothing
end

function cpukernel1(A, x, b)
    @threads for j ∈ 1:length(A)
        b[j] = A[j]*x[j]        
    end
    nothing
end

function example1_gpu()
    N = 10*1024

    A = 1:N |> Map(j -> SMatrix{2,2}(randn(Float32,2,2))) |> collect |> cu
    x = 1:N |> Map(j -> SVector{2}(randn(Float32,2))) |> collect |> cu

    b = cu(Vector{SVector{2,Float32}}(undef, N))

    #@benchmark @cuda kernel1($A, $x, $b)
    @benchmark $b .= $A .* $x
end

function example1_cpu()
    N = 10*1024

    A = 1:N |> Map(j -> SMatrix{2,2}(randn(Float32,2,2))) |> collect
    x = 1:N |> Map(j -> SVector{2}(randn(Float32,2))) |> collect

    b = Vector{SVector{2,Float32}}(undef, N)

    @benchmark cpukernel1($A, $x, $b)
end
