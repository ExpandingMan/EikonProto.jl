using EikonProto
using LinearAlgebra, Transducers, CUDA, StaticArrays, Colors, LorentzGroup
using ForwardDiff
using Infiltrator, Cthulhu
using BenchmarkTools

includet("utils.jl")

ENV["JULIA_DEBUG"] = "EikonProto"


@usingall EikonProto


src1() = quote
    p = MCPoint(Float32[0,0,0,0])
    q = MCPoint(Float32[2,0,0,0])

    u = MCVector(q, fourvelfromcoordvel(Float32[-0.5,0,0]))
    𝒻 = EikonProto.comoving(u)

    p′ = 𝒻(p)
    q′ = 𝒻(q)

    a = At(p, Float32[0,0,0,1], Float32[0,0,1,0])
end

#TODO: *carefully* work out the redshift pattern to confirm this is working properly


src2() = quote
    p = MCPoint(Float32[0,0,0,0])
    v = MCVector(p, Float32[-1,0,0,1])
    
    q = MCPoint(Float32[1.3,0,0,0])
    u = MCVector(q, fourvelfromcoordvel(Float32[0,0,0]))
    l = InertMCWorldline(u)
    #Σ = SphereSurface(l, 0.3f0)
    Σ = Box(l, (0.5f0,0.5f0,0.5f0), RotZ{Float32}(-π/6)*RotX{Float32}(π/6)*RotY{Float32}(-π/6), 0.05f0)

    m = AnisotropicEmitter(CBlackBodySpectrum(20.0f0, 0.3f0))
    s = Solid(Σ, m)

    #TODO: make it easier to move image
    dat = load(joinpath(homedir(),"Pictures","gaia_equirectangular_1.png"))
    #dat = load(joinpath(homedir(),"Pictures","se_equirect_local_1.png"))
    #dat = [fill(RGB(0,0,0), 512, 512) fill(RGB(0,1,0), 512, 512)
    #       fill(RGB(0,0,1), 512, 512) fill(RGB(1,1,1), 512, 512)
    #      ]
    smp = SphereEquirectangularSampler(dat, amplitude=2.0f0)
    inf = MinkowskiSphereInfinity(MCVector(p, Float32[0,0,0,1]), smp)
    #inf = BlackInfinity{Minkowski4}()
    S = MinkowskiSpacetime([s], inf)

    uc = MCVector(p, fourvelfromcoordvel(Float32[0.2,0,0.5]))
    cb = with(uc) do uc
        #At(p, Float32[0,0,0,1], Float32[0,0,-1,0], Float32[-1,0,0,0], Float32[0,-1,0,0])
        At(p, Float32[0,0,0,1], Float32[1,0,0,0], Float32[0,1,0,0], Float32[0,0,1,0])
    end[]
    c = IdealPinholeCam(S, cb, 0.05f0,
                        amplification=5*10.0f0^11,
                        exposure_time=0.01f0,
                        xpixels=2048,
                        ypixels=2048,
                        nevaluations=1,
                       )

    rt = SimpleRaytracer(S)
    rd = DefaultRenderer(S, rt)

    img = expose(rd, c)
end

src3() = quote
    B = CBlackBodySpectrum(20.0f0)
    S = ShiftedSpectrum(B, 0.5f0)

    cB = XYZ(B)
    cS = XYZ(S, 0.2)

    S = XYZSpectrum(RGB(0,1,0))
    S′ = ShiftedSpectrum(S, 1.2f0)

    cS′ = XYZ(S′)
    println(cS′)
    cS′
end


