#====================================================================================================
       makietools.jl

Tools for viewing objects from EikonProto.jl with Makie.  This gives us a simple way of
verifying they look ok without relying on the rendering of EikonProto.jl itself.
====================================================================================================#
module MakieTools
using GLMakie
using Transducers

using EikonProto: Manifold, Euclidean
using EikonProto: CartData, CartVecData, CartVecNormData
using EikonProto: Point, EuclidPoint
using EikonProto: Vector, EuclidVector
using EikonProto: distance, signeddistance, normal
using EikonProto: paralleltransport
using EikonProto: Surface, Sphere, Box
using EikonProto: SimpleDiffuseReflector


const DEFAULT_XRANGE = range(-2.0, 2.0, step=0.01)
const DEFAULT_YRANGE = range(-2.0, 2.0, step=0.01)


function _makecontourgrid(Σ::Surface, z::Real=0; xrange=DEFAULT_XRANGE, yrange=DEFAULT_YRANGE)
    ps = [EuclidPoint(CartVecData(x, y, z)) for x ∈ xrange, y ∈ yrange]
    map(p -> signeddistance(Σ, p), ps)
end

function contourplot(Σ::Surface, z::Real=0; xrange=DEFAULT_XRANGE, yrange=DEFAULT_YRANGE)
    fig = Figure()
    ax = Axis(fig[1,1])
    zs = _makecontourgrid(Σ, z; xrange, yrange)
    contour!(xrange, yrange, zs)
    fig
end



end

