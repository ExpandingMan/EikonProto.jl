# EikonProto

[![dev](https://img.shields.io/badge/docs-latest-blue?style=for-the-badge&logo=julia)](https://ExpandingMan.gitlab.io/EikonProto.jl/)
[![build](https://img.shields.io/gitlab/pipeline/ExpandingMan/EikonProto.jl/main?style=for-the-badge)](https://gitlab.com/ExpandingMan/EikonProto.jl/-/pipelines)

A very early prototype (hence the `Proto`) for a geometric optics simulator (physically based ray-trace renderer).

## Design Goals
- Interface using the language of differential geometry.
- Interface that supports arbitrary $(3,0)$ signature metrics.  (Though in this prototype I'll
    probably only do Euclidean.)
- It is not my goal to ever have performance competitive with mature renderers, but I would like the
    performance to be reasonable.  This implies a significant amount of work will go into figuring
    out how to parallelize, particularly with CUDA.

## References
Standard ray-tracing techniques are inappropriate for curved spacetime, so we use [ray
marching](https://en.wikipedia.org/wiki/Ray_marching).  The work of Inigo Quilez
(https://iquilezles.org/) is therefore an
invaluable resource.
- [Differential Geometry in
    Physics](https://uncpress.org/book/9781469669250/differential-geometry-in-physics/)
- [The Geometry of
    Physics](https://www.cambridge.org/core/books/geometry-of-physics/94894F70DB22055BD7BC2B84C135ABAF)
- [Modern Classical
    Physics](https://press.princeton.edu/books/hardcover/9780691159027/modern-classical-physics)
- [Physically Based Rendering](https://www.pbr-book.org/)
- [Distance functions](https://iquilezles.org/articles/distfunctions/).
- [Atlas of Coordinate Charts on de Sitter Spacetime](https://arxiv.org/abs/1211.2363)
- [de Sitter Geodesics](https://arxiv.org/abs/1711.02956)
