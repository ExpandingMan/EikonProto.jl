
struct NormArray1{N,T<:Number} <: StaticArray{Tuple{N},T,1}
    magnitude::T
    unit::SVector{N,T}

    NormArray1{N,T}(a::Checked{<:Number}, v::Checked{<:AbstractVector{<:Number}}) where {N,T} = new{N,T}(a[], v[])
end

function NormArray1{N,T}(data::Tuple) where {N,T}
    v = SVector{N,T}(data)
    a = norm(v)
    u = (a ≈ zero(a)) ? SVector{N,T}((one(T),zero(T),zero(T))) : normalize(v)
    NormArray1{N,T}(Checked(a), Checked(u))
end

# this errors for int but it's supposed to
function NormArray1(data::NTuple{N}) where {N}
    data = promote(data...)
    NormArray1{N,eltype(data)}(data)
end

NormArray1(v::StaticVector{N,T}) where {N,T} = NormArray1{N,T}(Tuple(v))  

NormArray1(v::AbstractVector) = NormArray1{length(v),eltype(v)}(v)

NormArray1(v::NormArray1) = v

StaticArrays.Size(::Type{<:NormArray1{N,T}}) where {N,T} = Size(N)

Base.size(v::NormArray1) = size(v.unit)

Base.IndexStyle(::Type{<:NormArray1}) = InddexLinear()

Base.@propagate_inbounds function Base.getindex(v::NormArray1, j::Int) 
    @boundscheck checkbounds(v, j)
    @inbounds v.magnitude * v.unit[j]
end

Base.:(-)(v::NormArray1) = NormArray1(v.magnitude, -v.unit)

#TODO: scaling will probably hit a shitload of method ambiguities

LinearAlgebra.norm(v::NormArray1) = v.magnitude
LinearAlgebra.normalize(v::NormArray1{N,T}) where {N,T} = NormArray1{N,T}(Checked(one(eltype(v))), Checked(v.unit))
