
abstract type Surface{M<:Manifold} <: ManifoldSubset{M} end

abstract type PrimitiveSurface{M<:Manifold} <: Surface{M} end

# for now this basically means that a spacelike slice is convex
isconvex(Σ::Surface) = isconvex(typeof(Σ))

# these don't *have* to apply, but are good defaults
for f ∈ (:frameofdistance, :invframeofdistance, :framewithinvofdistance, :velocity)
    @eval $f(Σ::Surface{M}, p::Point{M}) where {M} = $f(Worldline(Σ), p)
end

# fallback that may not get called
function sdistance(Σ::Surface{M}, p::Point{M}) where {M}
    with(frameofdistance(Σ, p), p) do p
        sdistancecomoving(Σ, p)
    end
end


# note that textures and shit like that will break spherical symmetry
struct SphereSurface{M<:Manifold,L<:Worldline{M},T<:Number} <: PrimitiveSurface{M}
    position::L
    radius::T
end

Worldline(Σ::SphereSurface) = Σ.position

SphereSurface(l::Worldline{M}, R::Real=1.0f0) where {M<:Manifold} = SphereSurface{M,typeof(l),typeof(R)}(l, R)
SphereSurface(p::Point, R::Real=1.0f0) = SphereSurface(InertCoordWorldline(p), R)

isconvex(::Type{<:SphereSurface}) = true

# the below looks kind of magical because the symmetry of the sphere let's use use distance(p, q)
# this frees us from having to do the boost because distance(p, q) == distance(p′, q′)
"""
    sdistance(Σ::Surface, q::Point)

Evaluate the signed distance function ``\\sigma`` for surface ``\\Sigma`` at point ``q``.  For points
outside the surface, this is defined as the spacelike geodesic distance from the point to the surface.

The gradient of this function at the surface is the spacelike surface normal.
"""
function sdistance(Σ::SphereSurface{M}, q::Point{M}) where {M<:Manifold}
    s = timeofdistance(Σ.position, q)
    p = Σ.position(s)
    distance(p, q) - Σ.radius
end


struct Box{M<:Minkowski{4},L<:InertialWorldline{Minkowski{4}},R<:Lorentz,T<:Number} <: PrimitiveSurface{M}
    position::L   # center
    dimensions::SVector{3,T}  # for now this is in coordinate rest frame
    rotation::R
    rounding_radius::T
end

Worldline(Σ::Box) = Σ.position

function Box(l::InertialWorldline{Minkowski{4}}, (a, b, c)::Tuple,
             R::Lorentz=one(Lorentz),
             r::Real=0.0f0)
    (a, b, c) = promote(a, b, c)
    T = typeof(a)
    dims = @SVector T[a, b, c]
    Box{Minkowski{4},typeof(l),typeof(R),typeof(a)}(l, dims, R, r)
end

isconvex(::Type{<:Box}) = true

"""
    sdistancecomoving(Σ::Surface, p::Point)

Compute the [`sdistance`](@ref) function with `p` guaranteed to be in comoving coordinates of the surface,
as defined by the surface's `Worldline`.  This function is *guaranteed* not to use coordinate dependent
data from the surface itself, so that e.g.
```julia
sdistancecomoving(Σ, 𝒻(p)) ≈ sdistancecomoving(𝒻(Σ), 𝒻(p))
```
where `𝒻` is the `Diffeomorphism` into the comoving coordinates.
"""
function sdistancecomoving(Σ::Box, q::MCPoint)
    # right now the way we have done this, `q` is guaranteed to be centered
    q = inv(Σ.rotation)*data(q)
    let b = Σ.dimensions, r = Σ.rounding_radius
        # note that broadcasting is dangerous and can cause allocations
        δ = @SVector [abs(q[j]) - b[j]/2 for j ∈ 1:3]
        norm(max.(δ, zero(eltype(δ)))) + min(max(δ[1],max(δ[2],δ[3])), zero(Float)) - r
    end
end


normal(::Type{OneForm}, Σ::Surface{M}, q::Point{M}) where {M} = ∇(r -> sdistance(Σ, r), q)

"""
    normal([OneForm], Σ::Surface{M}, p::AbstractPoint)

Vector field of normals to the surface `Σ` in manifold `M`, defined as the gradient of the
signed distance function (SDF).  As such, it is defined everywhere on ``\\mathcal{M}``, but
coincides with the usual notion of surface normal on ``\\Sigma``.

If the type argument `OneForm` is passed, the one-form dual to the normal will be returned instead.
"""
normal(Σ::Surface{M}, q::Point{M}) where {M} = 𝐠inv * normal(OneForm, Σ, q)

