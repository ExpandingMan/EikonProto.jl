
"""
    hav(θ)

The "haversine" function ``sin^2(\\theta/2)``.
"""
hav(θ) = sin(θ/2)^2

"""
    ahav(θ)

The inverse of the "haversine" function ``sin^2(\\theta/2)``.
"""
ahav(x) = acos(1 - 2*x)

randunit(::Type{T}) where {T} = randunit(Random.GLOBAL_RNG, T)
randnat(::Type{V}, p::Point) where {V<:Vector} = randnat(Random.GLOBAL_RNG, V, p)
randunitat(::Type{V}, p::Point) where {V<:Vector} = randunitat(Random.GLOBAL_RNG, V, p)
randunithemisphere(::Type{V}, u::Vector{M}) where {M<:Manifold,V<:Vector{M}} = randunithemisphere(Random.GLOBAL_RNG, V, u)
randunithemisphere(u::Vector) = randunithemisphere(Random.GLOBAL_RNG, u)

randunit(rng::AbstractRNG, ::Type{SVector{N,T}}) where {N,T} = normalize(randn(rng, SVector{N,T}))
