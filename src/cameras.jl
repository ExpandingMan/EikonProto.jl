
abstract type Camera{S<:Spacetime} end


# we have this because we often need these together, including before const of cam
# all of this is rest frame of course
struct IdealPinholeCamGeom{T<:Number}
    length::T
    aperture::T
    dimensions::NTuple{2,T}  # of imager
    npixels::NTuple{2,Int}
    pixeldims::NTuple{2,T}
    xrange::LinRange{T,Int}
    yrange::LinRange{T,Int}
end

function IdealPinholeCamGeom(length::Number, aperture::Number,
                             (xdim, ydim)::NTuple{2,<:Number},
                             (nx, ny)::NTuple{2,<:Integer},
                            )
    @reals length aperture xdim ydim
    T = typeof(length)
    pdims = (xdim/nx, ydim/ny)
    xrange = range(-xdim/2, xdim/2, length=nx)
    yrange = range(-ydim/2, ydim/2, length=ny)
    IdealPinholeCamGeom{T}(length, aperture, (xdim, ydim), (nx, ny), pdims, xrange, yrange)
end

pixelarea(geo::IdealPinholeCamGeom) = prod(geo.pixeldims)


# having a camera at a single point will be super useful for debugging
# *DO NOT* change it, create another camera if it bothers you so much
"""
    IdealPinholeCam <: Camera

An ideal pinhole camera.  Geometrically, the aperture size is zero, but the `aperture` parameter is
used as a regularization factor to get non-zero illumination on the imager.

The camera exists entirely on its worldline and has zero volume.  The focal length is used merely
to compute the angular separation of its pixel vectors.  The imager should therefore be considered
"virtual", meaning that the pixel vectors all exist at the same point.
"""
mutable struct IdealPinholeCam{S,W<:Worldline,V<:Vector,T<:Number} <: Camera{S}
    spacetime::S
    worldline::W  # of the pinhole
    time::T

    geometry::IdealPinholeCamGeom{T}

    # geometry
    pinhole::V
    x̂::V
    ŷ::V

    pixels::Matrix{V}  # already inverted
   
    amplification::Float
    exposure_time::Float  # physical
    nevaluations::Int  # implementation detail, how many times do we construct an image

    # this is essentially a camera geometry coefficient for its color output...
    # it's needed often so we compute it up front
    # at some point, when we do dynamic range, this will have to get a lot more sophisticated
    α::T
end

solidangle(geo::IdealPinholeCamGeom{T}) where {T} = T(4*π) * (geo.aperture/geo.length)^2 

camgeocoeff(geo::IdealPinholeCamGeom) = pixelarea(geo)*solidangle(geo)

function IdealPinholeCam(s::Spacetime{M}, l::Worldline{M}, geo::IdealPinholeCamGeom{T},
                         nxy::At{M,3}, t₀::Number;
                         amplification::Real=Float(10^5),
                         exposure_time::Real=Float(0.01),
                         nevaluations::Integer=1,
                        ) where {M,T}
    (n, x̂, ŷ) = nxy
    pixs = Matrix{typeof(n)}(undef, length(geo.yrange), length(geo.xrange))
    α = amplification*(exposure_time/nevaluations)*camgeocoeff(geo)
    C = IdealPinholeCam{typeof(s),typeof(l),typeof(n),T}
    c = C(s, l, t₀, geo, n, x̂, ŷ, pixs, amplification, exposure_time, nevaluations, α)
    settime!(c)
end

function IdealPinholeCam(s::Spacetime{M}, l::Worldline{M}, t₀::Number, nxy::At{M,3}, ℓ::Number;
                         xdim::Number=2*ℓ, ydim::Number=2*ℓ,
                         xpixels::Integer=1024, ypixels::Integer=1024,
                         aperture::Number=ℓ/10,
                         kw...
                        ) where {M}
    geo = IdealPinholeCamGeom(ℓ, aperture, (xdim, ydim), (xpixels, ypixels))
    IdealPinholeCam(s, l, geo, nxy, t₀; kw...)
end
function IdealPinholeCam(s::Spacetime{M}, unxy::At{M,4}, ℓ::Number; kw...) where {M}
    (nxy, u) = popfirst!!(unxy)
    IdealPinholeCam(s, InertialWorldline(u), zero(dataeltype(Point(u))), nxy, ℓ; kw...)
end

Spacetime(c::IdealPinholeCam) = c.spacetime

camgeocoeff(c::IdealPinholeCam) = camgeocoeff(c.geometry)

computecoeff!(c::IdealPinholeCam) = c.amplification*(c.exposure_time/c.nevaluations)*camgeocoeff(c)

Point(c::IdealPinholeCam) = Point(c.pinhole)
Worldline(c::IdealPinholeCam) = c.worldline

velocity(c::IdealPinholeCam) = tangent(Worldline(c), c.time)
velocity(c::IdealPinholeCam{M}, p::Point{M}) where {M} = velocity(Worldline(c), p)

pixelarea(c::IdealPinholeCam) = pixelarea(c.geometry)

# despite looking super fucking magical, the below should actually work in *any* frame
# (as long as you set the vectors correctly!) because you should be able to just boost
# this linear combination, even though coefficients look coordinate dependent
# note that these are *already* inverted
function pixels!(c::IdealPinholeCam)
    u = velocity(c)
    n = -c.pinhole
    x̂ = c.x̂
    ŷ = c.ŷ
    ℓ = c.geometry.length
    for idx ∈ CartesianIndices((length(c.geometry.yrange), length(c.geometry.xrange)))
        δy = c.geometry.yrange[idx[1]]
        δx = c.geometry.xrange[idx[2]]
        δ = √(δx^2 + δy^2)
        h = √(δ^2 + ℓ^2)
        cosϕ = ℓ/h
        sinϕ = δ/h
        c.pixels[idx] = u + cosϕ*n + sinϕ*((δx/δ)*x̂ + (δy/δ)*ŷ)
    end
    c
end

function settime!(c::IdealPinholeCam, t::Real=zero(Float))
    c.pinhole = paralleltransport(c.pinhole, c.worldline, c.time, t)
    c.x̂ = paralleltransport(c.x̂, c.worldline, c.time, t)
    c.ŷ = paralleltransport(c.ŷ, c.worldline, c.time, t)
    c.time = t
    pixels!(c)
end

elapsetime!(c::IdealPinholeCam, δ::Real) = settime!(c, c.time + δ)

getpixel(c::IdealPinholeCam, idx::CartesianIndex) = c.pixels[idx]

pixelradiance(rd::Renderer, c::IdealPinholeCam, idx::CartesianIndex) = c.α*radiance(rd, getpixel(c, idx))

pixelcolor(rd::Renderer, c::IdealPinholeCam, idx::CartesianIndex) = c.α*color(rd, getpixel(c, idx))

function evalexpose!(img::AbstractMatrix{C}, rd::Renderer, c::IdealPinholeCam) where {C<:Colorant}
    @threads for idx ∈ CartesianIndices(img)
        img[idx] = convert(C, pixelcolor(rd, c, idx))
    end
    img
end

function expose!(img::AbstractMatrix{<:Colorant}, rd::Renderer, c::IdealPinholeCam)
    for k ∈ 1:c.nevaluations
        evalexpose!(img, rd, c)
        elapsetime!(c, c.exposure_time/c.nevaluations)
    end
    img
end

function expose(::Type{C}, rd::Renderer, c::IdealPinholeCam) where {C<:Colorant}
    img = zeros(C, size(c.pixels)...)
    expose!(img, rd, c)
end
expose(rd::Renderer, c::IdealPinholeCam) = expose(colortype(rd), rd, c)

