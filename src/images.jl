
struct Indexer
    nx::Int
    ny::Int
    x₀::Float
    x₁::Float
    y₀::Float
    y₁::Float
end

Indexer((nx, ny), (x₀, x₁), (y₀, y₁)) = Indexer(nx, ny, x₀, x₁, y₀, y₁)

function _axisindex(n::Integer, flip::Bool, a::Real, b::Real, x::Real)
    ζ = abs(x - a)/abs(b - a)
    min(floor(Int, n*(flip ? one(ζ) - ζ : ζ))+1, n)
end

xaxisindex(idxer::Indexer, x::Real) = _axisindex(idxer.nx, idxer.x₁ < idxer.x₀, idxer.x₀, idxer.x₁, x)
yaxisindex(idxer::Indexer, y::Real) = _axisindex(idxer.ny, idxer.y₁ > idxer.y₀, idxer.y₀, idxer.y₁, y)

Base.CartesianIndex(idxer::Indexer, x::Real, y::Real) = CartesianIndex(yaxisindex(idxer, y), xaxisindex(idxer, x))


struct ColorspaceImage{A<:AbstractMatrix{<:XYZ}}
    data::A
    indexer::Indexer
end

Indexer(img::ColorspaceImage) = img.indexer
Base.CartesianIndices(img::ColorspaceImage) = CartesianIndices(img.data)

function ColorspaceImage(img::AbstractMatrix{<:Colorant}, (x₀, x₁), (y₀, y₁); standardize::Bool=false)
    img = convert.(XYZ, img)
    idxer = Indexer(reverse(size(img)), (x₀, x₁), (y₀, y₁))
    #standardize && (img = standardize_image(img))
    ColorspaceImage{typeof(img)}(img, idxer)
end

Base.getindex(img::ColorspaceImage, idx::CartesianIndex) = img.data[idx]

# we still bounds check by index not coords because otherwise very unsafe
pixel(img::ColorspaceImage, x::Real, y::Real) = img[CartesianIndex(img.indexer, x, y)]


abstract type Image end

EikonProto.radiance(img::Image, x::Real, y::Real, r::Number) = emittedradiance(img, x, y, r)


#WARN: this might be inverted but I'm too lazy to check right now


struct XYZImage{I<:ColorspaceImage,T<:Number} <: Image
    image::I
    amplitude::T
end

Indexer(img::XYZImage) = Indexer(img.image)

#TODO: good default amplitude
function XYZImage(img::ColorspaceImage; amplitude::Number=Float(1))
    XYZImage{typeof(img),typeof(amplitude)}(img, amplitude)
end

function EikonProto.emittedradiance(img::XYZImage, x::Real, y::Real, r::Number)
    c = convert(XYZ, pixel(img.image, x, y))
    redshift(XYZSpectrum(c, img.amplitude), r)
end
