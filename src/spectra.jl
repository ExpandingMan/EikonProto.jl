
abstract type Spectrum end


struct ZeroSpectrum{T<:Number} <: Spectrum end

ZeroSpectrum() = ZeroSpectrum{Float}()

(S::ZeroSpectrum{T})(ω::Number) where {T} = zero(T)

Base.:(*)(a::Number, s::ZeroSpectrum) = s

(::Type{C})(S::ZeroSpectrum{T}) where {T,C<:LinColor} = zero(C)


struct ShiftedSpectrum{S<:Spectrum,T<:Number} <: Spectrum
    spectrum::S
    redshift::T
end

(S::ShiftedSpectrum)(ω::Number) = S.spectrum(S.redshift*ω)

Base.:(*)(a::Number, s::ShiftedSpectrum) = ShiftedSpectrum(a*s.spectrum, s.redshift)

redshift(s::Spectrum, r::Number) = ShiftedSpectrum(s, r)

# this occurs so frequently it's nice to have a function
blueshift(s::Spectrum, r::Number) = redshift(s, inv(r))

redshift(S::ZeroSpectrum, r::Number) = S


plancklaw(ω::Real, T::Real) = ω^3 * (4*π^3)^(-1) * (exp(ω/T) - 1)^(-1)

abstract type BlackBodySpectrum <: Spectrum end

# this is simple version where we don't try to memoize the whole fucking universe
# what's the C for? concrete maybe?
struct CBlackBodySpectrum{S<:Number} <: BlackBodySpectrum
    T::S  # ceV
    scale::S
end

function CBlackBodySpectrum(T::Number, s::Number=one(T))
    (T, s) = promote(float(T), float(s))
    CBlackBodySpectrum{typeof(T)}(T, s)
end

Base.:(*)(a::Number, S::CBlackBodySpectrum) = CBlackBodySpectrum(S.T, a*S.scale)

temperature(B::CBlackBodySpectrum) = B.T

(B::CBlackBodySpectrum)(ω::Number) = plancklaw(ω, B.T)


#TODO: double check all this shit! (but looks ok so far)
# note that these are invalid below 14.365 ceV or above 215.425 ceV,
# but we'll continue without warning because why not
function _blackbody_xyY_x(T::Number)
    f = 8.617/T
    if T < 34.468
        -0.2661239*f^3 - 0.2343589*f^2 + 0.8776956*f + 0.17331
    else
        -3.025849*f^3 + 2.1070379*f^2 + 0.2226347*f + 0.24039
    end
end
function _blackbody_xyY_y(T::Number, x::Number=_blackbody_xyY_y(T))
    if T < 19.147
        -1.1063814*x^3 - 1.3481102*x^2 + 2.18555832*x - 0.20219683
    elseif T < 34.468
        -0.9549476*x^3 - 1.37418593*x^2 + 2.09137015*x - 0.16748867
    else
        3.0817580*x^3 - 5.87338670*x^2 + 3.75112997*x - 0.37001483
    end
end

#TODO: not too happy with how normalization works here... it's fine by itself but is it ok when we
#compare to spectrum of other stuff that might be implemented differently?

function ColorTypes.xyY(B::CBlackBodySpectrum, Y::Number=B.scale)
    x = _blackbody_xyY_x(B.T)
    y = _blackbody_xyY_y(B.T, x)
    xyY(x, y, Y)
end

function ColorTypes.xyY(S::ShiftedSpectrum{<:BlackBodySpectrum}, Y::Number=S.spectrum.scale)
    T = temperature(S.spectrum)/S.redshift
    x = _blackbody_xyY_x(T)
    y = _blackbody_xyY_y(T, x)
    xyY(x, y, Y/S.redshift^3)
end

(::Type{C})(B::CBlackBodySpectrum) where {C<:LinColor} = convert(C, xyY(B))
(::Type{C})(S::ShiftedSpectrum{<:BlackBodySpectrum}) where {C<:LinColor} = convert(C, xyY(S))


const HBARC = 19730.0  # ceV nm

# see https://en.wikipedia.org/wiki/CIE_1931_color_space
# all the below are in 1/ceV

const COLORMATCH_X_MUS = Float.((599.8, 442.0, 501.1) ./ HBARC)
const COLORMATCH_X_SIGMAS = Float.((37.9, 31.0, 16.0, 26.7, 20.4, 26.2) ./ HBARC)
const COLORMATCH_X_COEFFS = Float.((1.056, 0.362, -0.065))

const COLORMATCH_Y_MUS = Float.((568.8, 530.9) ./ HBARC)
const COLORMATCH_Y_SIGMAS = Float.((46.9, 40.5, 16.3, 31.1) ./ HBARC)
const COLORMATCH_Y_COEFFS = Float.((0.821, 0.286))

const COLORMATCH_Z_MUS = Float.((437.0, 459.0) ./ HBARC)
const COLORMATCH_Z_SIGMAS = Float.((11.8, 36.0, 26.0, 13.8) ./ HBARC)
const COLORMATCH_Z_COEFFS = Float.((1.217, 0.681))

function crookedgaussian(μ::Number, σ1::Number, σ2::Number, x::Number)
    if x < μ
        exp(-(x - μ)^2/(2*σ1^2))
    else
        exp(-(x - μ)^2/(2*σ2^2))
    end
end

# need this because of promotion annoyance
function _under2pi(ω::Number)
    ω = float(ω)
    T = typeof(ω)
    T(2*π)/ω
end

function colormatchwavelengthx(λ::Number)
    let (μ1, μ2, μ3) = COLORMATCH_X_MUS,
        (σ1, σ2, σ3, σ4, σ5, σ6) = COLORMATCH_X_SIGMAS,
        (c1, c2, c3) = COLORMATCH_X_COEFFS
        o = c1*crookedgaussian(μ1, σ1, σ2, λ) + c2*crookedgaussian(μ2, σ3, σ4, λ) + c3*crookedgaussian(μ3, σ5, σ6, λ)
        #o/Float(42.39825)
    end
end
colormatchx(ω::Number) = colormatchwavelengthx(_under2pi(ω))

function colormatchwavelengthy(λ::Number)
    let (μ1, μ2) = COLORMATCH_Y_MUS
        (σ1, σ2, σ3, σ4) = COLORMATCH_Y_SIGMAS
        (c1, c2) = COLORMATCH_Y_COEFFS
        o = c1*crookedgaussian(μ1, σ1, σ2, λ) + c2*crookedgaussian(μ2, σ2, σ3, λ)
        #o/Float(45.529377)
    end
end
colormatchy(ω::Number) = colormatchwavelengthy(_under2pi(ω))

function colormatchwavelengthz(λ::Number)
    let (μ1, μ2) = COLORMATCH_Z_MUS
        (σ1, σ2, σ3, σ4) = COLORMATCH_Z_SIGMAS
        (c1, c2) = COLORMATCH_Z_COEFFS
        o = c1*crookedgaussian(μ1, σ1, σ2, λ) + c2*crookedgaussian(μ2, σ2, σ3, λ)
        #o/Float(76.59515)
    end
end
colormatchz(ω::Number) = colormatchwavelengthz(_under2pi(ω))

colormatch(c::XYZ, ω::Number) = c.x*colormatchx(ω) + c.y*colormatchy(ω) + c.z*colormatchz(ω)


struct XYZSpectrum{T<:Number,I<:Integrator{1}} <: Spectrum
    color::XYZ{T}
    scale::T
    int::I  # only used when shifted

    function XYZSpectrum(c::Colorant, s::Number=Float(1),
                         # this seems sufficient in most cases
                         int::Integrator{1}=SimpleRiemannInt(Float(100),Float(400),32),
                        )
        s = float(s)
        c = convert(XYZ, c)
        T = promote_type(eltype(c), typeof(s))
        new{T,typeof(int)}(c, convert(T, s), int)
    end
end

Base.:(*)(a::Number, S::XYZSpectrum) = XYZSpectrum(S.color, a*S.scale, S.int)

(S::XYZSpectrum)(ω::Number) = S.scale*colormatch(S.color, ω)

# we do scaling twice hear because one is physical, one is for color conversion
(::Type{C})(S::XYZSpectrum) where {C<:LinColor} = convert(C, S.scale*S.color)

#WARN: still confused about normalization of this stuff

function ColorTypes.XYZ(S::ShiftedSpectrum{<:XYZSpectrum{T}}) where {T}
    int = S.spectrum.int
    c = S.spectrum.color
    r = S.redshift
    a = S.spectrum.scale
    x = T(∫(ω -> a*colormatchx(ω)*colormatch(c, r*ω), int))/Float(42.39825)
    y = T(∫(ω -> a*colormatchy(ω)*colormatch(c, r*ω), int))/Float(45.529377)
    z = T(∫(ω -> a*colormatchz(ω)*colormatch(c, r*ω), int))/Float(76.59515)
    XYZ{T}(x, y, z)
end

(::Type{C})(S::ShiftedSpectrum{<:XYZSpectrum{T}}) where {T,C<:LinColor} = convert(C, XYZ(S))
