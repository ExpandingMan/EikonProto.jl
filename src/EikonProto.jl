module EikonProto

using LinearAlgebra, Statistics, Random
using StaticArrays
using ForwardDiff, ReverseDiff
using CUDA
using Transducers, Transducers.BangBang
using Colors

import LorentzGroup
using LorentzGroup: Lorentz, LorMatrix, RotX, RotY, RotZ, BoostX, BoostY, BoostZ
using LorentzGroup: minkowski

using Infiltrator

using Base: RefValue

using Base.Threads

const Float = Float32

const AbstractArray1{T} = Base.AbstractVector{T}
const Array1{T} = Base.Vector{T}

const (∞) = Float(Inf32)

# linear colors which form a "half vector space". there may be others
const LinColor = Union{XYZ,LMS}

#UNITS: are ceV everywhere, this is good for floating point in visible part of spectrum

reals(args::Number...) = promote(map(float, args)...)

macro reals(args...)
    lhs = Expr(:tuple, args...)
    rhs = Expr(:call, :reals, args...)
    quote
        $lhs = $rhs
    end |> esc
end

#TODO: need null, timelike, spacelike vector wrappers instead of this for most stuff
"""
    Checked{T}

Type signifying that the wrapped value has been checked for validity.
`Checked` objects can be unwrapped with `getindex`, i.e. `cv[]`.

Note that `Checked` is interpreted as confirmation that *all* values are
valid to an arbitrary nesting level.  For example, passing a `Checked`
`AbstractVector` for polar coordinate data indicates not only that
the `AbstractVector` is of the appropriate length, but that all
the polar coordinate data it contains is valid.
"""
struct Checked{T}
    value::T
end

Base.getindex(cv::Checked) = cv.value


abstract type Manifold{n} end

abstract type Riemannian{n} <: Manifold{n} end

abstract type Lorentzian{n} <: Manifold{n} end


struct Euclidean{n} <: Riemannian{n} end
struct Sphere{n} <: Riemannian{n} end

struct Minkowski{n} <: Lorentzian{n} end
struct deSitter{α} <: Lorentzian{4} end

const Minkowski4 = Minkowski{4}

ndimensions(::Type{<:Manifold{n}}) where {n} = n
ndimensions(M::Manifold) = ndimensions(typeof(M))


abstract type MetricSignature{n} end
struct RiemannianSignature{n,T<:Number} <: MetricSignature{n} end
struct LorentzianSignature{n,T<:Number} <: MetricSignature{n} end

# inner product for orthonormal cases. Tensor interface extends this in a way that makes sense
(::RiemannianSignature)(u::AbstractArray1, v::AbstractArray1) = u⋅v

function (::LorentzianSignature{n})(u::AbstractArray1, v::AbstractArray1) where {n}
    -u[end]*v[end] + sum(ntuple(j -> u[j]*v[j], Val(n-1)))
end

MetricSignature(::Type{T}, M::Manifold) where {T} = MetricSignature(T, typeof(M))

MetricSignature(::Type{T}, ::Type{<:Riemannian{n}}) where {n,T} = RiemannianSignature{n,T}()
MetricSignature(::Type{T}, ::Type{<:Lorentzian{n}}) where {n,T} = LorentzianSignature{n,T}()

function data(::RiemannianSignature{n,T}) where {n,T}
    SMatrix{n,n,T}(ntuple(j -> T(mod1(j, n+1) == 1), Val(n^2)))
end

function data(::LorentzianSignature{n,T}) where {n,T}
    t = ntuple(Val(n*n)) do j
        j == n^2 ? -one(T) : T(mod1(j, n+1) == 1)
    end
    SMatrix{n,n,T}(t)
end

"""
    Character

Abstract type for the singleton types `Timelike`, `Spacelike` and `Null`, the sole values of
which are `timelike`, `spacelike` and `null` respectively.  These are used as arguments to
functions where appropriate, particularly in the context of the ADM 3+1 formalism.
"""
abstract type Character end
struct Spacelike <: Character end
const spacelike = Spacelike()

struct Timelike <: Character end
const timelike = Timelike()

struct Null <: Character end
const null = Null()


abstract type ManifoldSubset{M<:Manifold} end


"""
    datatype(x)

Returns the type used to store data of a tensor or a point.  The value is to be considered an
implementation detail, and the behavior of objects should not depend on the returned type.
"""
datatype(x) = datatype(typeof(x))

"""
    dataeltype(x)

Returns the element type of the type used to store data of a tensor or point.  The returned value
is to be considered an implementation detail, the behavior of objects should not depend on this type.
"""
dataeltype(x) = eltype(datatype(x))


include("arrays.jl")
include("integration.jl")
include("points.jl")

include("tensors/tensors.jl")
include("tensors/minkowski.jl")

include("diffeomorphisms.jl")
include("worldlines.jl")
include("surfaces.jl")
include("spectra.jl")
include("materials.jl")

include("images.jl")
include("samplers.jl")

include("spacetime.jl")
include("renderer.jl")
include("cameras.jl")
include("utils.jl")


end
