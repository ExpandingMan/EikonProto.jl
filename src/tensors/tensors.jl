
"""
    Tensor{M<:Manifold,r,s}

A tensor of the tangent space of a point ``p`` on the manifold ``M``.

All subtypes of `Tensor` are required to hold a [`Point`](@ref) object referencing the point to which they are tangent
which can be accessed via `Point(v::Tensor)`.

These tensors do *not* satisfy the `Base.AbstractArray` interface, but they must hold their data as an
`Base.AbstractArray`, this is accessible with [`data`](@ref) and is guaranteed to be a `StaticArray` with
`size(data(v)) == (r,s)`.  The tensor data needn't be stored permanently in the `Tensor` object and may
be computed on a call to `data`.

Tensor data requires specification of a vector basis to be useful.  This basis may either be holonomic or
orthonormal.  The basis type of a `Tensor` object is given by [`BasisType`](@ref).

**WARNING** While all tensor math functions check that coordinates and bases match between tensors, they
do *not* normally check if they are on the tangent space! This is an unfortunate consequence of this being
a much more expensive check that must be done at runtime.

All tensors should have, at minimum, a constructor
```julia
TensorType(p::Point{M}, data::AbstractArray)
```
where `data` is tensor data in the representation described by `BasisType(TensorType)`.
"""
abstract type Tensor{M<:Manifold,r,s} end

abstract type Metric{M<:Manifold} <: Tensor{M,0,2} end

abstract type InverseMetric{M<:Manifold} <: Tensor{M,2,0} end

const Vector{M} = Tensor{M,1,0}
const OneForm{M} = Tensor{M,0,1}

Manifold(u::Tensor{M}) where {M} = M()

Point(u::Tensor) = u.point
data(u::Tensor) = u.data

datatype(u::Tensor) = typeof(data(u))

"""
    constructortype(u::Tensor)

Returns a type used as a constructor for tensors with the same [`Representation`](@ref) as `u`.
By default, this is merely `typeof(u)`, but tensors are free to define alternatives as long as they
are of the same `Representation`.
"""
constructortype(::Type{T}) where {T<:Tensor} = T
constructortype(u::Tensor) = constructortype(typeof(u))

defaulttype(::Type{T}, p::Point{M}) where {M,T<:Tensor{M}} = defaulttype(T, typeof(p))

Base.:(+)(u::Tensor) = u

struct InvalidTensorComparison{U,V} <: Exception
    u::U
    v::V
end

function Base.showerror(io::IO, err::InvalidTensorComparison)
    println(io, "InvalidTensorComparison: executing code tried to compare tensors on different tangent spaces")
    print(io, "1st tensor point:\t")
    show(io, Point(err.u))
    println(io)
    print(io, "2nd tensor point:\t")
    show(io, Point(err.v))
end

function tangent_space_check(u::Tensor{M}, v::Tensor{M}; kw...) where {M}
    # this debug log is ok because we should never do it in performant code anyway
    @debug("DEBUG WARNING - tangent space check is being computed")
    if !(≈)(Point(u), Point(v); kw...)
        throw(InvalidTensorComparison(u, v))
    end
end
function tangent_space_check(u::Tensor{M}...; kw...) where {M}
    for j ∈ 2:length(u)
        tangent_space_check(u[1], u[j]; kw...)
    end
end

normal(::Timelike, p::Point) = normal(timelike, defaulttype(Vector, p), p)

projector(c::Character, p::Point) = projector(c, defaulttype(Vector, p), p)

projector(::Timelike, ::Type{T}, p::Point{M}) where {M,T<:Vector{M}} = Projector(normal(timelike, T, p))

projector(::Spacelike, ::Type{T}, p::Point{M}) where {M,T<:Vector{M}} = ComplimentProjector(normal(timelike, T, p))



abstract type BasisType end  # of the representation in the data
struct Orthonormal <: BasisType end
struct Holonomic <: BasisType end


"""
    Representation{M<:Manifold,B<:BasisType}

An abstract type used as a trait by [`Tensor`](@ref) indicating their representation.  This definition is
an API guarantee, rather than a strict mathemetical definition.  Tensors with the same `Representation`
are *not* required to be in the same coordinates, but tensors with the same `Representation`, the same tangent
space *and* the same coordinates must be compatible in the following sense:
- Addition between tensors is equivalent to addition between their data.
- All tensor operations to scalars are equal to equivalent operations on their data with the appropriate metric.

Thus, this trait is to indicate compatibility between tensors that share an API but not necessarily
implementation.
"""
abstract type Representation{M<:Manifold,B<:BasisType} end

Representation(x::Tensor) = Representation(typeof(x))

# this is essentially a check that types match
Representation(::R, ::R) where {R<:Representation} = R()
Representation(x::Tensor, y::Tensor) = Representation(Representation(x), Representation(x))

BasisType(::Representation{M,B}) where {M,B} = B()
BasisType(::Type{T}) where {T<:Tensor} = BasisType(Representation(T))
BasisType(x::Tensor) = BasisType(typeof(x))

Base.:(*)(a::Number, u::V) where {V<:Tensor} = V(Point(u), a*data(u))

add(::Representation{M}, u::Tensor{M}, v::Tensor{M}) where {M} = typeof(u)(Point(u), data(u) + data(v))

Base.:(+)(x::Tensor, y::Tensor) = add(Representation(x, y), x, y)

Base.:(-)(x::Tensor) = typeof(x)(Point(x), -data(x))

subtract(r::Representation{M}, u::Tensor{M}, v::Tensor{M}) where {M} = typeof(u)(Point(u), data(u) - data(v))

Base.:(-)(x::Tensor, y::Tensor) = subtract(Representation(x, y), x, y)

mult(::Representation{M}, u::OneForm{M}, v::Vector{M}) where {M} = InverseMetric(Point(u))(𝐠*u, v)
mult(::Representation{M}, u::Vector{M}, v::OneForm{M}) where {M} = Metric(Point(u))(u, 𝐠inv*v)

Base.:(*)(x::Tensor, y::Tensor) = mult(Representation(x, y), x, y)

function inner(::Representation{M,<:Orthonormal}, u::Vector{M}, v::Vector{M}) where {M} 
    MetricSignature(dataeltype(u), M)(data(u), data(v))
end
inner(::Representation{M,<:Holonomic}, u::Vector{M}, v::Vector{M}) where {M} = Metric(Point(u))(u, v)

function inner(::Representation{M,<:Orthonormal}, u::OneForm{M}, v::OneForm{M}) where {M} 
    MetricSignature(dataeltype(u), M)(data(u)', data(v)')
end
inner(::Representation{M,<:Holonomic}, u::OneForm{M}, v::OneForm{M}) where {M} = InverseMetric(Point(u))(u, v)

LinearAlgebra.:(⋅)(u::Vector, v::Vector) = inner(Representation(u, v), u, v)

"""
    exp(v::Vector, s::Number=1.0)

Compute the exponential map of tangent vector `v`, returning a point on the manifold
with the same coordinates system as `v`.  The real parameter `s` extends the definition
of the exponential map to arbitrary points along the geodesic to which `v` is tangent.
Therefore, ``\\exp(v_p, t)`` returns the point in the direction of ``v`` at a proper distance
``s`` if ``v`` is normalized.
"""
Base.exp(u::Vector{M}, s::Number=one(Float)) where {M} = exponential(Representation(u), u, s)

"""
    geodesic(u::Vector, t::Number)
    geodesic(u::Vector)

Returns the geodesic curve to which ``u`` is tangent as a real function returning `Point`.  The type of
the returned point will deend on the type of `u`.  If the argument ``t`` is omitted, the function
returns the geodesic as a function.
"""
geodesic(R::Representation{M}, u::Vector{M}, s::Number) where {M} = exponential(Representation(u), u, s)
geodesic(u::Vector, s::Number) = geodesic(Representation(u), u, s)
geodesic(u::Vector) = s::Number -> geodesic(u, s)

"""
    paralleltransport(u::Vector, v::Vector, t::Number=1)
    paralleltransport(u::Vector, t::Number=1)

Parallel transport tangent vector ``u`` along the geodesic to which ``v`` is tangent a proper distance ``t``.
Returns a vector ``v_q`` at point ``q = \\exp(v_p, t)`` which is parallel to ``v_p``.
If the second vector argument is omitted, the vector is parallel transported along itself.

Note that ``v`` is *not* normalized i.e. longer vectors are transported further.
See [`normedparalleltransport`](@ref)
"""
function paralleltransport(u::Vector{M}, v::Vector{M}, s::Number=one(Float)) where {M} 
    paralleltransport(Representation(u, v), u, v, s)
end
paralleltransport(u::Vector, t::Number=one(Float)) = paralleltransport(u, u, t)

"""
    normedparalleltransport(u::Vector, v::Vector=u, t::Number=1)

Parallel transport vector ``u`` along the geodesic to which ``v`` is tangent a proper distance ``t`` after
normalizing ``v``.  Note that this will go very badly wrong if ``v`` is null.
"""
function normedparalleltransport(u::Vector{M}, v::Vector{M}, t::Number=one(Float)) where {M}
    paralleltransport(u, normalize(v), t)
end
normedparalleltransport(u::Vector, t::Number=one(Float)) = normedparalleltransport(u, u, t)

BasisType(::Type{T1}, ::Type{T2}) where {T1<:Tensor,T2<:Tensor} = BasisType(BasisType(T1), BasisType(T2))
BasisType(u::Tensor{M}, v::Tensor{M}) where {M<:Manifold} = BasisType(typeof(u), typeof(v))


struct Identity{M<:Manifold,R<:Representation{M},P<:Point{M}} <: Tensor{M,1,1}
    point::P
end

Representation(::Identity{M,R}) where {M,R} = R()

mult(::Representation, ::Identity{M}, v::Vector{M}) where {M} = v
mult(::Representation, v::OneForm{M}, ::Identity{M}) where {M} = v


struct InvMetric{M,G<:Metric{M}} <: InverseMetric{M}
    metric::G
end

Representation(::Type{InvMetric{M,G}}) where {M,G} = Representation(G)

Point(g::InvMetric) = Point(g.metric)

Base.inv(g::Metric) = InvMetric(g)

mult(::Representation, ginv::InvMetric{M}, g::Metric{M}) where {M} = Identity(Point(g))

# square is separate from inner in case we want more efficient method
Base.@constprop :aggressive function Base.:(^)(u::Vector, n::Integer)
    n == 2 || throw(ArgumentError("vectors only support squaring, not being raised to other powers"))
    square(u)
end

"""
    Character(u::Vector)

Returns whether `u` is `null`, `spacelike` or `timelike`.  This requires run-time checking of vector
data for most vector implementations, so it can introduce type instability.
"""
function Character(u::Vector)
    u2 = u^2
    if u2 ≈ zero(dataeltype(u))
        null
    elseif u2 > 0
        spacelike
    else
        timelike
    end
end

# obviously this silently blows up for null vectors
LinearAlgebra.normalize(u::Vector) = (one(dataeltype(u))/√(abs(u⋅u)))*u


"""
    AutoMetric (𝐠)

This object allows for simple shorthand notation, especially for raising and lowering indices.
See also [`InvAutoMetric`](@ref).
"""
struct AutoMetric end
const 𝐠 = AutoMetric()

"""
    InvAutoMetric (𝐠inv)

This object allows for simple shorthand notation, especially for raising and lowering indcies.
See also [`AutoMetric`](@ref).
"""
struct InvAutoMetric end
const 𝐠inv = InvAutoMetric()

Base.:(*)(::AutoMetric, u::Vector) = Metric(Point(u))*u
Base.:(*)(u::Vector, ::AutoMetric) = 𝐠*u
Base.:(*)(::InvAutoMetric, u::OneForm) = InverseMetric(Point(u))*u
Base.:(*)(::OneForm, ::InvAutoMetric) = 𝐠inv*u



"""
    normalizeproj(::Character, u::Vector)

Normalize either the timelike or spacelike projection of a vector by re-scaling the vector (i.e.
the resulting vector is parallel to ``u``), where the timelike and spacelike projections are defined
by the coordinate system of the representation of `u`.  This operation is obviously badly
coordinate dependent, but it's occasionally useful in computations.
"""
normalizeproj(c::Union{Timelike,Spacelike}, u::Vector) = normalizeproj_with(c, u)[2]

function normalizeproj_with(c::Union{Timelike,Spacelike}, u::Vector)
    l = norm(c, u)
    (l, (1/l)*u)
end

# again, be careful with this because it's coordinate dependent
function LinearAlgebra.norm(c::Character, u::Vector)
    v = project(c, u)
    √(abs(v^2))
end

function equals(::Representation{M}, u::Tensor{M}, v::Tensor{M}) where {M}
    Point(u) == Point(v) || return false
    data(u) == data(v)
end

function Base.:(==)(u::Tensor, v::Tensor)
    R = Representation(u)
    R == Representation(v) || return false
    equals(R, u, v)
end

function approx(::Representation{M}, u::Tensor{M}, v::Tensor{M}; kw...) where {M}
    (≈)(Point(u), Point(v); kw...) || return false
    (≈)(data(u), data(v); kw...)
end

function Base.:(≈)(u::T, v::T; kw...) where {T<:Tensor}
    R = Representation(u)
    R == Representation(v) || return false
    approx(u, v; kw...)
end

function randnat(rng::AbstractRNG, ::Type{V}, ::Type{DT}, p::P) where {M,DT<:AbstractArray1,V<:Vector{M},P<:Point{M}}
    V(p, randn(rng, DT))
end


"""
    Projector <: Tensor{M,1,1}

A tensor that projects vectors onto a vector.  The left action of these tensors on vectors via `*`
is defined as the projection (note that we use `*` here because this operation corresponds to ordinary
matrix multiplication).  This object is defined with tensor rank ``(1,1)``.
"""
struct Projector{M,V<:Vector{M}} <: Tensor{M,1,1}
    vector::V
end

Projector(v::Vector{M}) where {M<:Manifold} = Projector{M,typeof(v)}(v)

Point(P::Projector) = Point(P.vector)

Representation(::Type{Projector{M,V}}) where {M,V} = Representation(V)

#TODO: defining datatype is non-trivial because it doesn't store the matrix
# for now we cheat and only define the dataeltype
dataeltype(::Type{Projector{M,V}}) where {M,V} = dataeltype(V)
dataeltype(P::Projector) = dataeltype(typeof(P))

function data(P::Projector) 
    let v = P.vector
        data(v) .* data(v)'
    end
end

function mult(::Representation{M}, P::Projector{M}, u::Vector{M}) where {M}
    let v = P.vector
        (u⋅v)*v
    end
end


struct ComplimentProjector{M,V<:Vector{M}} <: Tensor{M,1,1}
    vector::V
end

ComplimentProjector(v::Vector{M}) where {M<:Manifold} = ComplimentProjector{M,typeof(v)}(v)

Point(P::ComplimentProjector) = Point(P.vector)

Representation(::Type{ComplimentProjector{M,V}}) where {M,V} = Representation(V)

dataeltype(::Type{ComplimentProjector{M,V}}) where {M,V} = dataeltype(V)
dataeltype(P::ComplimentProjector) = dataeltype(typeof(P))

function data(P::ComplimentProjector)
    let v = P.vector, n = ndimensions(Manifold(P)), T = dataeltype(P)
        one(SMatrix{n,n,T}) - data(v) .* data(v)'
    end
end

function mult(::Representation{M}, P::ComplimentProjector{M}, u::Vector{M}) where {M}
    let v = P.vector, n = ndimensions(Manifold(P)), T = dataeltype(P)
        u - abs(u⋅v)*v
    end
end


"""
    project(timelike, u::Tensor)
    project(spacelike, u::Tensor)

Project the tensor ``u`` onto only its timelike or spacelike components, as defined by the coordinate basis
of the reprsentation of the point (given by `Point(u)`).

This is equivalent to `projector(character, coords, Point(u))*u)` but concrete tensor types may implement more
efficient methods.
"""
project(c::Character, u::Vector) = projector(c, typeof(u), Point(u))*u


_at(p::Point, td::Pair{Type{T},A}) where {T,A} = constructortype(T)(p, td[2])
_at(p::Point, td::Pair{DataType,A}) where {A} = constructortype(td[1])(p, td[2])
_at(p::Point{M}, data::AbstractArray1) where {M} = defaulttype(Vector{M}, p)(p, data)

"""
    At

This is a wrapper for tuples of tensors which are guaranteed to be at the same point.
"""
struct At{M,N,T<:Tuple}
    tensors::T
end

function At(p::Point{M}, args...) where {M} 
    tpl = map(a -> _at(p, a), args)
    At{M,length(args),typeof(tpl)}(tpl)
end

function BangBang.popfirst!!(a::At{M,N,T}) where {M,N,T} 
    t = Base.tail(a.tensors)
    (At{M,N-1,typeof(t)}(t), a.tensors[1])
end

Point(a::At) = Point(a.tensors[1])

Base.length(a::At) = length(a.tensors)
Base.iterate(a::At, s::Integer=1) = iterate(a.tensors, s)

Base.getindex(a::At, k::Integer) = a.tensors[k]

# this is private because it can easily move one of the tensors
function _map(𝒻, a::At{M}) where {M}
    tpl = map(𝒻, a.tensors)
    At{M,length(tpl),typeof(tpl)}(tpl)
end

Base.getindex(a::At) = _map(getindex, a)
