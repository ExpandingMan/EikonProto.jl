
struct MinkowskiCartesian{n} <: Representation{Minkowski{n},Orthonormal} end

defaultrep(::Type{<:MCPoint{n}}) where {n} = MinkowskiCartesian{n}()

# MC for Minkowski Cartesian
struct MCVector{n,P<:Point{Minkowski{n}},D<:StaticVector{n,<:Number}} <: Tensor{Minkowski{n},1,0}
    point::P
    data::D
end

constructortype(::Type{<:MCVector{n}}) where {n} = MCVector{n}

defaulttype(::Type{Vector{M}}, ::Type{<:MCPoint{n}}) where {M,n} = MCVector{n}

function MCVector{n}(p::Point{Minkowski{n}}, data::StaticVector{n,<:Number}) where {n}
    MCVector{n,typeof(p),typeof(data)}(p, data)
end

function MCVector{n}(p::Point{Minkowski{n}}, data::AbstractVector{<:Number}) where {n}
    MCVector{n}(p, convert(SVector{n,eltype(data)}, data))
end
MCVector(p::Point{Minkowski{n}}, data::AbstractVector{<:Number}) where {n} = MCVector{n}(p, data)

MCVector(v::MCVector) = v

datatype(::Type{MCVector{n,P,D}}) where {n,P,D} = D

function data(::Spacelike, v::MCVector{n}) where {n}
    T = dataeltype(v)
    SVector{n-1,T}(ntuple(j -> v.data[j], n-1))
end

function data(::Timelike, v::MCVector)
    T = dataeltype(v)
    SVector{1,T}((v.data[end],))
end

component(::Timelike, v::MCVector) = v.data[end]

Representation(::Type{<:MCVector{n}}) where {n} = MinkowskiCartesian{n}()

mult(::MinkowskiCartesian{n}, g::Metric{Minkowski{n}}, v::Vector{n}) where {n} = MCOneForm(Point(v), data(v)')

function square(v::MCVector)
    vx = data(spacelike, v)
    vt = component(timelike, v)
    -vt^2 + vx⋅vx
end

function project(::Timelike, u::MCVector{n}) where {n}
    T = dataeltype(u)
    MCVector(Point(u), vcat(@SVector(zeros(T, n-1)), data(timelike, u)))
end
function project(::Spacelike, u::MCVector{n}) where {n}
    T = dataeltype(u)
    MCVector(Point(u), vcat(data(spacelike, u), @SVector(zeros(T,1))))
end

Base.:(*)(Λ::Lorentz, u::MCVector) = MCVector(Point(u), Λ*data(u))


struct MCOneForm{n,P<:Point{Minkowski{n}},D<:StaticMatrix{1,n,<:Number}} <: Tensor{Minkowski{n},0,1}
    point::P
    data::D
end

constructortype(::Type{<:MCOneForm{n}}) where {n} = MCOneForm{n}

function MCOneForm{n}(p::Point{Minkowski{n}}, data::StaticMatrix{1,n,<:Number}) where {n}
    MCOneForm{n,typeof(p),typeof(data)}(p, data)
end

function MCOneForm{n}(p::Point{Minkowski{n}}, data::AbstractMatrix{<:Number}) where {n}
    MCOneForm{n}(p, convert(SMatrix{1,n,eltype(data)}, data))
end
MCOneForm(p::Point{Minkowski{n}}, data::AbstractMatrix{<:Number}) where {n} = MCOneForm{n}(p, data)

MCOneForm(v::MCOneForm) = v

datatype(::Type{MCOneForm{n,P,D}}) where {n,P,D} = D

function data(::Spacelike, v::MCOneForm{n}) where {n}
    T = dataeltype(v)
    SMatrix{1,n-1,T}(ntuple(j -> v.data[j], n-1))
end

function data(::Timelike, v::MCOneForm)
    T = dataeltype(v)
    SMatrix{1,1}((v.data[end],))
end

component(::Timelike, v::MCOneForm) = v.data[end]

Representation(::Type{<:MCOneForm{n}}) where {n} = MinkowskiCartesian{n}()

function mult(::MinkowskiCartesian{n}, ginv::InverseMetric{Minkowski{n}}, v::MCOneForm{n}) where {n} 
    MCVector(Point(v), vcat(vec(data(spacelike, v)), -vec(data(timelike, v))))
end
function mult(::MinkowskiCartesian{n}, v::MCOneForm{n}, ginv::InverseMetric{Minkowski{n}}) where {n} 
    mult(MinkowskiCartesian{n}(), ginv, v)
end

function square(v::MCOneForm)
    vx = data(spacelike, v)
    vt = component(timelike, v)
    -vt^2 + vx⋅vx
end


struct MCMetric{n,T<:Number,P<:Point{Minkowski{n}}} <: Metric{Minkowski{n}}
    point::P  # this is here to maintain consistency with interface
end
MCMetric(::Type{T}, p::Point{Minkowski{n}}) where {T<:Number,n} = MCMetric{n,T,typeof(p)}(p)
MCMetric(p::Point{Minkowski{n}}) where {n} = MCMetric(dataeltype(p), p)

Representation(::Type{<:MCMetric{n}}) where {n} = MinkowskiCartesian{n}()

Metric(p::MCPoint) = MCMetric(p)
InverseMetric(p::MCPoint) = inv(MCMetric(p))

function mult(::MinkowskiCartesian{n}, g::MCMetric{n}, v::Vector{Minkowski{n}}) where {n} 
    MCOneForm(Point(v), vcat(data(spacelike, v), -data(timelike, v))')
end
mult(::MinkowskiCartesian{n}, v::Vector{Minkowski{n}}, g::MCMetric{n}) where {n} = mult(MinkowskiCartesian{n}(), g, v)

function (η::MCMetric{n})(u::MCVector{n}, v::MCVector{n}) where {n}
    (ux, ut) = (data(spacelike, u), data(timelike, u))
    (vx, vt) = (data(spacelike, v), data(timelike, v))
    -ut⋅vt + ux⋅vx
end

function (η::InvMetric{Minkowski{n},<:MCMetric{n}})(u::MCOneForm{n}, v::MCOneForm{n}) where {n}
    (ux, ut) = (data(spacelike, u), data(timelike, u))
    (vx, vt) = (data(spacelike, v), data(timelike, v))
    -ut⋅vt + ux⋅vx
end

data(η::MCMetric{n,T}) where {n,T} = data(MinkowskiSignature{n,T}())
data(η::InvMetric{Minkowski{n},<:MCMetric{n,T}}) where {n,T} = data(η.metric)


"""
    exponential(R::Representation, u::Vector, s::Number)

A lower level method for the exponential map [`exp`](@ref) that takes the tensor representation as
an argument.  This should be `Representation(u)`.
"""
function exponential(::MinkowskiCartesian{n}, u::Vector{Minkowski{n}}, s::Number) where {n}
    p = Point(u)
    constructortype(p)(data(p) + s*data(u))
end

function paralleltransport(::MinkowskiCartesian{n}, u::Vector{Minkowski{n}}, v::Vector{Minkowski{n}},
                           s::Number=one(Float)) where {n}
    constructortype(u)(exp(v, s), data(u))
end

"""
    pointsto([null, ], p::Point, q::Point)

Returns a vector ``v`` at ``p`` which points toward the point ``q``, in the sense that ``q``
lies along the geodesic to which ``v`` is tangent.  The type of the returned vector will depend
on the type of points.

If the separation is specified to be null via the `null` argument, the resulting vector will have normalized
timelike and spacelike components.  Be warned that failing to pass `null` for null separations will cause
the program to attempt to normalize a null vector.
"""
pointsto(p::MCPoint{n}, q::MCPoint{n}) where {n} = normalize(MCVector(p, data(q) - data(p)))

"""
    paralleltransport(u::Vector, p::Point)

Parallel transport the vector ``u`` to the point ``p`` along the geodesic connecting them.
"""
paralleltransport(u::Vector{Minkowski{n}}, p::Point{Minkowski{n}}) where {n} = constructortype(u)(p, data(u))

"""
    nullpointsto(::Character, p::Point{M}, q::Point{M})

For null-separated points ``p,q``, create a null vector pointing from ``p`` to ``q`` which is normalized
according to [`normalizeproj`](@ref).
"""
function nullpointsto(c::Union{Spacelike,Timelike}, p::MCPoint{n}, q::MCPoint{n}) where {n}
    normalizeproj(c, MCVector(p, data(q) - data(p)))
end

function nullpointsfrom(c::Union{Spacelike,Timelike}, p::MCPoint{n}, q::MCPoint{n}) where {n}
    normalizeproj(c, MCVector(q, data(q) - data(p)))
end

#TODO: I still kind of hate this, especially since for some manifolds the timelike killing vector
#is actually unique... change or maybe just get rid of
"""
    killing(timelike, ::Type{V}, p::Point) where {V<:Vector}

Returns the normalized timelike Killing vector of type `V` at point ``p``.  The exact vector returned
will depend on the coordinate system of `p`, for example, in Minkowski space this will return
``\\partial_t``, where ``t`` is the time coordinate of the provided representation of ``p``.
"""
function killing(::Timelike, ::Type{<:MCVector{n}}, p::Point{Minkowski{n}}) where {n} 
    T = dataeltype(p)
    MCVector{n}(p, @SVector T[0,0,0,1])
end

"""
    ∇(𝒻, p::Point)

Compute the gradient of the scalar function ``f`` at the point ``p``, returning a `OneForm`.
"""
function ∇(𝒻, q::MCPoint{n}) where {n}
    v = ForwardDiff.gradient(data(q)) do qd
        𝒻(MCPoint{n}(qd))
    end
    v = @SMatrix [v[1] v[2] v[3] v[4]]
    MCOneForm(q, v)
end
