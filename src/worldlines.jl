
"""
    Worldline{M} <: ManifoldSubset{M}

A worldline in manifold `M`.  We define all worldlines to be timelike, in the sense that their tangent
at every point is timelike.
"""
abstract type Worldline{M<:Manifold} <: ManifoldSubset{M} end

abstract type InertialWorldline{M<:Manifold} <: Worldline{M} end

"""
    comoving(l::Worldline, s::Number)

Returns the diffeomorphism into the Gaussian normal coordinates of the tangent vector to the worldline at
the point given by the affine parameter ``s``.
"""
comoving(l::Worldline, s::Number) = comoving(tangent(l, s))

"""
    distance(l::Worldline, p::Point)
    distance(p::Point, l::Worldline)

The proper distance from the worldline to the point, defined to be the spacelike geodesic distance
from the worldline to the point.
"""
distance(p::Point{M}, l::Worldline{M}) where {M<:Manifold} = distance(l, p)

"""
    pointofdistance(l::Worldline, p::Point)

Returns the point ``q`` on the worldline such that the distance from the worldline to ``p``
is equal to the distance from ``p`` to ``q``.  See [`timeofdistance`](@ref).
"""
pointofdistance(l::Worldline{M}, p::Point{M}) where {M} = l(timeofdistance(l, p))

"""
    tangentofdistance(l::Worldline, p::Point)

Returns the tangent to the worldline at point ``q`` such that the distance from the worldline to ``p``
is equal to the distance from ``p`` to ``q``.  See [`timeofdistance`](@ref).
"""
tangentofdistance(l::Worldline{M}, p::Point{M}) where {M} = tangent(l, timeofdistance(l, p))

"""
    frameofdistance(l::Worldline, p::Point)

Returns the diffeomorphism into the Gaussian normal coordinates of the tangent to the worldline at the
point to which the distance from ``p`` to the worldline is measured.
"""
frameofdistance(l::Worldline{M}, p::Point{M}) where {M} = comoving(l, timeofdistance(l, p))

"""
    invframeofdistance(l::Worldline, p::Point)

Returns `invcomoving(tangent(l, timeofdistance(l, p)))` but may be more efficient in some cases.
"""
invframeofdistance(l::Worldline{M}, p::Point{M}) where {M} = invcomvoing(l, timeofdistance(l, p))

"""
    framewithinvofdistance(l::Worldline, p::Point)

Returns `comovingwithinv(tangent(l, timeofdistance(l, p)))` but may be more efficient in some cases.
"""
framewithinvofdistance(l::Worldline{M}, p::Point{M}) where {M} = comovingwithinv(l, timeofdistance(l, p))

"""
    paralleltransport(v::Vector, l::Worldline, t₁, t₂)

Parallel transport a vector ``v`` which is on the worldline `l` at affine time ``t_1`` to the point at affine
time ``t_2``.  For inertial worldlines this is equivalent to
```julia
paralleltransport(v, tangent(l, t₁), t₂ - t₁)
```
since that method parallel transports along the geodesic to which the second argument is tangent, which
in the case of inertial worldlines is the worldline itself.
"""
function paralleltransport(v::Vector{M}, l::InertialWorldline{M}, t₁::Number, t₂::Number) where {M}
    u = tangent(l, t₁)
    paralleltransport(v, u, t₂ - t₁)
end

"""
    velocity(l::Worldline, p::Point)

A velocity field on the whole manifold which matches the tangent to the worldline on the worldline.
By definition, this is the tangent to the worldline parallel transported along the geodeisc connecting
``p`` to the point on the worldline to which distance is maximized (i.e. the point to which distance
is measured).  This is useful because it gives the velocity of any matter at ``p`` which is comoving with
the worldline.  For example, this gives the velocity field of a rigid body moving with `l` at all
points inside the body.
"""
function velocity(l::InertialWorldline{M}, p::Point{M}) where {M}
    s = timeofdistance(l, p)
    paralleltransport(tangent(l, s), p)
end

struct InertMCWorldline{V<:Vector{Minkowski{4}},D<:Diffeomorphism{Minkowski{4}}} <: InertialWorldline{Minkowski{4}}
    vector::V  # tangent at s=0, must be normed
    # we keep the boost in the data structure so that we don't have to keep re-calculating
    boost::D  # stored so it doesn't have to be re-computed
end

InertMCWorldline(u::Vector{Minkowski{4}}) = InertMCWorldline(u, comoving(u))

InertialWorldline(u::MCVector) = InertMCWorldline(u)

(l::InertMCWorldline)(s::Number) = geodesic(l.vector, s)

tangent(l::InertMCWorldline, s::Number) = paralleltransport(l.vector, s)

(𝒻::Diffeomorphism{Minkowski{4}})(l::InertMCWorldline) = InertMCWorldline(𝒻(l.vector))

comoving(l::InertMCWorldline, s::Number) = l.boost

invcomoving(l::InertMCWorldline, s::Number) = inv(l.boost)

"""
    timeofdistance(l::Worldline, p::Point)

Get the proper time along worldline `l` of the point to which distance from point `p` is the distance from
`p` to `l`.  This is the proper time which *maximizes* the distance from the point at that time to `p`.
"""
function timeofdistance(l::InertMCWorldline, p::MCPoint)
    let u = data(l.vector), x = data(p), y = data(Point(l.vector))
        minkowski(u, y - x)
    end
end

function distance(l::InertMCWorldline, p::MCPoint)
    s = timeofdistance(l, p)
    distance(p, l(s))
end

# this is very efficient in MC
velocity(l::InertialWorldline, p::MCPoint) = paralleltransport(l.vector, p)

