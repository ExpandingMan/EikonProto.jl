
# integrator with n dimensional volume element
abstract type Integrator{n} end


for name ∈ (:SimpleRiemannInt, :SimpleRungeKuttaInt)
    @eval struct $name{T<:Number} <: Integrator{1}
        x₀::T
        x₁::T
        dx::T
    
        function $name(x₀::Number, x₁::Number, dx::Number)
            (x₀, x₁, dx) = promote(x₀, x₁, dx)    
            new{typeof(x₀)}(x₀, x₁, dx)
        end
    end
    @eval $name(x₀::Number, x₁::Number, n::Integer=100) = $name(x₀, x₁, (x₁ - x₀)/n)
end

∫(𝒻, int::SimpleRiemannInt) = let dx = int.dx, o = zero(dx), x₀ = int.x₀, x₁ = int.x₁
    x = x₀
    s = zero(dx)
    while x < x₁
        x += dx
        s += 𝒻(x)*dx
    end
    s
end

# I have explicitly checked that this does not allocate outside of 𝒻
∫(𝒻, int::SimpleRungeKuttaInt) = let dx = int.dx, o = zero(dx), x₀ = int.x₀, x₁ = int.x₁
    x = x₀
    s = zero(dx)
    k4 = 𝒻(x)
    while x < x₁
        k1 = k4  # k1 of this step is k4 of last
        # preamble
        x += dx/2
        k2 = 𝒻(x)
        x += dx/2
        k4 = 𝒻(x)
        s += (dx/6)*(k1 + 4*k2 + k4)
    end
    s
end



#====================================================================================================
TODO: everything below this point is likely going to be re-done somehow
====================================================================================================#

struct MetropolisSymSampler{L,E,F,T}
    lastvalue::RefValue{L}
    lasteval::RefValue{E}
    𝒻::F
    transition::T  # worried about abuse of closures

    function MetropolisSymSampler{L,E}(𝒻, rng::AbstractRNG, trans, xinit, n::Integer=32) where {L,E}
        o = new{L,E,typeof(𝒻),typeof(trans)}(RefValue{L}(), RefValue{E}(), 𝒻, trans)
        lastvalue!(o, xinit)
        lasteval!(o, 𝒻(xinit))
        for j ∈ 1:n
            markoviterate(rng, o)
        end
        o
    end
end

lastvalue(smp::MetropolisSymSampler) = smp.lastvalue[]
lastvalue!(smp::MetropolisSymSampler, x) = (smp.lastvalue[] = x)

lasteval(smp::MetropolisSymSampler) = smp.lasteval[]
lasteval!(smp::MetropolisSymSampler, x) = (smp.lasteval[] = x)

function markoviterate(rng::AbstractRNG, smp::MetropolisSymSampler)
    x′ = smp.transition(rng, lastvalue(smp))
    fx′ = smp.𝒻(x′)
    α = fx′/lasteval(smp)
    u = rand(rng, Float)  # is it really ok to just leave this as Float? 
    b = u ≥ α
    if b
        lastvalue!(smp, x′)
        lasteval!(smp, fx′)
    end
    b
end

#TODO: the below in no way checks if its starting to converge

function Base.rand(rng::AbstractRNG, smp::MetropolisSymSampler)
    while true
        markoviterate(rng, smp) && return lastvalue(smp)
    end
end



