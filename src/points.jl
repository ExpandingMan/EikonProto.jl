

"""
    Atlas{M<:Manifold}

Coordinate atlas on manifold ``M``.  We'd like these to cover the entire manifold, but we are very
laissez-faire in enforcement of this because it can drastically simplify some use cases to simply
allow an incomplete atlas and have special handling of any holes.
"""
abstract type Atlas{M<:Manifold} end


"""
    CartesianAtlas{M} <: Atlas{M}

A coordinate Atlas that consists of a single cartesian patch that covers the entire manifold.
"""
struct CartesianAtlas{M} <: Atlas{M} end


"""
    Point{M<:Manifold}

Abstract types for points on the manifold ``M``.  All implementations contain coordinate data.
Therefore, in some contexts `Point` can be thought of as "point with coordinate system".
For convenience, some methods might depend on the coordinates in which `p` is specified.  For
example, the function `normal(timelike, p)` returns a timelike normal vector proportional to
the timelike coordinate vector at `p`.  This should be used with care and should be considered
more of a convenient shortcut for `normal(timelike, p)` than a property of
`p`.
"""
abstract type Point{M<:Manifold} end

data(p::Point) = p.data  # fallback

datatype(p::Point) = typeof(data(p))

"""
    constructortype(p::Point)

Returns a type that can be used to construct a point.  Such a type *must* have a method which
takes as an argument only the coordinate data used by `p`.  Defaults to `typeof(p)`.
"""
constructortype(::Type{P}) where {P<:Point} = P
constructortype(p::Point) = constructortype(typeof(p))

# note that this fallback is only for identical types, otherwise you need to define
function Base.:(==)(p::P, q::P) where {P<:Point}
    # strict equality doesn't try to reconcile different patches, since floating point
    # will probably not match anyway
    patchindex(p) == patchindex(q) || return false
    data(q)
end
function Base.:(≈)(p::P, q::P; kw...) where {P<:Point} 
    (pidx, qidx) = (patchindex(p), patchindex(q))
    pidx == qidx || (p = topatch(p, qidx))
    (≈)(data(p), data(q); kw...)
end


struct MCPoint{n,D<:StaticVector} <: Point{Minkowski{n}}
    data::D
end

constructortype(::Type{<:MCPoint{n}}) where {n} = MCPoint{n}

MCPoint{n}(d::StaticVector{n,<:Number}) where {n} = MCPoint{n,typeof(d)}(d)

MCPoint{n}(v::AbstractVector{<:Number}) where {n} = MCPoint{n}(convert(SVector{n,eltype(v)}, v))
MCPoint(v::StaticVector{n,<:Number}) where {n} = MCPoint{n}(v)
MCPoint(v::AbstractVector) = MCPoint{length(v)}(v)

MCPoint{n}(p::MCPoint{n}) where {n} = p
MCPoint(p::MCPoint) = p

Atlas(p::MCPoint{n}) where {n} = CartesianAtlas{n}()

patchindex(::Type{Val}, p::MCPoint) = Val(1)
patchindex(p::MCPoint) = 1
topatch(p::MCPoint, ::Val{1}) = p
topatch(p::MCPoint, idx::Integer) = topatch(p, Val(idx))

datatype(::Type{<:MCPoint{n,D}}) where {n,D} = D

function data(::Spacelike, p::MCPoint{n}) where {n} 
    let pd = data(p)
        SVector{n-1,dataeltype(p)}(ntuple(j -> pd[j], n-1))
    end
end

"""
    normal(timelike, p::Point)

Returns the timelike coordinate vector, in the coordinate representation of ``p`` at ``p``.
"""
function normal(::Timelike, p::MCPoint{n}) where {n}
    T = dataeltype(p)
    MCVector{n}(p, @SVector T[0, 0, 0, 1])
end

function squaredistance(p::MCPoint{n}, q::MCPoint{n}) where {n}
    δ = data(q) - data(p)
    MinkowskiSignature{n,dataeltype(p)}()(δ, δ)
end

distance(p::MCPoint{n}, q::MCPoint{n}) where {n} = √(abs(squaredistance(p, q)))

# this is a requirement of our coordinate systems. this needn't be same thing as data!
component(::Timelike, p::Point) = p.data[end]


"""
    defaultrep(p::Point)
    defaultrep(::Type{<:Point})

The default representation for tangent tensors defined on the manifold of the point.  This
is merely a convenience to make it easier to quickly do things like construct the metric tensor.
"""
defaultrep(p::Point) = defaultrep(typeof(p))


struct SpherePolarAtlas1{n} <: Atlas{Sphere{n}} end

struct S2P1Point{D<:StaticVector} <: Point{Sphere{2}}
    data::D

    S2P1Point(d::StaticVector{2,<:Number}) = new{typeof(d)}(d)
end

constructortype(::Type{<:S2P1Point}) = S2P1Point

S2P1Point(v::AbstractVector{<:Number}) = S2P1Point(convert(SVector{2,eltype(v)}, v))

S2P1Point(p::S2P1Point) = p

Atlas(p::S2P1Point) = SpherePolarAtlas1{2}()

patchindex(::Type{Val}, p::S2P1Point) = Val(1)
patchindex(p::S2P1Point) = 1
topatch(p::S2P1Point, ::Val{1}) = p
topatch(p::S2P1Point, idx::Integer) = topatch(p, Val(idx))

datatype(::Type{<:S2P1Point{D}}) where {D} = D

data(::Spacelike, p::S2P1Point) = data(p)

function distance(p::S2P1Point, q::S2P1Point)
    # according to wikipedia, this version is numerically better conditioned
    let (θ₁, ϕ₁) = data(p), (θ₂, ϕ₂) = data(q)
        Δϕ = abs(ϕ₂ - ϕ₁)
        Δθ = abs(θ₂ - θ₁)
        ahav(hav(Δϕ) + hav(Δθ)*(1 - hav(Δϕ) - hav(ϕ₁ + ϕ₂)))
    end
end

squaredistance(p::S2P1Point, q::S2P1Point) = distance(p, q)^2
