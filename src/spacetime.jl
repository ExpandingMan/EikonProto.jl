
"""
    Solid{M<:Manifold,S<:Surface,T<:Material}

An object that describes a surface on the manifold ``M`` together with its optical properties.
"""
struct Solid{M<:Manifold,S<:Surface{M},T<:Material}
    surface::S
    material::T

    Solid(Σ::Surface{M}, m::Material) where {M<:Manifold} = new{M,typeof(Σ),typeof(m)}(Σ, m)
end

Surface(s::Solid) = s.surface
Material(s::Solid) = s.material

MaterialProperties(s::Solid) = MaterialProperties(Material(s))

isconvex(s::Solid) = isconvex(s.surface)

sdistance(s::Solid{M}, p::Point{M}) where {M<:Manifold} = sdistance(Surface(s), p)

velocity(s::Solid{M}, p::Point{M}) where {M<:Manifold} = velocity(Surface(s), p)

normal(s::Solid{M}, p::Point{M}) where {M<:Manifold} = normal(s.surface, p)

function emittedradiance(s::Solid, v::Vector{M}) where {M}
    u = velocity(s, Point(v))
    n = normal(s, Point(v))
    emittedradiance(Material(s), u, n, v)
end

for f ∈ (:frameofdistance, :invframeofdistance, :framewithinvofdistance)
    @eval $f(s::Solid{M}, p::Point{M}) where {M} = $f(Surface(s), p)
end


#====================================================================================================
infinity #TODO: nowhere near done       
====================================================================================================#


abstract type Infinity{M} end


struct BlackInfinity{M} <: Infinity{M} end

radiance(inf::BlackInfinity{M}, u::Vector{M}) where {M} = ZeroSpectrum()


struct MinkowskiSphereInfinity{D<:Diffeomorphism{Minkowski4},V<:Vector{Minkowski4},S<:ImageSampler} <: Infinity{Minkowski4}
    frame::D
    rest::V
    sampler::S
end

function MinkowskiSphereInfinity(u::Vector{Minkowski4}, s::ImageSampler)
    MinkowskiSphereInfinity(comoving(u), u, s)
end

function project(inf::MinkowskiSphereInfinity, u::Vector{Minkowski4})
    with(inf.frame, u) do u
        (x, y, z, t) = data(u)
        r = √(x^2 + y^2 + z^2)
        θ = asin(z/r)
        ϕ = (y ≥ 0 ? -1 : 1)*acos(x/√(x^2 + y^2))  # yes, must be backward
        isnan(ϕ) && (ϕ = zero(ϕ))  # should only happen at singularities so is safe
        S2P1Point(@SVector Float32[θ,ϕ])
    end
end

function radiance(inf::MinkowskiSphereInfinity, u::Vector{Minkowski4})
    p = project(inf, u)
    radiance(inf.sampler, p, -inf.rest⋅u)
end


#===================================================================================================#
 

"""
    Spacetime{M}

A spacetime on manifold ``M``.  In our parlance, the spacetime is the manifold together with all matter content,
which in our case mostly means `Solid` objects.
"""
abstract type Spacetime{M} end


"""
    solidarray(T, solids)

Creates an array containing a set of solids the `eltype` of which is the explicit union type
of all elements in `solids`.  The constructor `T` is called on the result.

This is a somewhat dubious trick to try to guarantee that union splitting will always work
efficiently on the set of `Solid` objects belonging to a `Spacetime` object.
"""
function solidarray(::Type{T}, solids) where {T<:Base.AbstractVector}
    alltypes = solids |> Map(typeof) |> Unique() |> Tuple
    if any(t -> !(t <: Solid), alltypes)
        throw(ArgumentError("got non solid type in solids array"))
    end
    o = Array1{Union{alltypes...}}(undef, length(solids))
    for (j, s) ∈ enumerate(solids)
        o[j] = s
    end
    T(o)
end

struct MinkowskiSpacetime{T<:AbstractArray1,I<:Infinity} <: Spacetime{Minkowski{4}}
    solids::T
    infinity::I

    function MinkowskiSpacetime(solids, inf::Infinity=BlackInfinity{Minkowski4}(), ::Type{T}=Array1) where {T}
        ss = solidarray(T, solids)
        new{typeof(ss),typeof(inf)}(ss, inf)
    end
end

Infinity(w::MinkowskiSpacetime) = w.infinity

getsolid(w::MinkowskiSpacetime, idx::Integer) = w.solids[idx]

radianceatinfinity(w::MinkowskiSpacetime, u::Vector) = radiance(Infinity(w), u)

sdistances(w::MinkowskiSpacetime, p::Point{<:Minkowski}) = Iterators.map(s -> sdistance(s.surface, p), w.solids)

