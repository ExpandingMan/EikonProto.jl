

abstract type Diffeomorphism{M<:Manifold} end

_output_with(𝒻::Diffeomorphism, x::Number) = x
_output_with(𝒻::Diffeomorphism, ::Nothing) = nothing
_output_with(𝒻::Diffeomorphism{M}, p′::Point{M}) where {M} = TransformedPoint(𝒻, p′)
_output_with(𝒻::Diffeomorphism{M}, u′::Tensor{M}) where {M} = TransformedTensor(𝒻, u′)

# I'm a bit dubious about this, essentially the idea is this is some kind of projection that we
# don't need to transform back
_output_with(𝒻::Diffeomorphism{M}, p′::Point{N}) where {M,N} = p′

_output_with(𝒻::Diffeomorphism, tpl::Tuple) = ntuple(j -> _output_with(𝒻, tpl[j]), Val(length(tpl)))
_output_with(𝒻::Diffeomorphism{M}, a::At{M}) where {M} = _map(t -> _output_with(𝒻, t), a)

# this is default, free to define more efficient specialized methods
_applyinv(𝒻::Diffeomorphism{M}, p::Point{M}) where {M} = _apply(inv(𝒻), p)

#TODO: for god knows what reason the compiler is losing type stability here if I do this generically
with(proc, 𝒻::Diffeomorphism, a)  = _output_with(𝒻, proc(_apply(𝒻, a)))
with(proc, 𝒻::Diffeomorphism, a, b) = _output_with(𝒻, proc(_apply(𝒻, a), _apply(𝒻, b)))
with(proc, 𝒻::Diffeomorphism, a, b, c) = _output_with(𝒻, proc(_apply(𝒻, a), _apply(𝒻, b), _apply(𝒻, c)))
with(proc, 𝒻::Diffeomorphism, a, b, c, d) = _output_with(𝒻, proc(_apply(𝒻, a), _apply(𝒻, b), _apply(𝒻, c), _apply(𝒻, d)))

_apply(𝒻::Diffeomorphism{M}, a::At{M}) where {M} = _map(t -> _apply(𝒻, t), a)
_applyinv(𝒻::Diffeomorphism{M}, a::At{M}) where {M} = _map(t -> _applyinv(𝒻, t), a)

# this should be considered private
struct NoApply{T}
    value::T
end

Base.getindex(x::NoApply) = x.value

_apply(𝒻::Diffeomorphism, x::NoApply) = x[]


struct TransformedPoint{M<:Manifold,D<:Diffeomorphism{M},P<:Point{M}} <: Point{M}
    diffeo::D
    point::P
end

(𝒻::Diffeomorphism{M})(p::Point{M}) where {M} = TransformedPoint(𝒻, _apply(𝒻, p))

with(proc, p′::TransformedPoint, args...) = with(proc, p′.diffeo, NoApply(p′.point), args...)

Base.getindex(p′::TransformedPoint) = _applyinv(p′.diffeo, p′.point)

data(p′::TransformedPoint) = data(p′[])

datatype(::Type{<:TransformedPoint{M,D,P}}) where {M,D,P} = datatype(P)

# invariant that's free to use transformed
distance(p′::TransformedPoint{M,D}, q′::TransformedPoint{M,D}) where {M,D} = distance(p′.point, q′.point)


struct TransformedTensor{M<:Manifold,r,s,D<:Diffeomorphism{M},T<:Tensor{M,r,s}} <: Tensor{M,r,s}
    diffeo::D
    tensor::T
end

const TransformedVector{M,D,T} = TransformedTensor{M,1,0,D,T}

(𝒻::Diffeomorphism{M})(u::Tensor{M,r,s}) where {M,r,s} = TransformedTensor(𝒻, _apply(𝒻, u))

(𝒻::Diffeomorphism{M})(ts::At{M}) where {M} = _map(𝒻, ts)

with(proc, u′::TransformedTensor, args...) = with(proc, u′.diffeo, NoApply(u′.tensor), args...)
with(proc, u::Tensor, args...)  = with(proc, comoving(u), u, args...)

Base.getindex(u′::TransformedTensor) = _applyinv(u′.diffeo, u′.tensor)

# not so sure about this...
Base.getindex(u::Tensor) = u

Point(u′::TransformedTensor) = _applyinv(u′.diffeo, Point(u′.tensor))

data(u′::TransformedTensor) = data(u′[])

# not 100% sure this needs to be a strict requirement yet...
Representation(::Type{<:TransformedTensor{M,r,s,D,T}}) where {M,r,s,D,T} = Representation(T)

Base.:(*)(a::Number, u′::TransformedTensor) = with(u -> a*u, u′)

# invariant
square(u′::TransformedTensor) = square(u′.tensor)

Base.exp(u′::TransformedVector, s::Number) = with(u -> exp(u, s), u′)

paralleltransport(u′::TransformedVector, s::Number) = with(u -> paralleltransport(u, s), u′)


struct IdentityDiffeomorphism{M} <: Diffeomorphism{M} end

Base.inv(𝒻::IdentityDiffeomorphism{M}) where {M} = 𝒻

_apply(𝒻::IdentityDiffeomorphism{M}, p::Point{M}) where {M} = p
_apply(𝒻::IdentityDiffeomorphism{M}, u::Tensor{M}) where {M} = u


struct InverseDiffeomorphism{M,D<:Diffeomorphism{M}} <: Diffeomorphism{M}
    diffeo::D
end

Base.inv(𝒻::Diffeomorphism) = InverseDiffeomorphism(𝒻)

# larger composites can be created by chaining these
struct CompositeDiffeomorphism{M,D1<:Diffeomorphism{M},D2<:Diffeomorphism{M}} <: Diffeomorphism{M}
    diffeo1::D1
    diffeo2::D2
end

# it's so long, let's allow a shorthand
const InvDiff{M,D} = InverseDiffeomorphism{M,D}
const CompDiff{M,D1,D2} = CompositeDiffeomorphism{M,D1,D2}

#WARN: the apply need to be more generic and dispatch on rep instead of type

Base.inv(𝒻::InvDiff) = 𝒻.diffeo

# fallbacks, can be extended
_apply(𝒻::InvDiff, p::Point) = _applyinv(𝒻.diffeo, p)
_apply(𝒻::InvDiff, u::Tensor) = _applyinv(𝒻.diffeo, u)

_applyinv(𝒻::InvDiff, p::Point) = _apply(𝒻.diffeo, p)
_applyinv(𝒻::InvDiff, u::Tensor) = _apply(𝒻.diffeo, u)

Base.:(∘)(𝒻2::Diffeomorphism{M}, 𝒻1::Diffeomorphism{M}) where {M} = CompDiff{M,typeof(𝒻1),typeof(𝒻2)}(𝒻1, 𝒻2)

_apply(𝒻::CompDiff{M}, p::Point{M}) where {M} = _apply(𝒻.diffeo2, _apply(𝒻.diffeo1, p))
_apply(𝒻::CompDiff{M}, u::Tensor{M}) where {M} = _apply(𝒻.diffeo2, _apply(𝒻.diffeo1, u))

_applyinv(𝒻::CompDiff{M}, p::Point{M}) where {M} = _applyinv(𝒻.diffeo1, _applyinv(𝒻.diffeo2, p))
_applyinv(𝒻::CompDiff{M}, u::Tensor{M}) where {M} = _applyinv(𝒻.diffeo1, _applyinv(𝒻.diffeo2, u))

Base.inv(𝒻::CompDiff) = inv(𝒻.diffeo1) ∘ inv(𝒻.diffeo2)


struct MCTranslation{n,P<:Point{Minkowski{n}}} <: Diffeomorphism{Minkowski{n}}
    origin::P
end

_apply(𝒻::MCTranslation{n}, p::MCPoint{n}) where {n} = MCPoint{n}(data(p) - data(𝒻.origin))
_applyinv(𝒻::MCTranslation{n}, p::MCPoint{n}) where {n}  = MCPoint{n}(data(p) + data(𝒻.origin))

_apply(𝒻::MCTranslation{n}, u::MCVector{n}) where {n} = MCVector{n}(_apply(𝒻, Point(u)), data(u))
_applyinv(𝒻::MCTranslation{n}, u::MCVector{n}) where {n} = MCVector{n}(_applyinv(𝒻, Point(u)), data(u))


struct MCLorentz{L<:Lorentz} <: Diffeomorphism{Minkowski{4}}
    transform::L
    invtransform::L  # we need this so much it's easier to just keep it here; might need to change type
end

function MCLorentz(u::Vector{Minkowski{4}})
    Λ = LorentzGroup.comoving(data(u))
    MCLorentz(Λ, inv(Λ))
end

_apply(𝒻::MCLorentz, p::MCPoint{4}) = MCPoint{4}(𝒻.transform*data(p))
_applyinv(𝒻::MCLorentz, p::MCPoint{n}) where {n} = MCPoint{4}(𝒻.invtransform*data(p))

_apply(𝒻::MCLorentz, u::MCVector{4}) = MCVector{4}(_apply(𝒻, Point(u)), 𝒻.transform*data(u))
_applyinv(𝒻::MCLorentz, u::MCVector{4}) = MCVector{4}(_applyinv(𝒻, Point(u)), 𝒻.invtransform*data(u))


"""
    comoving(u::Vector)

Returns the diffeomorophism into the comoving (Gaussian normal) coordinates of the vector `u`.
"""
function comoving(u::MCVector{4})
    ϕ1 = MCLorentz(u)
    ϕ2 = MCTranslation(Point(_apply(ϕ1, u)))
    ϕ2 ∘ ϕ1
end

"""
    invcomoving(u::Vector)

Returns the diffeomorphism *out* of the comoving (Gaussian normal) coordinates of the vector `u`.
Here `u` is to be provided in the coordinates that the returned diffeomorphism transforms *into*.
This is guaranteed to be equivalent to `inv(comoving(u))` but may be more efficient in some cases.
"""
function invcomoving(u::MCVector{4})
    # this isn't great, maybe change in LorentzGroup.jl
    ϕ1 = MCLorentz(-project(spacelike, u) + project(timelike, u))
    ϕ2 = inv(MCTranslation(Point(u)))
    ϕ2 ∘ ϕ1
end

"""
    comovingwithinv(u::Vector)

Equivalent to
```julia
(comoving(u), invcomoving(u))
```
but may be more efficient in some cases.
"""
comovingwithinv(u::MCVector{4}) = (comoving(u), invcomoving(u))
