
abstract type Raytracer{W<:Spacetime} end

@kwdef struct SimpleRaytracerOptions
    surface_neighborhood::Float = 10^(-4)
    escape_distance::Float = 10^3
    max_raytrace_iterations::Int = 50
    iteration_scale_factor::Float = 1.0
end

struct SimpleRaytracer{W<:Spacetime} <: Raytracer{W}
    spacetime::W
    options::SimpleRaytracerOptions
end
function SimpleRaytracer(w::Spacetime; kw...) 
    SimpleRaytracer{typeof(w)}(w, SimpleRaytracerOptions(;kw...))
end

Spacetime(rt::SimpleRaytracer) = rt.spacetime


abstract type Raytrace{W<:Spacetime} end


struct SimpleRaytrace{W<:Spacetime,V<:Vector} <: Raytrace{W}
    niter::Int
    index::Int  # 0 indicates escaped
    distance::Float  # can be negative
    vector::V
end

function SimpleRaytrace{W}(niter::Integer, idx::Integer, distance::Real, v::Vector{M}) where {W,M}
    v = v[]
    SimpleRaytrace{W,typeof(v)}(niter, idx, distance, v)
end

#TODO: I want to keep this around because it's a little easier to understand and might help in the
#future with debugging, make a parameter for simple raytrace so it's an option to use
function _simpleraytrace_inner_diffeo(rt::SimpleRaytracer{W}, s::Solid, v::Vector, minδ::Real,
                                      niter::Integer, minindex::Integer) where {W}
    n = normal(s, Point(v))
    ℓ = rt.options.iteration_scale_factor * minδ  # this is signed, so it should move is in right direction
    𝒻 = frameofdistance(s, Point(v))
    (n, v) = (𝒻(n), 𝒻(v))
    (ω, v̂) = with(v) do v
        normalizeproj_with(timelike, v)
    end
    cosϕ = n ⋅ v̂
    if cosϕ ≤ 0 && isconvex(s)
        return SimpleRaytrace{W}(niter, 0, ∞, v)
    end
    isconvex(s) && (ℓ = ℓ/cosϕ)  # this optimization only makes sense for convex
    w = ω*paralleltransport(v̂, -ℓ)
    SimpleRaytrace(rt, w; niter=niter+1, minδ, minindex)
end

function _simpleraytrace_inner(rt::SimpleRaytracer{W}, s::Solid, v::Vector, minδ::Real,
                               niter::Integer, minindex::Integer) where {W}
    n = normal(s, Point(v))
    u = velocity(s, Point(v))
    (ω, v̂) = normalizeproj_with(timelike, v)
    α = abs(u⋅v̂)
    ℓ = minδ / α  # this is signed, so it should move is in right direction
    cosϕ = (n ⋅ v̂)/α
    if cosϕ ≤ 0 && isconvex(s)
        return SimpleRaytrace{W}(niter, 0, ∞, v)
    end
    isconvex(s) && (ℓ = ℓ/cosϕ)  # this optimization only makes sense for convex
    w = ω*paralleltransport(v̂, -ℓ)
    SimpleRaytrace(rt, w; niter=niter+1, minδ, minindex)
end

function SimpleRaytrace(rt::SimpleRaytracer{W}, v::Vector{M};
                        niter::Integer=0,
                        minindex::Integer=0,
                        minδ::Real=∞,
                       ) where {M<:Manifold,W<:Spacetime}
    v = v[]
    # we assume without checking that v is null
    niter > rt.options.max_raytrace_iterations && return SimpleRaytrace{W}(niter-1, 0, minδ, v)
    maxδ = zero(typeof(minδ))
    minabsδ = abs(minδ)
    maxabsδ = abs(maxδ)
    for (j, δ) ∈ enumerate(sdistances(Spacetime(rt), Point(v)))
        abs(δ) < rt.options.surface_neighborhood && return SimpleRaytrace{W}(niter, j, δ, v)
        if abs(δ) < minabsδ
            (minδ, minabsδ) = (δ, abs(δ))
            minindex = j
        end
        if abs(δ) > maxabsδ
            (maxδ, maxabsδ) = (δ, abs(δ))
        end
    end
    if maxδ > rt.options.escape_distance
        SimpleRaytrace{W}(niter, 0, ∞, v)
    else
        s = getsolid(Spacetime(rt), minindex)
        # this function barrier can help the compiler in some cases
        _simpleraytrace_inner(rt, s, v, minδ, niter, minindex)
    end
end

isescaped(r::SimpleRaytrace) = r.index == 0

raytrace(rt::SimpleRaytracer, v::Vector; kw...) = SimpleRaytrace(rt, v; kw...)


abstract type Renderer{W<:Spacetime} end

emittedradiance(rd::Renderer, s::Solid, v::Vector{M}) where {M} = emittedradiance(s, v)

#TODO: all 8 methods
function radiance(rd::Renderer, ::MaterialProperties{true,false,false}, s::Solid, v::Vector{M}) where {M}
    emittedradiance(rd, s, v)
end
function radiance(rd::Renderer, ::MaterialProperties{false,true,false}, s::Solid, v::Vector{M}) where {M}
    reflectedradiance(rd, s, v)
end

"""
    radiance(rd::Renderer, s::Solid, u::Vector)

Compute the radiance of solid `s` in the direction of ``u`` using renderer `rd`, at the photon
energy given by the magnitude of ``u``.  This is defined in terms of the normal field of the
surface, so it returns the radiance as if the point of ``u`` were "on" the surface, regardless of
the actual location of the surface.  The `s` argument can be omitted to calculate the true radiance.
"""
radiance(rd::Renderer, s::Solid, u::Vector) = radiance(rd, MaterialProperties(s), s, u)

radianceatinfinity(rd::Renderer, u::Vector) = radianceatinfinity(Spacetime(rd), u)

getsolid(rd::Renderer, idx) = getsolid(Spacetime(rd), idx)

isescaped(rd::Renderer, r::Raytrace) = isescaped(r)

struct DefaultRenderer{W<:Spacetime,R<:Raytracer{W},C<:LinColor} <: Renderer{W}
    spacetime::W
    raytracer::R
end

function DefaultRenderer(s::Spacetime, rt::Raytracer=SimpleRaytracer(s), ::Type{C}=XYZ{Float}) where {C<:LinColor}
    DefaultRenderer{typeof(s),typeof(rt),C}(s, rt)
end

colortype(rd::DefaultRenderer{S,R,C}) where {S,R,C} = C

Spacetime(rd::DefaultRenderer) = rd.spacetime


"""
    radiance(rd::Renderer, u::Vector)

Compute the radiance for photons with momentum ``u``, at the point to which ``u`` is tangent.
This is the principle function for rendering computations.
"""
function radiance(rd::DefaultRenderer, u::Vector)
    # only possibilities are we have a surface or have escaped
    r = raytrace(rd.raytracer, u)
    w = r.vector
    if isescaped(rd, r)
        radianceatinfinity(rd, w)
    else
        radiance(rd, getsolid(rd, r.index), w)
    end
end

#TODO: above is type unstable, but color is stable... does it matter where boundary is?
color(rd::DefaultRenderer, u::Vector) = colortype(rd)(radiance(rd, u))
