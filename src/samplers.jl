
abstract type ImageSampler{M<:Manifold,I<:Image} end

radiance(s::ImageSampler, p::S2P1Point, r::Number) = emittedradiance(s, p, r)


struct SphereEquirectangularSampler{I<:Image} <: ImageSampler{Sphere{2},I}
    image::I
end

Image(s::SphereEquirectangularSampler) = s.image

function EikonProto.emittedradiance(s::SphereEquirectangularSampler, p::S2P1Point, r::Number)
    (θ, ϕ) = EikonProto.data(p)
    emittedradiance(Image(s), ϕ, θ, r)
end

function SphereEquirectangularSampler(cimg::ColorspaceImage; kw...)
    img = XYZImage(cimg; kw...)
    SphereEquirectangularSampler(img)
end

function SphereEquirectangularSampler(cimg::AbstractMatrix{<:Colorant}; kw...)
    cimg = ColorspaceImage(cimg, (Float(π), -Float(π)), (-Float(π/2), Float(π/2)))
    SphereEquirectangularSampler(cimg; kw...)
end

