
"""
    Material

Material types describe the optical properties of a surface.  These can be associated with
surfaces via the [`Solid`](@ref) type.
"""
abstract type Material end

"""
    MaterialProperties{e,r,t}

A trait implemented by materials describing whether they are emissive (`e`),
reflective (`r`) and transmissive (`t`).  These properties are used to determine
whether calculations for these properties can be elided.
"""
struct MaterialProperties{e,r,t} end

MaterialProperties(m::Material) = MaterialProperties(typeof(m))

function radiance end

# this one is special because can be computed with material alone
function emittedradiance end

function reflectedradiance end

function transmittedradiance end


struct Emitter{S<:Spectrum} <: Material
    spectrum::S
end

MaterialProperties(::Type{<:Emitter}) = MaterialProperties{true,false,false}()

#WARN: using u⋅v as arg to redshift assumes v₀ = -1 in frame where we want to get back the spectrum

emittedradiance(m::Emitter, u::Vector{M}, n::Vector{M}, v::Vector{M}) where {M<:Manifold} = blueshift(m.spectrum, -u⋅v)


struct AnisotropicEmitter{F,S<:Spectrum} <: Material
    anisotropy::F
    spectrum::S
end
AnisotropicEmitter(s::Spectrum) = AnisotropicEmitter(identity, s)

MaterialProperties(::Type{<:AnisotropicEmitter}) = MaterialProperties{true,false,false}()

function emittedradiance(m::AnisotropicEmitter, u::Vector{M}, n::Vector{M}, v::Vector{M}) where {M<:Manifold}
    # confusingly, this is *redshift* because of the time reversal
    r = Float(-u⋅v)
    α = m.anisotropy(v⋅n / r)
    α*redshift(m.spectrum, r)
end

