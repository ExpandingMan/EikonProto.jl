#====================================================================================================
NOTE:
this exists for development of surrogates
====================================================================================================#

struct MinkowskiCartesian{T<:Number} <: Spacetime{T} end

MinkowskiCartesian() = MinkowskiCartesian{Float64}()

basetype(::Type{<:MinkowskiCartesian}) = MinkowskiCartesian

function metric(M::MinkowskiCartesian{T1}, x::AbstractVector{T2}) where {T1,T2}
    T = promote_type(T1, T2)
    @SMatrix T[1 0 0 0
               0 1 0 0
               0 0 1 0
               0 0 0 -1
              ]
end

invmetric(M::MinkowskiCartesian, x::AbstractVector) = metric(M, x)

System(M::MinkowskiCartesian) = System(M, nothing)

function _minkcart_solution(x0, p0, η, tspan, s::Real)
    # this is here to mimic behavior of actual solutions
    if !(tspan[1] ≤ s ≤ tspan[2])
        throw(ArgumentError("proper time outside bounds"))
    end
    x = x0 + s*(η*p0)
    (x, p0)
end

function Solution(sys::System{T}, x0, p0, tspan; kw...) where {T<:MinkowskiCartesian}
    x0 = convert(SVector{4}, x0)
    p0 = convert(SVector{4}, p0)
    η = invmetric(sys.spacetime, x0)
    sol = s -> _minkcart_solution(x0, p0, η, tspan, s)
    Solution{T,typeof(sys),typeof(sol)}(sys.spacetime, sys, sol)
end

function HamiltonianDiffEq.timespan(sol::Solution{<:MinkowskiCartesian}; δs::Real=0.01)
    (t0, t1) = sol.solution.tspan
    range(t0, t1, step=δs)
end
