#====================================================================================================
timediff       
====================================================================================================#
_maybe_recdualpart(::Nothing) = nothing
_maybe_recdualpart(x::Number) = recdualpart(x)
_maybe_recdualpart(x::Dual) = recdualpart(x)
_maybe_recdualpart(x::AbstractArray) = recdualpart(x)
_maybe_recdualpart(x::Tuple) = map(_maybe_recdualpart, x)
_maybe_recdualpart(x::NamedTuple) = map(_maybe_recdualpart, x)

# this function is to make objects passed by rrule compatible with zygote pullback
_handle_pullback_arg(y) = y
_handle_pullback_arg(::NoTangent) = nothing
_handle_pullback_arg(::ZeroTangent) = nothing
_handle_pullback_arg(y::Tangent) = map(_handle_pullback_arg, y.backing)

_restucture_timediff_pullback(xs) = (NoTangent(), _maybe_recdualpart(xs)...)

function ChainRulesCore.rrule(::typeof(timediff), 𝒻, x)
    (o, _withtimediff_inner_pullback) = Zygote.pullback(_dualwithtimediff, 𝒻, x)
    (recdualpart(o), _restucture_timediff_pullback ∘ _withtimediff_inner_pullback ∘ _handle_pullback_arg)
end
#===================================================================================================#

