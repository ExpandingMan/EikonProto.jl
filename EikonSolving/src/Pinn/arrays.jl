
"""
    DiagFactorMatrix

A matrix which is factorized in the form ``W = V \\textrm{diag}(\\exp(s))``.  This is a trick to help with NN
parameter training as the diagonal elements and the elements of ``V`` are treated as separate
parameters when taking a gradient.  Note that the stored parameters ``s`` must be exponentiated, as they are
the parameters used in gradient descent.
"""
struct DiagFactorMatrix{T,M<:AbstractMatrix,D<:AbstractVector} <: AbstractMatrix{T}
    V::M
    s::D
end

DiagFactorMatrix{T}(V::AbstractMatrix, s::AbstractVector) where {T} = DiagFactorMatrix{T,typeof(V),typeof(s)}(V, s)

Base.size(W::DiagFactorMatrix) = size(W.V)

function DiagFactorMatrix{T}(rng::AbstractRNG, W::AbstractMatrix, μ::Real=1.0f0, σ::Real=0.1f0) where {T}
    n = size(W,2)
    s = similar(W, T, n)
    s .= rand(rng, Normal(μ, σ), n)
    for j ∈ 1:length(s)
        W[:,j] ./= exp(s[j])
    end
    DiagFactorMatrix{T}(W, s)
end

function DiagFactorMatrix{T}(W::AbstractMatrix, μ::Real=1.0f0, σ::Real=0.1f0) where {T}
    DiagFactorMatrix{T}(Random.default_rng(), W, μ, σ)
end
function DiagFactorMatrix(rng::AbstractRNG, W::AbstractMatrix, μ::Real=1.0f0, σ::Real=0.1f0)
    DiagFactorMatrix{eltype(W)}(rng, W, μ, σ)
end
DiagFactorMatrix(W::AbstractMatrix, μ::Real=1.0f0, σ::Real=0.1f0) = DiagFactorMatrix{eltype(W)}(W, μ, σ)

# to resovle ambiguity
function _luxdevice(D::AbstractLuxDevice, W::DiagFactorMatrix)
    V = D(W.V)
    s = D(W.s)
    DiagFactorMatrix{eltype(W),typeof(V),typeof(s)}(V, s)
end
(D::LuxCUDADevice)(W::DiagFactorMatrix) = _luxdevice(D, W)

Base.getindex(W::DiagFactorMatrix, I::Vararg{Int,2}) = exp(W.s[I[2]])*W.V[CartesianIndex(I)]

# this is needed to ensure the container type is appropriate
# e.g. otherwise it won't know to make a CuArray
Base.similar(W::DiagFactorMatrix, ::Type{T}, dims::Tuple{Int,Vararg{Int,N}}) where {T,N} = similar(W.V, T, dims)

function LinearAlgebra.mul!(Y::AbstractMatrix, W::DiagFactorMatrix, A::AbstractMatrix)
    # not ideal, but getting rid of intermediate here is hard
    Y .= W.V * (exp.(W.s) .* A)
end

function LinearAlgebra.mul!(Y::AbstractMatrix, A::AbstractMatrix, W::DiagFactorMatrix)
    mul!(Y, A, W.V)
    Y .*= exp.(W.s)'
end


function factored_glorot_uniform(rng::AbstractRNG, ::Type{T}, m::Integer, n::Integer;
                                 μ::Real=1.0f0, σ::Real=0.1f0, kw...
                                ) where {T<:Number}
    W = glorot_uniform(rng, T, m, n; kw...)
    DiagFactorMatrix(rng, W, μ, σ)
end
function factored_glorot_uniform(::Type{T}, m::Integer, n::Integer; kw...) where {T<:Number} 
    factored_glorot_uniform(Random.default_rng(), T, m, n; kw...)
end
function factored_glorot_uniform(rng::AbstractRNG, m::Integer, n::Integer; kw...)
    factored_glorot_uniform(rng, Float32, m, n; kw...)
end
factored_glorot_uniform(m::Integer, n::Integer; kw...) = factored_glorot_uniform(Float32, m, n; kw...)

function factored_glorot_normal(rng::AbstractRNG, ::Type{T}, m::Integer, n::Integer;
                                μ::Real=1.0f0, σ::Real=0.1f0, kw...
                               ) where {T<:Number}
    W = glorot_normal(rng, T, m, n; kw...)
    DiagFactorMatrix(rng, W, μ, σ)
end
function factored_glorot_normal(::Type{T}, m::Integer, n::Integer; kw...) where {T<:Number}
    factored_glorot_normal(Random.default_rng(), T, m, n; kw...)
end
function factored_glorot_normal(rng::AbstractRNG, m::Integer, n::Integer; kw...)
    factored_glorot_normal(rng, Float32, m, n; kw...)
end
factored_glorot_normal(m::Integer, n::Integer; kw...) = factored_glorot_normal(Float32, m, n; kw...)


