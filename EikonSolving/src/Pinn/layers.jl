
struct RandomFourierLayerVar{T<:Number} <: AbstractExplicitLayer
    n::Int
    mhalf::Int
    σ::T
end

function RandomFourierLayerVar(n::Integer, m::Integer; σ::Real=1.0f0)
    if !iseven(m)
        throw(ArgumentError("RandomFourierLayerVar requires even output dimensions"))
    end
    RandomFourierLayerVar(n, fld(m,2), σ)
end

Lux.parameterlength(γ::RandomFourierLayerVar) = γ.n * γ.mhalf
Lux.statelength(::RandomFourierLayerVar) = 0

function Lux.initialparameters(rng::AbstractRNG, γ::RandomFourierLayerVar{T}) where {T}
    (B=rand(rng, Normal{T}(zero(T), γ.σ), γ.mhalf, γ.n),)
end

function (γ::RandomFourierLayerVar)(X::AbstractMatrix, params, state::NamedTuple)
    y = vcat(cos.(params.B*X), sin.(params.B*X))
    (y, state)
end


struct RandomFourierLayer{M<:AbstractMatrix} <: AbstractExplicitLayer
    B::M
end

function RandomFourierLayer(rng::AbstractRNG, dev::AbstractLuxDevice, n::Integer, m::Integer; σ::Real=1.0f0)
    if !iseven(m)
        throw(ArgumentError("RandomFourierLayer requires even output dimensions"))
    end
    B = rand(rng, Normal{typeof(σ)}(zero(σ), σ), fld(m,2), n) |> dev
    RandomFourierLayer(B)
end
function RandomFourierLayer(dev::AbstractLuxDevice, n::Integer, m::Integer; kw...)
    RandomFourierLayer(Random.default_rng(), dev, n, m; kw...)
end

Lux.parameterlength(γ::RandomFourierLayer) = 0
Lux.statelength(γ::RandomFourierLayer) = 0

Lux.initialparameters(rng::AbstractRNG, ::RandomFourierLayer) = NamedTuple()

function (γ::RandomFourierLayer)(X::AbstractMatrix, params, state)
    y = vcat(cos.(γ.B*X), sin.(γ.B*X))
    (y, state)
end


@concrete struct DiagFactorLayer <: AbstractExplicitLayer
    activation
    n::Int
    m::Int
    init_W
    init_b
end

function DiagFactorLayer(n::Integer, m::Integer, activation=tanh;
                         init_weight=factored_glorot_uniform,
                         init_bias=zeros32,
                         allow_fast_activation::Bool=true,
                        )
    act = allow_fast_activation ? NNlib.fast_act(activation) : activation
    DiagFactorLayer(act, n, m, init_weight, init_bias)
end

Lux.parameterlength(l::DiagFactorLayer) = (l.n+1)*l.m + l.n
Lux.statelength(l::DiagFactorLayer) = 0

function Lux.initialparameters(rng::AbstractRNG, l::DiagFactorLayer)
    W = l.init_W(rng, l.m, l.n)
    if !(W isa DiagFactorMatrix)
        error("DiagFactorLayer was initialized with wrong matrix type")
    end
    b = l.init_b(rng, l.m)
    (V=W.V, s=W.s, b=b)
end

function (l::DiagFactorLayer)(X::AbstractMatrix, params, state::NamedTuple)
    (V, s, b) = (params.V, params.s, params.b)
    # we do this "manually" because I don't want autodiff to fuck up
    y = l.activation.((V .* exp.(s)') * X .+ b)
    (y, state)
end


@concrete struct ModifiedChain <: AbstractExplicitContainerLayer{(:left, :right, :layers)}
    left
    right
    layers
end

function (mc::ModifiedChain)(X, θ, ψ::NamedTuple)
    (U, ψl) = mc.left(X, θ.left, ψ.left)
    (V, ψr) = mc.right(X, θ.right, ψ.right)
    y = X
    ψout = tuple()
    for (j, l) ∈ enumerate(mc.layers)
        (y, ψ′) = Lux.apply(l, y, θ.layers[j], ψ.layers[j])
        ψout = tuple(ψout..., ψ′)
        y = U .* y .+ V .* (1.0f0 .- y)
    end
    (y, (left=ψl, right=ψr, layers=ψout))
end

#====================================================================================================
TODO:

SympNet stuff seems like maybe it could be made to work in this application.
Unsurprisingly, it works very well out of the box for relatively short time steps (at least up
to quarter cycle for HO).  For longer durations it has interesting behavior that might be worth
studying carefully, for example with HO it struggles to get in a full cycle, but it tends to
look very much like a perfectly well behaved HO of longer period.

Performance is fucked, I think because AD through batched_vec is super inefficient.  Scales
favorably with bigger networks, but very badly with bigger input data.
====================================================================================================#


# this default simply scales weights by t; roughly what they did in SympNets paper
scaling_symp_inner(n::Integer) = Chain(
    Scale(n^2, gelu, init_weight=zeros32, use_bias=false),
    ReshapeLayer((n, n)),
)


struct SympLinearSubLayer{uplo,L<:AbstractExplicitLayer} <: AbstractExplicitContainerLayer{(:inner,)}
    n::Int
    inner::L  # this should *include reshaping*!

    function SympLinearSubLayer{uplo}(n::Integer, inner::AbstractExplicitLayer=scaling_symp_inner(n)) where {uplo}
        if uplo ∉ (:lower, :upper)
            throw(ArgumentError("invalid SympLinearSubLayer type"))
        end
        new{uplo,typeof(inner)}(n, inner)
    end
end

Lux.parameterlength(l::SympLinearSubLayer) = Lux.parameterlength(l.inner)
Lux.statelength(l::SympLinearSubLayer) = Lux.statelength(l.inner)

Lux.initialparameters(rng::AbstractRNG, l::SympLinearSubLayer) = Lux.initialparameters(rng, l.inner)

function (l::SympLinearSubLayer{:lower})((t, q, p), θ, ψ::NamedTuple)
    (A, ψ′) = Lux.apply(l.inner, t, θ, ψ)
    S = (A .+ PermutedDimsArray(A, (2,1,3))) ./ 2
    #WARN: seems like batched_vec is cause of the worst of the performance fuckage
    p′ = p + NNlib.batched_vec(S, q)
    q′ = q
    ((t, q′, p′), ψ′)
end
function (l::SympLinearSubLayer{:upper})((t, q, p), θ, ψ::NamedTuple)
    (A, ψ′) = Lux.apply(l.inner, t, θ, ψ)
    S = (A .+ PermutedDimsArray(A, (2,1,3))) ./ 2
    p′ = p
    q′ = NNlib.batched_vec(S, p) + q
    ((t, q′, p′), ψ′)
end


@concrete struct SympLinearLayer{NT<:NamedTuple} <: AbstractExplicitContainerLayer{(:sublayers,)}
    n::Int
    sublayers::NT
    init_b
end

function SympLinearLayer(n::Integer, layers::SympLinearSubLayer...; init_bias=zeros32)
    nt = ntuple(Val(length(layers))) do j
        Symbol("layer_$j")=>layers[j]
    end |> NamedTuple
    SympLinearLayer(n, nt, init_bias)
end
function SympLinearLayer(n::Integer, sl::Integer; init_bias=zeros32)
    subl = ntuple(Val(sl)) do j
        l = if isodd(j)
            SympLinearSubLayer{:lower}(n)
        else
            SympLinearSubLayer{:upper}(n)
        end
        Symbol("layer_$j")=>l
    end |> NamedTuple
    SympLinearLayer(n, subl, init_bias)
end

Lux.parameterlength(l::SympLinearLayer) = mapreduce(Lux.parameterlength, +, l.sublayers, init=0) + 2*l.n
Lux.statelength(l::SympLinearLayer) = mapreduce(Lux.statelength, +, l.sublayers, init=0)

function Lux.initialparameters(rng::AbstractRNG, l::SympLinearLayer)
    params = map(sl -> Lux.initialparameters(rng, sl), l.sublayers)
    (;params..., bq=l.init_b(rng, l.n), bp=l.init_b(rng, l.n))
end

@generated function _layers(layers::NamedTuple{names}, tqp, θ, ψ::NamedTuple) where {names}
    N = length(names)
    xsym = vcat([:tqp], [gensym() for _ in 1:N])
    ψsym = [gensym() for _ in 1:N]
    calls = map(1:N) do i
        quote
            ($(xsym[i+1]), $(ψsym[i])) = Lux.apply(
                layers.$(names[i]), $(xsym[i]), θ.$(names[i]), ψ.$(names[i]),
            )
        end
    end
    push!(calls, :(ψ = NamedTuple{$names}((($(Tuple(ψsym)...),)))))
    last = quote
        (t, q, p) = $(xsym[N+1])
        #TODO: paper recommends scaling these biases by t but I think it's worse for long times
        ((t, q .+ θ.bq, p .+ θ.bp), ψ)
    end
    push!(calls, last)
    Expr(:block, calls...)
end

(l::SympLinearLayer)(tqp, θ, ψ::NamedTuple) = _layers(l.sublayers, tqp, θ, ψ)

function default_symp_activation_inner(n::Integer)
    l1 = Dense(1, n, init_weight=zeros32, gelu)
    l2 = Dense(n, n, init_weight=zeros32, gelu)
    Chain(l1, l2)
end


struct SympActivationLayer{uplo,L<:AbstractExplicitLayer,A} <: AbstractExplicitContainerLayer{(:inner,)}
    n::Int
    inner::L
    activation::A

    function SympActivationLayer{uplo}(n::Integer,
            inner::AbstractExplicitLayer=default_symp_activation_inner(n),
            activation=gelu;
            allow_fast_activation::Bool=true,
        ) where {uplo}
        if uplo ∉ (:lower, :upper)
            throw(ArgumentError("invalid SympActivationLayer type"))
        end
        act = allow_fast_activation ? NNlib.fast_act(activation) : activation
        new{uplo,typeof(inner),typeof(act)}(n, inner, act)
    end
end

Lux.parameterlength(l::SympActivationLayer) = Lux.parameterlength(l.inner)
Lux.statelength(l::SympActivationLayer) = Lux.statelength(l.inner)

Lux.initialparameters(rng::AbstractRNG, l::SympActivationLayer) = Lux.initialparameters(rng, l.inner)

function (l::SympActivationLayer{:lower})((t, q, p), θ, ψ::NamedTuple)
    (a, ψ′) = Lux.apply(l.inner, t, θ, ψ)
    p′ = p
    q′ = a .* l.activation(p) + q
    ((t, q′, p′), ψ′)
end
function (l::SympActivationLayer{:upper})((t, q, p), θ, ψ::NamedTuple)
    (a, ψ′) = Lux.apply(l.inner, t, θ, ψ)
    p′ = p + a .* l.activation(q)
    q′ = q
    ((t, q′, p′), ψ′)
end


function defaultpinn(n::Integer, width::Integer, l::Integer, activation=gelu;
        device::AbstractLuxDevice=cpu_device(),
        σ::Real=1.0f0,
    )
    in = PairwiseFusion(vcat, NoOpLayer(), NoOpLayer())
    out = BranchLayer(SelectDim(1, 1:n), SelectDim(1, (n+1):(2*n)))
    inner = ntuple(Val(l)) do j
        DiagFactorLayer(width,width,activation)
    end
    Chain(in, RandomFourierLayer(device,2*n+1,width;σ), inner..., Dense(width,2*n), out)
end

function modifiedpinn(n::Integer, width::Integer, l::Integer, activation=gelu, cactivation=tanh;
        device::AbstractLuxDevice=cpu_device(),
        σ::Real=1.0f0,
    )
    in = PairwiseFusion(vcat, NoOpLayer(), NoOpLayer())
    out = BranchLayer(SelectDim(1, 1:n), SelectDim(1, (n+1):(2*n)))
    left = DiagFactorLayer(width,width,cactivation)
    right = DiagFactorLayer(width,width,cactivation)
    inner = ntuple(Val(l)) do j
        DiagFactorLayer(width,width,activation)
    end
    Chain(in, RandomFourierLayer(device,2*n+1,width;σ), ModifiedChain(left, right, inner), Dense(width,2*n), out)
end

