module Pinn

using LinearAlgebra, Random, Distributions, Statistics, StaticArrays, Functors, Optimisers
using ConcreteStructs, KernelAbstractions
using ChainRulesCore
using GPUArrays
using Lux, LuxCUDA
using ForwardDiff, Zygote

using TensorBoardLogger, LoggingExtras

using Lux.NNlib

using ForwardDiff: Dual

using Lux: AbstractExplicitLayer, AbstractExplicitContainerLayer
using Lux.LuxDeviceUtils: AbstractLuxDevice

using FastSymplecticIntegrators; const FSI = FastSymplecticIntegrators

using Infiltrator


abs4(x::Number) = abs2(x)^2

function pmean(𝒻, x::AbstractMatrix)
    l = size(x,2)
    sum(ξ -> 𝒻(ξ)/l, x)
end

_param_path_string(path::KeyPath) = join(map(string, path), "/")
_log_param_convert(x) = x
_log_param_convert(x::AbstractArray) = Vector(vec(x))

function logparamdict(θ)
    dict = Dict{String,Any}()
    fmap_with_path(θ) do path, node
        dict[_param_path_string(path)] = _log_param_convert(node)
    end
    dict
end

set_log_step!(logger::TBLogger, k::Integer) = (TensorBoardLogger.set_step!(logger, k); nothing)
set_log_step!(logger::AbstractLogger, ::Integer) = nothing


include("derivatives.jl")
include("arrays.jl")
include("rules.jl")
include("layers.jl")
include("loss.jl")
include("direct.jl")


end
