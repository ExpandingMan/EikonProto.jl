
struct PropagatorBatch{T<:AbstractMatrix}
    t::T
    q::T
    p::T
end

function _luxdevice(D::AbstractLuxDevice, batch::PropagatorBatch)
    PropagatorBatch(D(batch.t), D(batch.q), D(batch.p))
end
(D::LuxCUDADevice)(batch::PropagatorBatch) = _luxdevice(D, batch)

Base.length(b::PropagatorBatch) = length(b.t)
Base.eltype(b::PropagatorBatch) = eltype(b.t)
ndimensions(b::PropagatorBatch) = size(b.q,1)

# these are real views on GPU
function Base.view(b::PropagatorBatch, cols::AbstractUnitRange)
    PropagatorBatch(view(b.t, :, cols), view(b.q, :, cols), view(b.p, :, cols))
end

function Base.hcat(b1::PropagatorBatch, b2::PropagatorBatch)
    PropagatorBatch(hcat(b1.t, b2.t), hcat(b1.q, b2.q), hcat(b1.p, b2.p))
end

function onbatch(model, b::PropagatorBatch, mparams)
    (qp, ψ) = model((b.t, b.q, b.p), mparams...)
    (PhaseSpaceBatch(qp), ψ)
end

function onbatchtimediff(model, b::PropagatorBatch, mparams)
    (t, q, p) = (b.t, b.q, b.p)
    (qp, ψ) = timediff(t) do τ
        model((τ, q, p), mparams...)  
    end
    (PhaseSpaceBatch(qp), ψ)
end


struct PhaseSpaceBatch{T<:AbstractMatrix}
    # we have to keep these separate because views would be non-contiguous
    q::T
    p::T
end

PhaseSpaceBatch((q,p)::Tuple) = PhaseSpaceBatch(q,p)

_luxdevice(D::AbstractLuxDevice, batch::PhaseSpaceBatch) = PhaseSpaceBatch(D(batch.q), D(batch.p))
(D::LuxCUDADevice)(batch::PhaseSpaceBatch) = _luxdevice(D, batch)

Base.length(b::PhaseSpaceBatch) = size(b.q,2)
Base.eltype(b::PhaseSpaceBatch) = eltype(b.q)
ndimensions(b::PhaseSpaceBatch) = size(b.q,1)

PhaseSpaceBatch(batch::PropagatorBatch) = PhaseSpaceBatch(batch.q, batch.p)


struct HamTrainBatch{B<:PropagatorBatch}
    batch::B

    # we always require s = 0 to be in first slot

    n_samples::Int  # number of distinct boundary conditions
    n_times::Int  # number of temporal points
    M::Int  # n temporal segments

    seglen::Int  # cld(n_times, M)
end

ndimensions(batch::HamTrainBatch) = ndimensions(batch.boundary)
Base.length(batch::HamTrainBatch) = length(batch.batch)

function _luxdevice(D::AbstractLuxDevice, b::HamTrainBatch)
    HamTrainBatch(D(b.batch), b.n_samples, b.n_times, b.M, b.seglen)
end
(D::LuxCUDADevice)(batch::HamTrainBatch) = _luxdevice(D, batch)

batchview(batch::HamTrainBatch, cols::AbstractUnitRange) = view(batch.batch, cols)

function temporalindices(batch::HamTrainBatch, n::Integer, m::Integer=n)
    ns = batch.n_samples
    nt = batch.n_times
    l = length(batch)
    ((n-1)*ns + 1):min(length(batch), m*ns)
end

temporalview(batch::HamTrainBatch, n::Integer, m::Integer=n) = batchview(batch, temporalindices(batch, n, m))
boundaryview(batch::HamTrainBatch) = batchview(batch, temporalindices(batch, 1))

function temporal_segment_view_indices(batch::HamTrainBatch, n::Integer, m::Integer=n)
    nseg = batch.seglen
    ((n-1)*nseg+1, min(m*nseg, batch.n_times))
end

function temporalsegmentindices(batch::HamTrainBatch, n::Integer, m::Integer=n)
    if n > batch.M || m > batch.M
        throw(ArgumentError("invalid segment view"))
    end
    temporalindices(batch, temporal_segment_view_indices(batch, n, m)...)
end

function temporalsegmentview(batch::HamTrainBatch, n::Integer, m::Integer=n)
    batchview(batch, temporalsegmentindices(batch, n, m))
end

function _make_hambatch_tbuffer(ns::Integer, ts)
    ts = sort!(collect(ts))
    o = Matrix{eltype(ts)}(undef, 1, ns*(length(ts)+1))
    o[1,1:ns] .= zero(eltype(ts))
    for j ∈ 2:(length(ts)+1)
        o[1,((j-1)*ns+1):(j*ns)] .= ts[j-1]
    end
    o
end

# ts may or may not include 0
function HamTrainBatch((q, p), ts, M::Integer=1)
    ns = size(q,2)
    nt = length(ts) + 1
    q0 = repeat(q, 1, nt)
    p0 = repeat(p, 1, nt)
    t = _make_hambatch_tbuffer(ns, ts)
    b = PropagatorBatch(t, q0, p0)
    HamTrainBatch(b, ns, nt, M, cld(nt, M))
end



@kwdef struct HamiltonianLossParams
    α::Float32 = 0.9f0
    ϵ::Float32 = 1.0f0
    M::Int = 4  # number of temporal segments
    max_updated_weight::Float32 = 10.0f0
    weight_update_interval::Int = 1000
    log_interval::Int = 100
end


@concrete mutable struct HamiltonianLoss{n,T<:Number}
    # these will require NN to initialize
    λc::T
    λr::T
    λsh::T

    H
    ∂qH
    ∂pH

    params::HamiltonianLossParams

    opt_state

    k::Int  # iteration
end

ndimensions(::HamiltonianLoss{n}) where {n} = n

function boundaryloss(ℓ::HamiltonianLoss, model, batch::PropagatorBatch, mparams)
    (qp′, _) = onbatch(model, batch, mparams)
    (q′, p′) = (qp′.q, qp′.p)
    q0p0 = PhaseSpaceBatch(batch)
    (q0, p0) = (q0p0.q, q0p0.p)
    mean(abs2, q′ .- q0) + mean(abs2, p′ .- p0)
end
function boundaryloss(ℓ::HamiltonianLoss, model, hb::HamTrainBatch, mparams)
    boundaryloss(ℓ, model, boundaryview(hb), mparams)
end

function _withresidualloss(ℓ::HamiltonianLoss{n}, model, batch::PropagatorBatch, mparams) where {n}
    (qp, _) = onbatch(model, batch, mparams)
    (qpdot, _) = onbatchtimediff(model, batch, mparams)
    (q, p) = (qp.q, qp.p)
    (q̇, ṗ) = (qpdot.q, qpdot.p)
    #WARN: these still don't really broadcast correctly
    l1 = mean(abs2, ℓ.∂qH(q, p) .+ ṗ, dims=1)
    l2 = mean(abs2, ℓ.∂pH(q, p) .- q̇, dims=1)
    (qp, l1 + l2)
end
function _withresidualloss(ℓ::HamiltonianLoss, model, hb::HamTrainBatch, mparams)
    _withresidualloss(ℓ, model, hb.batch, mparams)
end

function _resid_tempweight_sum(hb::HamTrainBatch, l::AbstractMatrix, k::Integer)
    w = similar(l, 1, hb.n_samples)
    w .= zero(eltype(w))
    for j ∈ 1:(k-1)
        (a, b) = temporal_segment_view_indices(hb, j)
        for i ∈ a:b
            tidx = temporalindices(hb, i)
            w .+= view(l, :, tidx)
        end
    end
    w
end

function _tempweights_from_resid(hbatch::HamTrainBatch, l::AbstractMatrix, k::Integer; ϵ::Real=1.0f0)
    ϵ = convert(eltype(l), ϵ)
    w = _resid_tempweight_sum(hbatch, l, k)
    w .= exp.(-ϵ .* w)
end

function _with_residualloss_tempweights(ℓ::HamiltonianLoss, model, hb::HamTrainBatch, mparams)
    (qp, l) = _withresidualloss(ℓ, model, hb, mparams)
    ignore_derivatives() do
        for j ∈ 1:ℓ.params.M
            w = _tempweights_from_resid(hb, l, j; ϵ=ℓ.params.ϵ)
            (a, b) = temporal_segment_view_indices(hb, j)
            for i ∈ a:b
                tidx = temporalindices(hb, i)
                l[:, tidx] .*= w
            end
        end
    end
    (qp, l)
end

function withresidualloss(ℓ::HamiltonianLoss, model, hb::HamTrainBatch, mparams)
    (qp, l) = if ℓ.params.M > 1
        _with_residualloss_tempweights(ℓ, model, hb, mparams)
    else
        _withresidualloss(ℓ, model, hb, mparams)
    end
    (qp, mean(abs2, l))
end
function residualloss(ℓ::HamiltonianLoss, model, hb::HamTrainBatch, mparams)
    withresidualloss(ℓ, model, hb, mparams)[2]
end


# should take as an argument qp evaluated for residual
function conservationloss(ℓ::HamiltonianLoss{n}, model, batch::PropagatorBatch, qp::PhaseSpaceBatch) where {n}
    (q, p) = (qp.q, qp.p)
    (q0, p0) = (batch.q, batch.p)
    mean(abs2, ℓ.H(q, p) .- ℓ.H(q0, p0))
end
function conservationloss(ℓ::HamiltonianLoss, model, batch::HamTrainBatch, mparams)
    (qp, _) = onbatch(model, batch.batch, mparams)
    conservationloss(ℓ, model, batch.batch, qp)
end

#TODO: this nowhere near done
function HamiltonianLoss{n}(H, ∂qH, ∂pH, params::HamiltonianLossParams, opts) where {n}
    HamiltonianLoss{n}(
        10.0f0, 1.0f0, 10.0f0,
        H, ∂qH, ∂pH,
        params,
        opts,
        0,
    )
end

# probably temporary
function reset!(ℓ::HamiltonianLoss)
    ℓ.k = 0
    ℓ
end

# a recursive sum of squares we need for gradients
recsumsquares(::Nothing) = 0.0f0  #TODO: this is a little scary...
recsumsquares(x::AbstractArray) = sum(abs2, x)
recsumsquares(x::Tuple) = sum(recsumsquares, x)
recsumsquares(x::NamedTuple) = sum(recsumsquares, x)

# this method because Zygote returns some nothing
_weighted_sum(::HamiltonianLoss, ::Nothing, ::Nothing, ::Nothing) = nothing
_weighted_sum(ℓ::HamiltonianLoss, l1, l2, l3) = ℓ.λc*l1 + ℓ.λr*l2 + ℓ.λsh*l3
_weighted_sum(::HamiltonianLoss, ::Nothing) = nothing
_weighted_sum(ℓ::HamiltonianLoss, l1) = ℓ.λc*l1

function _update_weights!(ℓ::HamiltonianLoss, grads)
    @info("updating weights...")
    ss = map(recsumsquares, grads)
    tot = sum(ss)
    λnew = min.(ℓ.params.max_updated_weight, tot ./ ss)
    α = ℓ.params.α
    ℓ.λc = α*ℓ.λc + (1 - α)*λnew[1]
    ℓ.λr = α*ℓ.λr + (1 - α)*λnew[2]
    ℓ.λsh = α*ℓ.λsh + (1 - α)*λnew[3]
    nothing
end

function withgradient!(ℓ::HamiltonianLoss, model, batch::HamTrainBatch, (θ, ψ); no_modify::Bool=false)
    params = ℓ.params
    lboundary = Zygote.withgradient(θ) do ϑ
        boundaryloss(ℓ, model, batch, (ϑ, ψ))
    end
    lresid = Zygote.withgradient(θ) do ϑ
        residualloss(ℓ, model, batch, (ϑ, ψ))  
    end
    lcons = Zygote.withgradient(θ) do ϑ
        conservationloss(ℓ, model, batch, (ϑ, ψ))  
    end
    ls = (lboundary.val, lresid.val, lcons.val)
    grads = (lboundary.grad, lresid.grad, lcons.grad)
    no_modify || (mod1(ℓ.k, params.weight_update_interval) == 1 && _update_weights!(ℓ, grads))
    (∇ℓ,) = fmap(grads...) do g1, g2, g3
        _weighted_sum(ℓ, g1, g2, g3)
    end
    l = _weighted_sum(ℓ, ls...)
    if mod1(ℓ.k, params.log_interval) == 1
        @info("boundary loss: $(ls[1]); residual loss: $(ls[2]); conservation loss: $(ls[3]); total: $l",
            #recsumsquares(∇ℓ)
        )
    end
    (l, ∇ℓ)
end
function withgradient(ℓ::HamiltonianLoss, model, hb::HamTrainBatch, (θ, ψ))
    withgradient!(ℓ, model, hb, (θ, ψ), no_modify=true)
end

function (ℓ::HamiltonianLoss)(model, hb::HamTrainBatch, (θ, ψ))
    lboundary = boundaryloss(ℓ, model, hb, (θ, ψ))
    lresid = residualloss(ℓ, model, hb, (θ, ψ))
    lcons = conservationloss(ℓ, model, hb, (θ, ψ))
    _weighted_sum(ℓ, lboundary, lresid, lcons)
end

function update!(ℓ::HamiltonianLoss, model, batch::HamTrainBatch, (θ, ψ))
    ℓ.k += 1
    (l, ∇ℓ) = withgradient!(ℓ, model, batch, (θ, ψ))
    (opts, θ) = Optimisers.update!(ℓ.opt_state, θ, ∇ℓ)
    ℓ.opt_state = opts
    θ
end

function train!(ℓ::HamiltonianLoss, model, batch::HamTrainBatch, (θ, ψ), n::Integer)
    for j ∈ 1:n
        update!(ℓ, model, batch, (θ, ψ))
    end
    model
end
