
struct TimeDiffTag end


function dual(a::Real, b::Real=zero(typeof(a)))
    (a, b) = promote(a, b)
    Dual{TimeDiffTag}(a, b)
end

realpart(x::Dual) = x.value
realpart(x::Number) = x

dualpart(x::Dual) = x.partials[1]
dualpart(x::Number) = x


recdual(x::Number) = dual(x, one(x))
recdual(x::AbstractArray) = recdual.(x)

recrealpart(x::Number) = realpart(x)
recrealpart(x::AbstractArray) = recrealpart.(x)
recrealpart(x::Tuple) = map(recrealpart, x)
recrealpart(x::NamedTuple) = map(recrealpart, x)

recdualpart(x::Number) = dualpart(x)
recdualpart(x::AbstractArray) = recdualpart.(x)
recdualpart(x::Tuple) = map(recdualpart, x)
recdualpart(x::NamedTuple) = map(recdualpart, x)


# this method exists as helper in rrule
_dualwithtimediff(𝒻, t) = 𝒻(recdual(t))


"""
    timediff(𝒻, t)

Compute the derivative of `𝒻` with respect to `t`.

This function only returns the derivative, and not the primal, because this makes it vastly simpler to compute
derivatives involving calls to `timediff`.
"""
function timediff(𝒻, t)
    y = _dualwithtimediff(𝒻, t)
    recdualpart(y)
end
