
@kwdef struct DirectTrainerParams
    n_batch_samples::Int = 256
    batch_iter::Int = 10
    log_interval::Int = 10
    log_model_interval::Int = 1000
end

@concrete terse mutable struct DirectTrainer{n,R<:AbstractRNG,I<:FastSymplecticIntegrator,TR<:AbstractRange}
    rng::R

    device

    sample_boundary

    integrator::I

    temporal_range::TR

    model
    opt_state
    model_θ
    model_ψ

    logger

    params::DirectTrainerParams
    k::Int  # iteration
end

function DirectTrainer{n}(
        dev::LuxDeviceUtils.AbstractLuxDevice,
        rng::AbstractRNG,
        sampler, int::FastSymplecticIntegrator, trange::AbstractRange,
        model, opt::AbstractRule;
        logger::AbstractLogger=global_logger(),
        kw...
    ) where {n}
    params = DirectTrainerParams(;kw...)
    (θ, ψ) = Lux.setup(rng, model) |> dev
    opts = Optimisers.setup(opt, θ)
    DirectTrainer{n}(
        rng, dev,
        sampler, int, trange,
        model, opts, θ, ψ,
        logger,
        params,
        0,
    )
end

# note that training in general is *not* independent of the ordering here
function _make_direct_propagator_batch(tr::DirectTrainer{n}, paths) where {n}
    nt = length(tr.temporal_range)
    ns = length(paths)
    t = Matrix{Float32}(undef, 1, nt*ns)
    q = Matrix{Float32}(undef, n, nt*ns)
    p = Matrix{Float32}(undef, n, nt*ns)
    for (j, ((q0, p0), _)) ∈ enumerate(paths)
        for k ∈ 1:nt
            idx = (j-1)*nt + k
            t[1,idx] = tr.temporal_range[k]
            q[:,idx] .= q0
            p[:,idx] .= p0
        end
    end
    PropagatorBatch(t, q, p)
end
()
function _make_direct_phase_space_batch(tr::DirectTrainer{n}, paths) where {n}
    nt = length(tr.temporal_range)
    ns = length(paths)
    q = Matrix{Float32}(undef, n, nt*ns)
    p = Matrix{Float32}(undef, n, nt*ns)
    for (j, ((_, _), path)) ∈ enumerate(paths)
        for k ∈ 1:nt
            idx = (j-1)*nt + k
            q[:,idx] .= path[k][1]
            p[:,idx] .= path[k][2]
        end
    end
    PhaseSpaceBatch(q, p)
end

function makebatches(rng::AbstractRNG, tr::DirectTrainer, n::Integer)
    paths = map(1:n) do j
        (q0, p0) = tr.sample_boundary(rng)
        path = FSI.trajectory(tr.integrator, q0, p0, tr.temporal_range)
        ((q0, p0), path)
    end
    X = _make_direct_propagator_batch(tr, paths)
    Y = _make_direct_phase_space_batch(tr, paths)
    (X, Y) |> tr.device
end
makebatches(tr::DirectTrainer, n::Integer=tr.params.n_batch_samples) = makebatches(tr.rng, tr, n)

function loss(tr::DirectTrainer, X::PropagatorBatch, Y::PhaseSpaceBatch, (θ,ψ)=(tr.model_θ, tr.model_ψ))
    (qp, _) = onbatch(tr.model, X, (θ, ψ))
    pmean(abs4, qp.q - Y.q) + pmean(abs4, qp.p - Y.p)
end

set_log_step!(tr::DirectTrainer, k::Integer=tr.k) = set_log_step!(tr.logger, k)

#WARN: this whole mess isn't done; it's to extract optimizer state from opt state
function _extract_optimizer(opts)
    #FUCK: uh... this is going to be really ugly because functors doesn't exactly make it easy
    #for us to get one of these leaves
end

function updateone!(tr::DirectTrainer, (X, Y)=makebatches(tr))
    tr.k += 1
    (l, (∇l,)) = Zygote.withgradient(tr.model_θ) do ϑ
        loss(tr, X, Y, (ϑ, tr.model_ψ))
    end
    (opts, θ) = Optimisers.update!(tr.opt_state, tr.model_θ, ∇l)
    tr.opt_state = opts
    tr.model_θ = θ
    mod1(tr.k, tr.params.log_interval) == 1 && with_logger(tr.logger) do
        set_log_step!(tr)
        @info("training", loss=l, log_step_increment=0)
    end
    mod1(tr.k, tr.params.log_model_interval) == 1 && with_logger(tr.logger) do
        set_log_step!(tr)
        dict = logparamdict(tr.model_θ)
        gdict = logparamdict(∇l)
        @info("model", params=dict, log_step_increment=0)
        @info("gradients", gradient=gdict, log_step_increment=0)
    end
    l
end

function update!(tr::DirectTrainer, n::Integer, (X, Y)=makebatches(tr))
    for j ∈ 1:n
        updateone!(tr, (X, Y))
    end
    tr
end

function train!(tr::DirectTrainer, n::Integer; batch_iter::Integer=tr.params.batch_iter)
    for j ∈ 1:n
        (X, Y) = makebatches(tr)
        update!(tr, batch_iter, (X, Y))
    end
    tr
end
