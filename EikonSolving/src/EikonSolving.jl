module EikonSolving

using LinearAlgebra
using StaticArrays
using Symbolics, Symbolics.SymbolicUtils
using LorentzGroup

import SciMLBase
using SciMLBase: ODESolution, ODEProblem, DynamicalODEProblem, AbstractODEAlgorithm


include("hamiltoniandiffeq/HamiltonianDiffEq.jl")
using .HamiltonianDiffEq; const HDEq = HamiltonianDiffEq
using .HamiltonianDiffEq: getsymname, symparams, phasespacevars, timespan

include("Pinn/Pinn.jl")
using .Pinn


using Infiltrator


const Vec4{T} = StaticVector{4,T}

#====================================================================================================
TODO:

There is a lot of messy re-implementation of stuff we did much more nicely in DiffGeo.

I have decided that I'm just going to have to live with that until we have a real working example,
because all this stuff has to get plugged back into DiffGeo, I don't want to put in the effort
of organizing everything nicely before I have a really good idea what the result should look like.
====================================================================================================#


struct Gradient{T<:Number}
    x::Vec4{T}
end

(∂::Gradient)(𝒻) = @SVector [simplify(Symbolics.derivative(𝒻, ∂.x[j])) for j ∈ 1:4]

# I don't see this in Symboilcs

# this includes the coordinate system
abstract type Spacetime{T<:Number} end

issymbolic(::Spacetime) = false
issymbolic(M::Spacetime{<:Num}) = true

basetype(M::Spacetime) = basetype(typeof(M))

HamiltonianDiffEq.symparams(M::Spacetime) = params(M) |> symparams
HamiltonianDiffEq.symparams(::Type{T}, M::Spacetime) where {T<:Union{NamedTuple,Dict}} = symparams(T, params(M))

function symbolic(M::Spacetime)
    ps = symparams(NamedTuple, M)
    basetype(M)(;ps...)
end


paramvals(M::Spacetime) = ntuple(j -> getfield(M, j), nfields(M))

params(M::Spacetime) = NamedTuple{fieldnames(typeof(M))}(paramvals(M))

function mass²(M::Spacetime, x::AbstractVector, p::AbstractVector)
    ginv = invmetric(M, x)
    # note that YOU put this minus sign in here which might later be confusing
    -p' * ginv * p
end

function mass²(M::Spacetime, sol::ODESolution, s::Real)
    (x, p) = evalsol(sol, s)
    mass²(M, x, p)
end


hamiltonian(M::Spacetime, x::Vec4, p::Vec4) = p' * invmetric(M, x) * p / 2

function ∂hamiltonian(M::Spacetime, x::Vec4, p::Vec4)
    H0 = hamiltonian(M, x, p)
    ∂ = Gradient(x)
    ∂(H0)
end

struct System{M<:Spacetime,S}
    spacetime::M
    system::S
end

# argument here should be non-symbolic I guess
function System(M::Spacetime)
    (s, x, p) = phasespacevars(Val(4))
    Ms = symbolic(M)
    Hs = hamiltonian(Ms, x, p)
    sys = HamiltonianSystem{true}(Hs, s, x, p, paramvals(Ms))
    System(M, sys)
end

Spacetime(S::System) = S.spacetime

# DynamicalODEProblem is a "fake" type
problem(S::System, x0, p0, range) = DynamicalODEProblem(S.system, x0, p0, range)


struct Solution{M<:Spacetime,S<:System,HS}
    spacetime::M
    system::S
    solution::HS
end

function Solution(sys::System{M}, prob::ODEProblem,
                  alg::AbstractODEAlgorithm=Tao2016();
                  dt::Real=0.01,  # placeholder; might not be a reasonable default
                  kw...
                 ) where {M<:Spacetime}
    sol = HamiltonianSolution(sys.system, SciMLBase.solve(prob, alg; dt, kw...))
    Solution{M,typeof(sys),typeof(sol)}(sys.spacetime, sys, sol)
end

function Solution(sys::System{M}, x0, p0, tspan,
                  alg::AbstractODEAlgorithm=Tao2016();
                  kw...
                 ) where {M<:Spacetime}
    prob = problem(sys, x0, p0, tspan)
    Solution(sys, prob, alg; kw...)
end

(sol::Solution)(s::Real) = sol.solution(s)

function mass²(sol::Solution, s::Real=0.0)
    (x, p) = sol(s)
    mass²(sol.spacetime, x, p)
end

HamiltonianDiffEq.timespan(sol::Solution) = timespan(sol.solution)

mass²s(sol::Solution) = [mass²(sol, s) for s ∈ timespan(sol)]    


#TODO: initial conditions stuff
# is there a general way of doing conserved quantities?

include("utils.jl")
include("minkowski_cartesian.jl")
include("schwarzschild_polar.jl")
include("schwarzschild_isotropic.jl")
include("schwarzschild_schild.jl")


end # module EikonSolving
