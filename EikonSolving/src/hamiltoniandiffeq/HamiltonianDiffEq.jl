module HamiltonianDiffEq

using LinearAlgebra
using StaticArrays
using Symbolics
using RuntimeGeneratedFunctions
import SciMLBase, DiffEqBase, CommonSolve

using SciMLBase: AbstractDEProblem, AbstractODEAlgorithm
using SciMLBase: ODESolution, DynamicalODEFunction

RuntimeGeneratedFunctions.init(@__MODULE__)


getsymname(x::Num) = x.val.name

function symparam(name::Symbol, default::Real)
    sym = Sym{Real}(name)
    sym = Symbolics.setdefaultval(sym, default)
    sym = SymbolicUtils.setmetadata(sym, Symbolics.VariableSource, (:parameters, name))
    #WARN: this will have to be replaced since we don't want to use ModelingToolkit
    ModelingToolkit.toparam(Symbolics.wrap(sym))
end

function symparams(nt::NamedTuple)
    ntuple(length(nt)) do j
        (k, v) = (keys(nt)[j], values(nt)[j])
        symparam(k, v)
    end
end

symparams(::Type{NamedTuple}, nt::NamedTuple) = NamedTuple(getsymname(x)=>x for x ∈ symparams(nt))
symparams(::Type{Dict}, nt::NamedTuple) = Dict(symparams(NamedTuple, nt))

function phasespacevars(::Val{n}) where {n}
    @variables t q(t)[1:n] p(t)[1:n]
    q = convert(SVector{n}, Symbolics.scalarize(q))
    p = convert(SVector{n}, Symbolics.scalarize(p))
    (t, q, p)
end


include("hamiltoniansystem.jl")
include("tao_integrator.jl")


export HamiltonianSystem, HamiltonianSolution, Tao2016


end
