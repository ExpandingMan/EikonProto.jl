
struct HamiltonianSystem{IH,QP,Prs} #<: AbstractODESystem  (not going to attempt this right now)
    H::Num 
    t::Num  # time parameter
    q::QP
    p::QP
    params::Prs  # should make this a tuple I think
end

ishamiltonian(::HamiltonianSystem{IH}) where {IH} = IH

function HamiltonianSystem{IH}(H::Num, t::Num, q, p, params) where {IH}
    (q, p) = promote(q, p)
    HamiltonianSystem{IH,typeof(q),typeof(params)}(H, t, q, p, params)
end

function HamiltonianSystem{IH}(𝒻, val::Val{n}, params=()) where {IH,n}
    ps = symparams(NamedTuple, params)
    (t, q, p) = phasespacevars(val)
    H = 𝒻(q, p, t, ps)
    HamiltonianSystem{IH}(H, t, q, p, values(ps))
end
HamiltonianSystem{IH}(𝒻, n::Integer, params=()) where {IH} = HamiltonianSystem{IH}(𝒻, Val(n), params)


symbolic_∂qH(sys::HamiltonianSystem) = Symbolics.gradient(sys.H, sys.q)
symbolic_∂pH(sys::HamiltonianSystem) = Symbolics.gradient(sys.H, sys.p)

substituteparams(sys::HamiltonianSystem, expr) = substitute(expr, paramdict(sys))

#TODO: going to have to pass default params to DynamicalODEProblem
paramdict(sys::HamiltonianSystem) = Dict(r=>Symbolics.getdefaultval(r) for r ∈ sys.params)

# this method allows us to set some of the params
paramdict(sys::HamiltonianSystem, newparams) = merge(paramdict(sys), newparams)

paramdefaults(sys::HamiltonianSystem) = collect(values(paramdict(sys)))

# these don't depend on ishamiltonian because of how DynamicalODEProblem works
function make_pdot_func(sys::HamiltonianSystem)
    ∂qH = -symbolic_∂qH(sys)
    expr = build_function(∂qH, sys.q, sys.p, sys.t, sys.params)[2]
    f = @RuntimeGeneratedFunction(expr)
    (o, q, p, params, t) -> f(o, q, p, t, params)
end
function make_qdot_func(sys::HamiltonianSystem)
    ∂pH = symbolic_∂pH(sys)
    expr = build_function(∂pH, sys.q, sys.p, sys.t, sys.params)[2]
    f = @RuntimeGeneratedFunction(expr)
    (o, q, p, params, t) -> f(o, q, p, t, params)
end

function energyfunction(sys::HamiltonianSystem{false}, params::AbstractDict=Dict())
    params = paramdict(sys, params)
    H = substitute(sys.H, params)
    expr = build_function(H, sys.q, sys.p, sys.t)
    @RuntimeGeneratedFunction(expr)
end
function energyfunction(sys::HamiltonianSystem{true}, params::AbstractDict=Dict())
    params = paramdict(sys, params)
    H = substitute(sys.H, params)
    expr = build_function(H, sys.q, sys.p)
    @RuntimeGeneratedFunction(expr)
end


function SciMLBase.DynamicalODEFunction(sys::HamiltonianSystem)
    f1 = make_qdot_func(sys)
    f2 = make_pdot_func(sys)
    DynamicalODEFunction(f1, f2)
end

#TODO: parameters aren't handled right here, need to be able to provide them

function SciMLBase.DynamicalODEProblem(sys::HamiltonianSystem, qinit, pinit, tspan, params=())
    func = DynamicalODEFunction(sys)
    params = isempty(params) ? paramdefaults(sys) : params
    DynamicalODEProblem(func, qinit, pinit, tspan, params)
end


# this is purely for convenience, doesn't do much
struct HamiltonianSolution{IH,Sy<:HamiltonianSystem{IH},S<:ODESolution,EF}
    sys::Sy
    sol::S
    E::EF
end

function HamiltonianSolution(sys::HamiltonianSystem{IH}, sol::ODESolution) where {IH}
    E = energyfunction(sys)
    HamiltonianSolution{IH,typeof(sys),typeof(sol),typeof(E)}(sys, sol, E)
end

function (sol::HamiltonianSolution)(t::Real)
    (q, p) = sol.sol(t).x
    (q, p)
end

energyfunction(sol::HamiltonianSolution) = sol.E

function energy(sol::HamiltonianSolution{true}, t::Real)
    (q, p) = sol(t)
    sol.E(q, p)
end
function energy(sol::HamiltonianSolution{false}, t::Real)
    (q, p) = sol(t)
    sol.E(q, p, t)
end

timespan(sol::HamiltonianSolution) = sol.sol.t

energies(sol::HamiltonianSolution) = [energy(sol, t) for t ∈ timespan(sol)]
