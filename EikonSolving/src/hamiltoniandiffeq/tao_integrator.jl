
# used https://github.com/SciML/SimpleDiffEq.jl as ref


# we follow SimpleDiffEq for interface details
struct Tao2016 <: AbstractODEAlgorithm end


mutable struct TaoIntegrator{IIP,F<:DynamicalODEFunction,Z,PR,R<:Real}
    l::Int  # order

    f::F  # contains ∂H

    #TODO: for now we will probably always just convert to Vector
    q0::Z  # we convert inputs if necessary
    p0::Z

    q::Z
    x::Z
    p::Z
    y::Z

    # where we store results of evaluated functions
    ∂qH::Z
    ∂xH::Z
    ∂pH::Z
    ∂yH::Z

    params::PR # system params

    n::Int  # iteration number

    # time parameter
    t::R

    # params
    ω::R
    h::R
end

#TODO: actually, this absolutely sucks, can we get rid of it?
DiffEqBase.isinplace(::TaoIntegrator{IIP}) where {IIP} = IIP

# of configuration space, total dims 2ndims
ndims(int::TaoIntegrator) = length(int.q)

# these help us keep track fo the confusing f1, f2 names
function eval_∂qH!(int::TaoIntegrator{true})
    int.f.f2(int.∂qH, int.q, int.y, int.params, int.t)
    int.∂qH .= .- int.∂qH  # fix wrong sign from f2
end
function eval_∂yH!(int::TaoIntegrator{true})
    int.f.f1(int.∂yH, int.q, int.y, int.params, int.t)
end
function eval_∂pH!(int::TaoIntegrator{true})
    int.f.f1(int.∂pH, int.x, int.p, int.params, int.t)
end
function eval_∂xH!(int::TaoIntegrator{true})
    int.f.f2(int.∂xH, int.x, int.p, int.params, int.t)
    int.∂xH .= .- int.∂xH  # fix wrong sign from f2
end

function eval_∂qH!(int::TaoIntegrator{false})
    int.∂qH .= .- int.f.f2(int.q, int.y, int.params, int.t)
end
function eval_∂yH!(int::TaoIntegrator{false})
    int.∂yH .= int.f.f1(int.q, int.y, int.params, int.t)
end
function eval_∂pH!(int::TaoIntegrator{false})
    int.∂pH .= int.f.f1(int.x, int.p, int.params, int.t)
end
function eval_∂xH!(int::TaoIntegrator{false})
    int.∂xH .= .- int.f.f2(int.x, int.p, int.params, int.t)
end

function _Astep!(int::TaoIntegrator, h::Real=int.h)
    eval_∂qH!(int)
    int.p .-= h .* int.∂qH
    eval_∂yH!(int)
    int.x .+= h .* int.∂yH
    int
end

function _Bstep!(int::TaoIntegrator, h::Real=int.h)
    eval_∂pH!(int)
    int.q .+= h .* int.∂pH
    eval_∂xH!(int)
    int.y .-= h .* int.∂xH
    int
end

function _Cstep!(int::TaoIntegrator, h::Real=int.h)
    (q, p, x, y) = (int.q, int.p, int.x, int.y)
    # this looks strange because of our notation, but we are merely using these as buffers
    (q′, p′, x′, y′) = (int.∂qH, int.∂pH, int.∂xH, int.∂yH)
    (s, c) = sincos(2*int.ω*h)

    @. begin
        q′ = (q + x) + c*(q - x) + s*(p - y)
    end
    @. begin
        p′ = (p + y) - s*(q - x) + c*(p - y)
    end
    @. begin
        x′ = (q + x) - c*(q - x) - s*(p - y)
    end
    @. begin
        y′ = (p + y) + s*(q - x) - c*(p - y)
    end

    @. begin
        int.q = q′/2
        int.p = p′/2
        int.x = x′/2
        int.y = y′/2
    end

    int
end

_γ(l::Integer) = 1/(2 - 2^(1/(l + 1)))

function ϕ!(int::TaoIntegrator, l::Integer=int.l, h::Real=int.h)
    if l == 2
        _Astep!(int, h/2)
        _Bstep!(int, h/2)
        _Cstep!(int, h)
        _Bstep!(int, h/2)
        _Astep!(int, h/2)
    else
        γ = _γ(l)
        ϕ!(int, l - 2, γ*h)
        ϕ!(int, l - 2, (1 - 2*γ)*h)
        ϕ!(int, l - 2, γ*h)
    end
    int
end

function CommonSolve.step!(int::TaoIntegrator)
    ϕ!(int)
    int.n += 1
    nothing
end

function _tao_init(prob::AbstractDEProblem;
                   l::Integer=2,
                   ω::Real=1.0,
                   kw...
                  )
    if l < 2
        throw(ArgumentError("Tao integrator must be of order l ≥ 2"))
    end
    kwargs = merge(NamedTuple(prob.kwargs), merge(NamedTuple(kw), (ω=ω,)))
    q0 = prob.u0.x[1]
    p0 = prob.u0.x[2]
    params = prob.p
    n = length(q0)
    if n ≠ length(p0)
        throw(ArgumentError("provided positions and momenta are of different dimensionality"))
    end
    cType = Vector{promote_type(float(eltype(q0)), float(eltype(p0)))}
    (q0, p0) = convert.(cType, (q0, p0))
    (q, x, p, y) = copy.((q0, q0, p0, p0))
    (∂qH, ∂xH, ∂pH, ∂yH) = (zero(q), zero(x), zero(p), zero(y))
    dt = kwargs[:dt]
    R = promote_type(float(typeof(ω)), float(typeof(dt)))
    intType = TaoIntegrator{DiffEqBase.isinplace(prob),typeof(prob.f),cType,typeof(params),R}
    intType(l, prob.f, q0, p0,
            q, x, p, y,
            ∂qH, ∂xH, ∂pH, ∂yH,
            params, 0, zero(R),
            ω, dt,
           )
end

function DiffEqBase.__init(prob::AbstractDEProblem, ::Tao2016;
                           dt::Real, ω::Real=1.0, kw...
                          )
    _tao_init(prob; dt, ω, kw...)
end

function DiffEqBase.__solve(prob::AbstractDEProblem, alg::Tao2016;
                            dt::Real,
                            l::Integer=2,
                            ω::Real=1.0,
                           )
    tspan = prob.tspan
    ts = collect(tspan[1]:dt:tspan[2])
    n = length(ts)
    int = _tao_init(prob; dt, l, ω)
    qps = Vector{typeof(prob.u0)}(undef, n)
    qps[1] = ArrayPartition((copy(int.q0), copy(int.p0)))
    for j ∈ 1:(n-1)
        step!(int)
        qps[j+1] = ArrayPartition(copy(int.q), copy(int.p))
    end
    DiffEqBase.build_solution(prob, alg, ts, qps, calculate_error=false)
end
