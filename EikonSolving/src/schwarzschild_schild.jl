
@kwdef struct SchwarzschildSchild{T<:Number} <: Spacetime{T}
    G::T
    m::T
end

basetype(::Type{<:SchwarzschildSchild}) = SchwarzschildSchild

function _l̂(x::AbstractVector)
    r = radiusfromcart(x)
    @SVector [x[1]/r, x[2]/r, x[3]/r, -1]
end

_η(::Type{T}) where {T} = @SMatrix T[1 0 0 0
                                     0 1 0 0
                                     0 0 1 0
                                     0 0 0 -1]

function metric(M::SchwarzschildSchild{T1}, x::AbstractVector{T2}) where {T1,T2}
    T = promote_type(T1, T2)
    r = radiusfromcart(x)
    A = 2*M.G*M.m/r
    η = _η(T)
    l̂ = _l̂(x)
    η + A * (l̂ .* l̂')
end

function invmetric(M::SchwarzschildSchild{T1}, x::AbstractVector{T2}) where {T1,T2}
    T = promote_type(T1, T2)
    r = radiusfromcart(x)
    A = 2*M.G*M.m/r
    η = _η(T)
    l̂ = _l̂(x)
    η - A * (l̂ .* l̂')
end

conserved_E(M::SchwarzschildSchild, x::AbstractVector, p::AbstractVector) = -p[4]

for f ∈ (:conserved_E,)
    @eval function $f(sol::Solution{<:SchwarzschildSchild}, s::Real)
        (x, p) = evalsol(sol, s)
        $f(sol.spacetime, x, p)
    end
end
