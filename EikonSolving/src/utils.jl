

radiusfromcart²(x::AbstractVector) = x[1]^2 + x[2]^2 + x[3]^2
radiusfromcart(x::AbstractVector) = √(radiusfromcart²(x))

# requires up-index
angularmomentumx(x::AbstractVector, p::AbstractVector) = x[2]*p[3] - x[3]*p[2]
angularmomentumy(x::AbstractVector, p::AbstractVector) = x[3]*p[1] - x[1]*p[3]
angularmomentumz(x::AbstractVector, p::AbstractVector) = x[1]*p[2] - x[2]*p[1]
