
@kwdef struct SchwarzschildIsotropic{T<:Number} <: Spacetime{T}
    G::T
    m::T
end

basetype(::Type{<:SchwarzschildIsotropic}) = SchwarzschildIsotropic

_schwarzrad(M::SchwarzschildIsotropic) = M.G*M.m/2

function metric(M::SchwarzschildIsotropic{T1}, x::AbstractVector{T2}) where {T1,T2}
    T = promote_type(T1, T2)
    ρs = _schwarzrad(M)
    ρ = radiusfromcart(x)
    A = 1 - ρs/ρ
    B = 1 + ρs/ρ
    C = B^4
    @SMatrix T[C 0 0 0
               0 C 0 0
               0 0 C 0
               0 0 0 -(A/B)^2
              ]
end

function invmetric(M::SchwarzschildIsotropic{T1}, x::AbstractVector{T2}) where {T1,T2}
    T = promote_type(T1, T2)
    ρs = _schwarzrad(M)
    ρ = radiusfromcart(x)
    A = 1 - ρs/ρ
    B = 1 + ρs/ρ
    C = B^(-4)
    @SMatrix T[C 0 0 0
               0 C 0 0
               0 0 C 0
               0 0 0 -(B/A)^2
              ]
end

#WARN: this is confusing because it assumes the momentum vector is normed
conserved_E(M::SchwarzschildIsotropic, x::AbstractVector, p::AbstractVector) = -p[4]

for ax ∈ (:x, :y, :z)
    fname = Symbol("conserved_L", ax)
    Lname = Symbol("angularmomentum", ax)
    @eval $fname(M::SchwarzschildIsotropic, x::AbstractVector, p::AbstractVector) = $Lname(x, p)
end

for f ∈ (:conserved_E, :conserved_Lx, :conserved_Ly, :conserved_Lz, :conserved_L)
    @eval function $f(sol::Solution{<:SchwarzschildIsotropic}, s::Real)
        (x, p) = sol(s)
        $f(sol.spacetime, x, p)
    end
end
