
@kwdef struct SchwarzschildPolar{T<:Number} <: Spacetime{T}
    G::T
    m::T
end

basetype(::Type{<:SchwarzschildPolar}) = SchwarzschildPolar

_factor(M::SchwarzschildPolar, x::AbstractVector) = (1 - (2*M.G*M.m)/x[1])

function metric(M::SchwarzschildPolar{T1}, x::AbstractVector{T2}) where {T1,T2}
    T = promote_type(T1, T2)
    A = _factor(M, x)
    @SMatrix T[1/A 0 0 0 
               0 x[1]^2 0 0
               0 0 (x[1]*sin(x[2]))^2 0
               0 0 0 -A
              ]
end

function invmetric(M::SchwarzschildPolar{T1}, x::AbstractVector{T2}) where {T1,T2}
    T = promote_type(T1, T2)
    A = _factor(M, x)
    @SMatrix T[A 0 0 0
               0 x[1]^(-2) 0 0
               0 0 (x[1]*sin(x[2]))^(-2) 0
               0 0 0 -(1/A)
              ]
end

conserved_E(M::SchwarzschildPolar, x::AbstractVector, p::AbstractVector) = -p[4]

conserved_Lz(M::SchwarzschildPolar, x::AbstractVector, p::AbstractVector) = p[3]

for f ∈ (:conserved_E, :conserved_Lz)
    @eval function $f(sol::Solution{<:SchwarzschildPolar}, s::Real)
        (x, p) = sol(s)
        $f(sol.spacetime, x, p)
    end
end


function _schwarz_circ_pt(M::SchwarzschildPolar, r::Real, rs::Real=2*M.G*M.m)
    rs = 2*M.G*M.m
    √((1 + rs/(2*r))/(1 - rs/r))
end

#TODO: probably should do all this stuff exactly for a ∼ r_s

"""
    newtonian_circular_Loverm

Compute the angular momentum per unit mass for a circular orbit at radius `r` in the Newtonian regime
(high angular momentum).
"""
newtonian_circular_Loverm(M::SchwarzschildPolar, r::Real) = √(M.G*M.m/r^3)

"""
    initcircular(M::SchwarzschildPolar, r::Real; ϕ₀=0, t₀=0)

Create initial data for a circular orbit at radius `r`.  Only valid for ``\\theta = \\pi/2``.
Note this returns the coordinate position and the _covariant_ momentum.
"""
function initcircular(M::SchwarzschildPolar, r::Real;
                      ϕ₀::Real=0.0,
                      t₀::Real=0.0,
                     )
    # note this is not valid at other θ because of θ̇
    x = @SVector Float64[r, π/2, ϕ₀, t₀]
    rs = 2*M.G*M.m
    pt = _schwarz_circ_pt(M, r, rs)
    ϕdot = newtonian_circular_Loverm(M, r)
    p = @SVector Float64[0, 0, ϕdot, rs]  # CONTRA
    (x, metric(M, x)*p)
end

