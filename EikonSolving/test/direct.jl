using LinearAlgebra, Random, Statistics, Distributions, StaticArrays, FillArrays, BenchmarkTools, ConcreteStructs, Functors
using LuxCUDA, CUDA, Lux, Optimisers, ParameterSchedulers
using ForwardDiff
using ChainRulesCore, ChainRules, ChainRulesTestUtils
using Infiltrator
using GLMakie
using TensorBoardLogger, LoggingExtras
import Zygote

using FastSymplecticIntegrators; const FSI = FastSymplecticIntegrators
using FastSymplecticIntegrators: Tao

using EikonSolving.Pinn
using EikonSolving.Pinn: DirectTrainer, makebatches
using EikonSolving.Pinn: train!

const dev = gpu_device()

const m = 1.0f0
const k = 1.0f0

hoH(q, p) = (p⋅p)/(2*m) + (k/2)*(q⋅q)

ho∂qH(q, p) = k*q
ho∂pH(q, p) = p/m

function ho_make_sample(rng::AbstractRNG, ::Val{n}=Val(2)) where {n}
    q0 = SVector{n,Float32}(ntuple(j -> 2*rand(Float32) - 1.0f0, Val(n)))
    p0 = SVector{n,Float32}(ntuple(j -> 2*rand(Float32) - 1.0f0, Val(n)))
    (q0, p0)
end


src1() = quote
    rng = Xoshiro(999)

    int = Tao{4}(ho∂qH, ho∂pH; ω=1.0f0)

    model = Pinn.defaultpinn(2, 128, 4, gelu)

    #opt = Adam(0.01f0)
    _steps = [fill(1000, 2); fill(100, 8)]
    opt = Scheduler(Adam, eta=Step(start=0.01f0, decay=0.9f0, step_sizes=_steps))

    logger = TBLogger("_data/test", tb_overwrite)
    trainer = DirectTrainer{2}(
        dev, Xoshiro(999),
        ho_make_sample, int, range(0.0f0, Float32(2*π), step=0.01f0),
        model, opt;
        logger,
        n_batch_samples=2048,
        batch_iter=10,
        log_interval=10,
        log_model_interval=100,
    )
end

dotrain() = quote
    Pinn.train!(trainer, 100)
end

src2() = quote
    t = collect(reshape(trainer.temporal_range, 1, length(trainer.temporal_range)))
    q = repeat(Float32[1, 1], 1, length(trainer.temporal_range))
    p = repeat(Float32[0, 0], 1, length(trainer.temporal_range))
    batch = Pinn.PropagatorBatch(t, q, p) |> dev
    (y, _) = Pinn.onbatch(model, batch, (trainer.model_θ, trainer.model_ψ))
    y1 = Vector(y.q[1,:])
    y2 = Vector(y.q[2,:])
end


