using LinearAlgebra, Random, Statistics, Distributions, StaticArrays, FillArrays, BenchmarkTools, ConcreteStructs, Functors
using LuxCUDA, CUDA, Lux, Optimisers, ParameterSchedulers
using ForwardDiff
using ChainRulesCore, ChainRules, ChainRulesTestUtils
using Infiltrator
using GLMakie
import Zygote

using EikonSolving.Pinn
using EikonSolving.Pinn: PropagatorBatch, PhaseSpaceBatch
using EikonSolving.Pinn: timediff
using EikonSolving.Pinn: HamTrainBatch, HamiltonianLoss, HamiltonianLossParams
using EikonSolving.Pinn: boundaryloss, residualloss, withresidualloss, conservationloss
using EikonSolving.Pinn: SympLinearSubLayer, SympLinearLayer, SympActivationLayer

const dev = gpu_device()

#====================================================================================================
Notes:

Trains to very good accuracy without temporal weights if doing only fixed boundary conditions.
====================================================================================================#


includet("hamiltonians.jl")


#====================================================================================================
Validation: these are for checking known solution
====================================================================================================#
function testbatch_arrays(l::Integer, q0::Real=1.0f0, q1::Real=0.5f0, p0::Real=0.0f0, p1::Real=0.0f0)
    q = repeat(Float32[q0,q1], 1, l)
    p = repeat(Float32[p0,p1], 1, l)
    (q, p)
end
function testbatch(l::Integer, n::Integer, t0::Real, tf::Real, args...; M::Integer=1)
    (q, p) = testbatch_arrays(l, args...)
    HamTrainBatch((q, p), range(t0, tf, length=n), M)
end

#TODO: for now this is only for p₀ = 0
function ho((t,q0,p0), θ, ψ)
    q = q0 .* vcat(cos.(t), cos.(t))
    p = q0 .* vcat(-sin.(t), -sin.(t))
    ((q, p), ψ)
end
#===================================================================================================#


function _make_sample(n::Integer=256)
    dist = Uniform(-1.0f0, 1.0f0)
    convert(Matrix{Float32}, rand(dist, 2, n))
end
make_sample(n::Integer=256) = (_make_sample(n), _make_sample(n))


# could be interesting to pursue but not really working
function sympnet1(n::Integer)
    l1 = SympLinearLayer(2, 4)
    l2 = SympActivationLayer{:lower}(2)
    l3 = SympLinearLayer(2, 4)
    l4 = SympActivationLayer{:upper}(2)
    l5 = SympLinearLayer(2, 4)
    l6 = SympActivationLayer{:lower}(2)
    l7 = SympLinearLayer(2, 4)
    l8 = SympActivationLayer{:upper}(2)
    l9 = SympLinearLayer(2, 4)
    out = WrappedFunction(tqp -> (tqp[2], tqp[3]))
    Chain(l1, l2, l3, l4, l5, l6, l7, l8, l9, out)
end


src1() = quote
    #model = Pinn.modifiedpinn(2, 256, 4, gelu, gelu)
    model = Pinn.defaultpinn(2, 512, 4, gelu, device=dev, σ=4.0f0)
    #model = sympnet1(2)
    (θ, ψ) = Lux.setup(Xoshiro(999), model) |> dev

    # grad clipping is *essential*...
    # you need it to train for a long time on very small losses
    opt = OptimiserChain(ClipGrad(10.0f0), Adam(0.001f0))
    #opt = OptimiserChain(ClipGrad(1.0f0), Scheduler(Adam, Exp(0.01f0, 0.8f0)))  #TODO: haven't tried this yet
    #opt = Adam(0.001f0)
    #opt = Descent(0.001f0)
    opts = Optimisers.setup(opt, θ)

    #(H, ∂qH, ∂pH) = pendulum1(1.0f0, 1.0f0)
    (H, ∂qH, ∂pH) = ho2(1.0f0, 1.0f0)
    params = HamiltonianLossParams(α=0.9f0, ϵ=2.0f0, weight_update_interval=1000, M=8, log_interval=10)
    ℓ = HamiltonianLoss{1}(H, ∂qH, ∂pH, params, opts)

    hb = HamTrainBatch(make_sample(1024), range(0.0f0, Float32(2*π), length=32), 8) |> dev
    #hb = HamTrainBatch(testbatch_arrays(2), range(0.0f0, Float32(2*π), length=128), 4) |> dev
end

src2() = quote
    for j ∈ 1:20
        #Optimisers.adjust!(ℓ.opt_state, ParameterSchedulers.next!(sch))
        local hb = HamTrainBatch(make_sample(1024), range(0.0f0, Float32(2*π), length=32), 8) |> dev
        @info("starting new batch...")
        #local hb = HamTrainBatch(testbatch_arrays(2), range(0.0f0, Float32(2*π), length=128), 4) |> dev
        Pinn.train!(ℓ, model, hb, (θ, ψ), 100)
    end
end

src3() = quote
    hb = testbatch(1, 128, 0.0f0, Float32(2*π), 0.8f0, 0.3f0; M=1) |> dev
    t = cpu_device()(hb.batch.t'[:,1])
    l = ℓ(model, hb, (θ, ψ))
    (y, _) = Pinn.onbatch(model, hb.batch, (θ, ψ))
    y1 = Vector(y.q[1,:])
    y2 = Vector(y.q[2,:])
end


wtf1() = quote
    l1 = boundaryloss(ℓ, model, hb, (θ, ψ))
    (qp, l2) = withresidualloss(ℓ, model, hb, (θ, ψ))
    l3 = conservationloss(ℓ, model, hb, (θ, ψ))

    batch = hb.batch

    t = hb.batch.t
    q = hb.batch.q
    p = hb.batch.p

    fuck = Zygote.withgradient(θ) do ϑ
        #boundaryloss(ℓ, model, hb, (ϑ, ψ))
        #residualloss(ℓ, model, hb, (ϑ, ψ))
        conservationloss(ℓ, model, hb, (ϑ, ψ))
    end
end


ho_reference_test() = quote
    l1 = boundaryloss(ℓ, ho, hb, (θ, ψ))
    (qp, l2) = withresidualloss(ℓ, ho, hb, (θ, ψ))
    l3 = conservationloss(ℓ, ho, hb, (θ, ψ))
end


wtf2() = quote
    W = -ones(1,1) |> cu

    q = ones(1,4) |> cu
    p = ones(1,4) |> cu

    nn = (q0, W) -> (W .* W)*q0
    𝒻 = (q, p) -> 2*sum(q) + 3*sum(p)

    wtf = Zygote.gradient(W) do w
        h = Pinn.oncols(𝒻, Val(2), nn(q, w), nn(p, w))
        sum(h)
    end
end
