using LinearAlgebra, Random, Statistics, Distributions, StaticArrays, FillArrays, BenchmarkTools, ConcreteStructs, Functors
using LuxCUDA, CUDA, Lux, Optimisers
using ForwardDiff
using ChainRulesCore, ChainRules, ChainRulesTestUtils
using Infiltrator
using GLMakie
import Zygote

using EikonSolving.Pinn
using EikonSolving.Pinn: PropagatorBatch, PhaseSpaceBatch
using EikonSolving.Pinn: timediff
using EikonSolving.Pinn: HamTrainBatch, HamiltonianLoss, HamiltonianLossParams
using EikonSolving.Pinn: boundaryloss, residualloss, withresidualloss, conservationloss
using EikonSolving.Pinn: SympLinearSubLayer, SympLinearLayer, SympActivationLayer

const dev = gpu_device()


const m = 1.0f0
const k = 1.0f0

# GPU friendly column-wise inner product
inner(q1::AbstractMatrix, q2::AbstractMatrix) = sum(q1 .* q2, dims=1)

H(q, p) = inner(p, p)/(2*m) + (k/2)*inner(q, q)

∂qH(q, p) = k*q
∂pH(q, p) = p/m


function testbatch_arrays(l::Integer, q0::Real=1.0f0, q1::Real=0.5f0, p0::Real=0.0f0, p1::Real=0.0f0)
    q = repeat(Float32[q0,q1], 1, l)
    p = repeat(Float32[p0,p1], 1, l)
    (q, p)
end
function testbatch(l::Integer, n::Integer, t0::Real, tf::Real, args...)
    (q, p) = testbatch_arrays(l, args...)
    t = Matrix(range(t0, tf, length=l)')
    (t, q, p)
end



src1() = quote
    (t, q, p) = testbatch(8, 4, 0.0f0, 1.0f0) |> dev

    l1_1 = SympLinearSubLayer{:lower}(2)
    l1_2 = SympLinearSubLayer{:upper}(2)
    l1 = SympLinearLayer(2, l1_1, l1_2)

    l1 = SympLinearLayer(2, 3)

    l2 = SympActivationLayer{:upper}(2, Dense(1,2))

    model = Chain(l1, l2)

    (θ, ψ) = Lux.setup(Xoshiro(999), model) |> dev
end

src2() = quote
    ((t′, q′, p′), _) = model((t,q,p), θ, ψ)
end

src3() = quote
    l1 = Scale(4, use_bias=false)
    l2 = ReshapeLayer((2, 2))
    ls = Chain(l1, l2)
    (θ, ψ) = Lux.setup(Xoshiro(999), ls) |> dev

    t1 = Float32[1 100 1000 10000] |> dev
    (A, _) = ls(t1, θ, ψ)
end
