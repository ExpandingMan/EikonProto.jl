#====================================================================================================
       simple_train

This file is for testing the architecture (i.e. defaultpinn) by training it to fit some simple
functions.

seems to work fine!
====================================================================================================#
using LinearAlgebra, Random, StaticArrays, Distributions, Optimisers
using Lux, LuxCUDA, MLFlowClient
using Zygote
using GLMakie, ProgressMeter
using Infiltrator

using EikonSolving.Pinn
using EikonSolving.Pinn: DiagFactorMatrix, RandomFourierLayer, DiagFactorLayer, defaultpinn

const gdev = gpu_device()

const rng = Random.default_rng()
Random.seed!(rng, 999)

function mlflowserver()
    cmd = Cmd(`poetry run mlflow ui`, dir=joinpath(homedir(),"src","pyenv"))
    #WARN: can't kill this right now because poetry detaches it... annoying
    run(cmd, wait=false)
end


mlflow() = quote
    mlf_server = mlflowserver()
    mlf = MLFlow("http://localhost:5000")
    mlf_exp_id = createexperiment(mlf, name="go fuck yourself")
    mlfexprun = createrun(mlf, mlf_exp_id)
end

function makeplot((X, y), ŷ)
    ŷ = cpu_device()(ŷ)
    (X, y) = cpu_device()((X,y))
    fig = Figure()
    ax = Axis(fig[1,1], xlabel="x", ylabel="y")
    lines!(ax, X[1,:], x -> evalpoly(x, (0, -2, 1)), linewidth=3, color=:blue)
    scatter!(ax, X[1,:], y[1,:], markersize=12, alpha=0.5, color=:orange, strokewidth=2)
    scatter!(ax, X[1,:], ŷ[1,:], markersize=12, alpha=0.5, color=:green, strokewidth=2)
    fig
end

function makedata(rng::AbstractRNG=rng)
    X = reshape(collect(range(-2.0f0, 2.0f0, 128)), (1, 128))
    y = evalpoly.(X, ((0, -2, 1),)) .+ randn(rng, (1, 128)) .* 0.1f0
    return (X, y)
end


function loss(model, θ, ψ, (X, y))
    (ŷ, ψ) = Lux.apply(model, X, θ, ψ)
    mean(abs2, ŷ .- y)
end


function train(rng::AbstractRNG, model, (X,y), nepochs::Integer;
               gpu=gpu_device(),
               opt::AbstractRule=Adam(0.03f0),
              )
    (X, y) = gpu((X, y))
    (θ, ψ) = Lux.setup(rng, model) |> gpu
    optψ = Optimisers.setup(opt, θ)
    @showprogress for j ∈ 1:nepochs
        (∇ℓ,) = gradient(θ) do θ
            loss(model, θ, ψ, (X, y))
        end
        (optψ, θ) = Optimisers.update!(optψ, θ, ∇ℓ)
    end
    (θ, ψ)
end


src() = quote
    Random.seed!(rng, 999)

    #model = Chain(Dense(1=>16, tanh), Dense(16=>1))
    model = defaultpinn(1, 1, 32, 3)

    (X, y) = makedata(rng)

    (θ, ψ) = train(rng, model, (X,y), 250)

    (ŷ, _) = Lux.apply(model, gdev(X), θ, ψ)
end



