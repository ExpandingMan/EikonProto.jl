using KernVecFunctions



refho_inner(q1::AbstractMatrix, q2::AbstractMatrix) = sum(q1 .* q2, dims=1)

refho(q, p) = refho_inner(p, p)/2 + refho_innerinner(q, q)/2


function ho2(m::Real, k::Real)
    H(q, p) = (p⋅p)/(2*m) + (k/2)*(q⋅q)
    ∂qH(q, p) = k*q
    ∂pH(q, p) = p/m
    f1 = KernVecFuncWithGrad{(2,2)}(H, (∂qH, ∂pH))
    (f1, ∂qH, ∂pH)
end

function pendulum1(m::Real, l::Real)
    H(q, p) = (p⋅p)/(2*m) - l*cos(sum(q))
    ∂qH(q, p) = l*sin.(q)
    ∂pH(q, p) = p/m
    f1 = KernVecFuncWithGrad{(2,2)}(H, (∂qH, ∂pH))
    (f1, ∂qH, ∂pH)
end
