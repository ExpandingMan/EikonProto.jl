#====================================================================================================
Results Summary:

Thanks to KernelAbstractions, implementing this with trivial parallelism is super easy.
HO test shows that per-thread operations with StaticArrays are 5 to 10 times slower than combining
simple kernels.  Per-thread static arrays are 10 to 12 times faster on GPU than CPU.
Of course, there is no obvious fast alternative for more complicated functions such as this
Schwarzschild hamiltonian, so basically I'd say this is viable.  It would be incredibly difficult
to get something that complicated up to the performance of the simple kernels we can use in HO
no matter what, so performance trade-off is reasonable for
the generalization power it gives you.  And no, you don't need gigantic arrays to get these benefits,
(in fact for small arrays I beat the canned kernel versions I think because of allocations).
====================================================================================================#
using LinearAlgebra, BenchmarkTools, KernelAbstractions, StaticArrays
using ChainRulesCore, Zygote
using CUDA

const TEST_LENGTH = 10^4

const G = 1.0f0
const m = 1.0f0
const k = 1.0f0

refinner(q1::AbstractMatrix, q2::AbstractMatrix) = sum(q1 .* q2, dims=1)

function refH!(h, q, p)
    h .= refinner(p, p)/(2*m) .+ (k/2)*refinner(q, q)
end

refH(q, p) = refinner(p, p)/(2*m) .+ (k/2)*refinner(q, q)


function invmetric(x::AbstractVector)
    A = 1 - 2*G*m/x[1]
    @SMatrix eltype(x)[
        A 0 0 0
        0 x[1]^(-2) 0 0
        0 0 (x[1]*sin(x[2]))^(-2) 0
        0 0 0 -(1/A)
    ]
end


H(x, p) = p' * invmetric(x) * p ./ 2

ho(q, p) = (p⋅p)/(2*m) + (k/2)*(q⋅q)

@kernel function bycols_kernel!(𝒻, n::Val, hs::AbstractMatrix, qs::AbstractMatrix, ps::AbstractMatrix)
    j = @index(Global)
    q = SVector(ntuple(i -> qs[i,j], n))
    p = SVector(ntuple(i -> ps[i,j], n))
    h = 𝒻(q, p)
    @inbounds for i ∈ 1:size(hs,1)
        hs[i,j] = h[i]
    end
end

@kernel function rbycols_kernel!(𝒻, n::Val, hs::AbstractMatrix, qs::AbstractMatrix, ps::AbstractMatrix)
    j = @index(Global)
    q = SVector(ntuple(i -> qs[i,j], n))
    p = SVector(ntuple(i -> ps[i,j], n))
    hs[1,j] = 𝒻(q, p)
end

function bycols!(𝒻, n::Val, hs::AbstractMatrix, qs::AbstractMatrix, ps::AbstractMatrix)
    if !(size(hs,2) == size(qs,2) && size(qs,2) == size(ps,2))
        throw(ArgumentError("wrong array sizes"))
    end
    l = size(qs,2)
    backend = get_backend(hs)
    kernel! = bycols_kernel!(backend, 256)
    kernel!(𝒻, n, hs, qs, ps; ndrange=(l,))
    hs
end

function rbycols!(𝒻, n::Val, hs::AbstractMatrix, qs::AbstractMatrix, ps::AbstractMatrix)
    if !(size(hs,2) == size(qs,2) && size(qs,2) == size(ps,2))
        throw(ArgumentError("wrong array sizes"))
    end
    l = size(qs,2)
    backend = get_backend(hs)
    kernel! = rbycols_kernel!(backend, 256)
    kernel!(𝒻, n, hs, qs, ps; ndrange=size(hs))
    hs
end

function bycols(𝒻, n::Val, qs::AbstractMatrix, ps::AbstractMatrix)
    hs = similar(qs)
    bycols!(𝒻, n, hs, qs, ps)
end

function rbycols(𝒻, n::Val, qs::AbstractMatrix, ps::AbstractMatrix)
    hs = similar(qs, 1, size(qs,2))
    rbycols!(𝒻, n, hs, qs, ps)
end

#====================================================================================================
See map example

ChainRules/src/rulesets/base/base.jl 243
====================================================================================================#


#FUCK: have to re-do this stuff

function _inner_rbycols_pullback((j, _, hhob), dy, dqs, dps)
    #TODO: use bycols
    #(_, qq, pp) = hhob(sum(dy[:, j]))
    #dqs[:,j] .= qq
    #dps[:,j] .= pp
    nothing
end

function ChainRulesCore.rrule(cfg::RuleConfig{>:HasReverseMode}, ::typeof(rbycols), 𝒻,
        n::Val, qs::AbstractMatrix, ps::AbstractMatrix,
    )
    l = size(qs,2)
    hob = map(1:l) do j
        tuple(j, rrule_via_ad(cfg, 𝒻, view(qs,:,j), view(ps,:,j))...)
    end
    y = reshape(map(first, hob), 1, :)
    function rbycols_pullback(dy_raw)
        dy = unthunk(dy_raw)
        dqs = similar(qs)
        dps = similar(ps)
        _inner_rbycols_pullback.(hob, (dy,), (dqs,), (dps,))
        (NoTangent(), NoTangent(), NoTangent(), dqs, dps)
    end
    function rbycols_pullback(::AbstractZero)
        (NoTangent(), NoTangent(), NoTangent(), Returns(NoTangent()), Returns(NoTangent()))
    end
    (y, rbycols_pullback)
end


function _test_arrays(N::Integer, m::Integer=1)
    qs = randn(Float32, 4, N)
    ps = randn(Float32, 4, N)
    hs = fill(NaN32, m, N)
    (hs, qs, ps)
end


function bycols_gpu_benchmark(N::Integer=TEST_LENGTH)
    (hs, qs, ps) = _test_arrays(N, 4) |> cu

    @benchmark CUDA.@sync bycols!((a, b) -> a + b, $(Val(4)), $hs, $qs, $ps)
end

function ref_bycols_gpu_benchmark(N::Integer=TEST_LENGTH)
    (hs, qs, ps) = _test_arrays(N, 4) |> cu

    @benchmark CUDA.@sync begin
        $hs .= $qs + $ps
    end
end

function rbycols_gpu_benchmark(N::Integer=TEST_LENGTH)
    (hs, qs, ps) = _test_arrays(N) |> cu

    @benchmark CUDA.@sync oncols!(H, $hs, $qs, $ps)
end

function ref_gpu_benchmark(N::Integer=TEST_LENGTH)
    (hs, qs, ps) = _test_arrays(N) |> cu

    @benchmark CUDA.@sync refH!($hs, $qs, $ps)
end

function ho_gpu_benchmark(N::Integer=TEST_LENGTH)
    (hs, qs, ps) = _test_arrays(N) |> cu

    @benchmark CUDA.@sync oncols!(ho, $hs, $qs, $ps)
end

function cpu_benchmark(N::Integer=TEST_LENGTH)
    (hs, qs, ps) = _test_arrays(N)

    @benchmark oncols!(H, $hs, $qs, $ps)
end

function ho_cpu_benchmark(N::Integer=TEST_LENGTH)
    (hs, qs, ps) = _test_arrays(N)

    @benchmark oncols!(ho, $hs, $qs, $ps)
end


function grad_benchmark_1(N::Integer=TEST_LENGTH)
    (hs, qs, ps) = _test_arrays(N)
    cfg = Zygote.ZygoteRuleConfig()

    @benchmark begin
        (y, back) = rrule_via_ad($cfg, oncols, H, $(Val(2)), $qs, $ps)
        # this doesn't make sense but is just for testing performance
        #back(y)
    end
end

function ref_grad_benchmark_1(N::Integer=TEST_LENGTH)
    (hs, qs, ps) = _test_arrays(N)
    cfg = Zygote.ZygoteRuleConfig()

    @benchmark begin
        (y, back) = rrule_via_ad($cfg, refH, $qs, $ps)
        # this doesn't make sense but is just for testing performance
        back(y)
    end
end


src1() = quote
    q = randn(4,8) |> cu
    p = randn(4,8) |> cu

    #h = rbycols(H, Val(4), q, p)
    h = bycols((x, y) -> x, Val(4), q, p)
end


src2() = quote
    cfg = Zygote.ZygoteRuleConfig()

    q = 2*ones(2,4) |> cu
    p = 3*ones(2,4) |> cu

    𝒻 = (q, p) -> 2*sum(q .* q) + 3*sum(p .* p)

    h = oncols(𝒻, Val(2), q, p)

    #(y, back) = rrule_via_ad(cfg, 𝒻, q[:,2], p[:,2])
    (y, back) = rrule_via_ad(cfg, oncols, 𝒻, Val(2), q, p)

    @code_warntype rrule_via_ad(cfg, oncols, 𝒻, Val(2), q, p)

    dy = ones(1,4) |> cu
    wtf = back(dy)
end


