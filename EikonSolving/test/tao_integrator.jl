using Test

using EikonSolving.HamiltonianDiffEq
using EikonSolving.HamiltonianDiffEq: energy, energies
using DifferentialEquations, ModelingToolkit, StaticArrays
using LinearAlgebra, Random, Statistics

Random.seed!(999)



function hamtests(sys::HamiltonianSystem, q0, p0,
                  tspan=(0.0, 100.0);  # we want this to always be "a long time" for a good test
                  dt::Real=0.01,
                  atol::Real=0.005,
                 )
    prob = DynamicalODEProblem(sys, q0, p0, tspan)
    sol = solve(prob, Tao2016(); dt)
    sol = HamiltonianSolution(sys, sol)
    Es = energies(sol)
    E0 = first(Es)
    @test mean(Es) ≈ E0 atol=atol
    @test std(Es) ≈ 0.0 atol=atol
end


@testset "oscillators" begin
    @testset "harmonic_2d" begin
        sys = HamiltonianSystem{true}(2, (m=1.0, k=1.0)) do q, p, t, params
            m = params.m
            k = params.k
            p⋅p/(2*m) + k*q⋅q/2
        end

        q0s = [4*randn(2) for j ∈ 1:10]
        p0s = [4*randn(2) for j ∈ 1:10]

        for (q0, p0) ∈ zip(q0s, p0s)
            hamtests(sys, q0, p0)
        end
    end

    @testset "harmonic_10d" begin
        sys = HamiltonianSystem{true}(10, (m=1.0, k=1.0)) do q, p, t, params
            m = params.m
            k = params.k
            p⋅p/(2*m) + k*q⋅q/2
        end

        q0s = [4*randn(10) for j ∈ 1:10]
        p0s = [4*randn(10) for j ∈ 1:10]

        for (q0, p0) ∈ zip(q0s, p0s)
            hamtests(sys, q0, p0)
        end
    end

    @testset "pendulum_1d" begin
        sys = HamiltonianSystem{true}(1, (m=1.0, g=1.0, ℓ=1.0)) do q, p, t, params
            (m, g, ℓ) = (params.m, params.g, params.ℓ)
            m*ℓ^2*p[1]^2 + m*g*sin(q[1])
        end

        q0s = [mod.(2*randn(1), 2π) for j ∈ 1:10]
        p0s = [2*randn(1) for j ∈ 1:10]

        for (q0, p0) ∈ zip(q0s, p0s)
            hamtests(sys, q0, p0)
        end
    end
end
