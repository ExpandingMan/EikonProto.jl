using LinearAlgebra, Statistics, StaticArrays, FillArrays, BenchmarkTools, ConcreteStructs, Functors
using LuxCUDA, CUDA, Lux
using ForwardDiff
using ChainRulesCore, ChainRules, ChainRulesTestUtils
using Infiltrator
import Zygote

using EikonSolving.Pinn
using EikonSolving.Pinn: timediff

const dev = gpu_device()


testfunc1(q, t) = q .* (t .* t) .+ 1
testfunc2(q, t) = q .* (t .* t .* t) .- 1
function testfunc3(q, t) 
    (testfunc1(q, t), NamedTuple())
end


src1() = quote
    q = fill(3.0f0, 1, 4) |> dev
    t = collect(range(1.0f0, 4.0f0, length=4))' |> dev

    (grad1,) = Zygote.gradient(q) do qq
        sum(timediff(τ -> testfunc1(qq, τ), t))
    end

    (grad2,) = Zygote.gradient(q) do qq
        sum(timediff(τ -> testfunc2(qq, τ), t))
    end

    o = timediff(τ -> testfunc3(q, τ), t)

    (grad3,) = Zygote.gradient(q) do qq
        sum(timediff(τ -> testfunc3(qq, τ), t)[1])
    end
end


zygraw() = quote
    (o, back) = Zygote.pullback(timediff, τ -> testfunc(q, τ), t)
end

zygraw1() = quote
    (o, back) = Zygote.pullback(Pinn._dualwithtimediff, τ -> testfunc(q, τ), t)
    y = ones(Float32, size(o)) |> dev
end
