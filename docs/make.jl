using EikonProto
using Documenter

DocMeta.setdocmeta!(EikonProto, :DocTestSetup, :(using EikonProto); recursive=true)

makedocs(;
    modules=[EikonProto],
    authors="ExpandingMan <savastio@protonmail.com> and contributors",
    repo="https://gitlab.com/ExpandingMan/EikonProto.jl/blob/{commit}{path}#{line}",
    sitename="EikonProto.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://ExpandingMan.gitlab.io/EikonProto.jl",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
