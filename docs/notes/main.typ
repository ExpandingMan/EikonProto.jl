#import "lib.typ": *
#show: doc => conf(
  title: [`EikonProto.jl` Notes],
  authors: "Mike Savastio",
  contents: true,
  page-numbers: "1",
  doc,
)

#set table(stroke: 0.04em)

#show link: l => underline(text(fill: blue, l))


= Relativistic Ray-Marching
For a manifold $M$ with $3+1$ metric signature, the ray-marching algorithm finds the point at which
a null ray intersects with a surface in $M$.  We assume that within $M$ we can define a finite set
of fixed timelike surfaces $Sigma_k$ (we will often drop the surface index for convenience).  The
goal is to find the point $q in M$ at which a geodesic to which a given null vector $lr(v^a |)_p$ is
tangent, intersects one of the surfaces if it exists.

While it seems intuitively obvious, the concept of distance to a surface on a Minkowskian manifold
is somewhat subtle.  The spacelike distance from a point to a timelike worldline is the _maximum_
distance along geodesics connecting the worldline to the point.  However, the spacelike distance
from a point to a a curve lying in a spacelike hypersurface is the _minimum_ distance along
geodesics connecting the point to the curve.  The distance from a point $p$ to a timelike surface
$Sigma$ is therefore a saddle point.

More precisely, consider a surface with embedding function $Sigma^mu : Sigma -> M$.  We will use
dotted indices to indicate surface coordinates.  By definition, the normal $n^a$ to the surface
$Sigma$ is spacelike at every point.  Let $z^mu (s \; y^dot(alpha))$ be the spacelike geodesic intersecting
the point $y^dot(alpha)$ such that $z^mu (0 \; y^dot(alpha)) = n^mu$ and
$ R_x = { y^dot(alpha) | exists s [z^mu (s \; y^dot(alpha)) = x^mu] } $
be the set of points on the surface intersecting the geodesic $z^mu (s)$ that meets the point $x^mu$.
We define the distance from $Sigma$ to $x^mu$ to be
$ Delta(Sigma \; x^mu) = inf { s in bb(R) | y^dot(alpha) in R_x and z^mu (s \; y^dot(alpha)) = x^mu } $
for the affine parameter $s$.

For a given surface $Sigma$ we can therefore define the (spacelike) distance to $Sigma$ everywhere
on $M$.  We define the scalar field
$ sigma_Sigma : M -> bb(R) $
with the added condition that $sigma_Sigma (p) < 0$ for points $p$ on the interior of
$Sigma$#footnote[For now we impose that it makes sense to define an interior as a condition on the
class of surfaces we are considering.].  By construction, we have $sigma_Sigma (p in Sigma) = 0$.
We define the normal vector field of $Sigma$ on $M$ by $nabla^a sigma$, which corresponds to the
usual notion of normal on the surface.  Note that $nabla^a sigma$ can be efficiently computed using
automatic differentiation.

== Formulating $sigma$ for specific surfaces
To make use of the ray-marching algorithm, we want to be able to easily come up with $sigma$ for
different surfaces.  In the comoving coordinates of the surface, we can easily generalize Euclidean
signed distance functions to Minkowski space and, in some cases (notably the sphere) this gives us a
convenient way of defining surfaces on curved spacetimes.

Let $y^mu (s)$ be the affinely parameterized worldline of a point within the surface such that
$sigma(y^mu) lt.eq 0 thick forall s$ such that the entire surface is stationary with respect to the
worldline (typically we will choose this to be the "center" of the surface).  Given a point with
coordinates $x^mu$, we define $s_(*x)$ to be the parameter for which the spacelike distance from
$x^mu$ is maximized#footnote[That is, the maximum spacelike distance from $x^mu$ to $y^mu (s)$.
This is guaranteed to exist, while the minimum does not, since we can pick geodesics between $x^mu$
and $y^mu (s)$ with arbitrarily small arc lengths as we approach a null ray.], and we write
$hat(y)^mu = y^mu (s_(*x))$ and $hat(u)^mu = lr((d y^mu)/(d s)|_(s_(*x)))$.  The comoving
coordinates of $y^mu (s)$ at $hat(y)^mu$ are then coordinates in which we can most simply formulate
$sigma$.  For example, for a sphere on Minkowski space, we have
$ sigma(x^mu) = (x^i - y^i)^2 - R $
in the comoving coordinates of the sphere, from which we can obtain $sigma$ in any other coordinates
via boosts.  Note that our procedure is valid even for non-inertial surfaces.

In the general case, the above procuedure is only an approximation because it relies on the
worldline $y^mu (s)$ rather than a full parameterization of the surface.  This is a good
approximation for spacetimes which are stationary on scales comparable with the size of the surface,
and exact in many cases of interest.  The exact alternative would require us not merely to find
$op(y^mu)(s)$ but the saddle point from which we measure the distance to $x^mu$, which is likely to
be considerably more difficult in most cases.  Regardless, this is a mere implementation detail,
$sigma$ exists and is a scalar field in any case, we may simply choose to approximate it.

Finding $hat(y)^mu$ in the general case is likely going to be challenging.  It might be a good use
case for hybrid symbolic/numeric root-finding methods such as described in the `Symbolics.jl`
documentaiton.

== #TODO[step size]


== Defining solids in curved spacetime
There are many surfaces which are common in rendering and game engines that have no obvious analog
in curved spacetime.  

First, we should address whether it is always possible to, given a surface on a Euclidean
$3$-manifold, define an analogous surface on a Minkowskian $4$-manifold.  For our purposes, it
suffices to start with a worldline $w : RR -> cal(M)$, then, using the ADM formalism,
we can use the worldline to define a family of spacelike hypersurfaces orthogonal to the velocity
vector of $w$, each of which we can then map to Euclidean space.  Therefore, given a surface in
Euclidean $3$-space we can uniquely define an analogous surface in Minkowskian $4$-space.

Some surfaces have simple definitions that can be trivially extended to any
manifold.  For example, a sphere on any Riemannian manifold $cal(R)$ can be defined as the locus of
points at a proper distance $r$ from a point $p in cal(R)$.  This can be extended to a Minkowskian
manifold $cal(M)$ by defining it as the locus of points at spacelike distance $r$ from a timelike
worldline $w : RR -> cal(M)$.  Some surfaces such as ellipsoids have definitions that can be
similarly extended.  These definitions do not depend on the curvature on $cal(M)$.

Other surfaces, such as polytopes, do not have obvious extensions to curved spacetime.  It seems
natural to extend polytopes to curved spaces by setting the degree and angles at their vertices,
since each vertex can be defined on a single tangent tangent space $T_p cal(M)$, however, polytopes
defined this way will not, in general have vertices connected by geodesics.  Similarly, a polytope
constructed from geodesics on curved spacetime will not in general have the same vertex properties
as their Euclidean analogs.  Therefore, to define a polytope in curved spacetime, some arbitrary
choice must be made.

The situation is somewhat different on asymptotically flat spacetimes.  In these spacetimes, there
is a class of surfaces the (timelike) worldlines of which escape to infinity in either the past or
the future.  Therefore, we can specify these surfaces by initial conditions on flat Minkowski space.
This in turn allows us to define any "common" surface (in particular all polytopes) by mapping it to
a local subset of the manifold near infinity, then letting it fall inward.  When defining surfaces
this way, we must consider the internal dynamics of the solid object we are defining.  The simplest
option for a polytope would be to allow each vertex to follow a geodesic as it falls inward, though
of course this will result in a "distorted" shape.  This is the limit of an object with no rigidity.
The opposite limit is a perfectly rigid object.  In such an object, the internal forces would have
to exactly cancel gravitational tidal forces as it falls inward.  We can do this using using the
geodesic deviation equation.  Given a 1-parameter family of geodesics $gamma_(s)(tau)$ the geodesic
deviation equation is
$ (Dif^2 S^mu)/(dif tau^2) = lr(R^mu)_(nu rho sigma) T^nu T^rho S^sigma $
where $T^mu = (diff_tau)^mu$, $S^mu = (diff_s)^mu$.  The internal force between neighboring
elements of a solid is then given by $S^mu$, with appropriate initial conditions.
#footnote[#TODO[I'll have to work this out more carefully.  Pretty sure this is basically
true, but I'm not completely confident that $S^mu = f^mu$ exactly, or whether there is more
subtlety.]]




= Differential geometry in software
Preserving the diffeomorphism symmetry in a software representation of a manifold $M$ can be
challenging.  Suppose we represent some $p in M$ by the array `[0,0,0,0]`.  Clearly, under a simple
translation, we must represent this by some other array that is not identically zero.  However, in
so doing, we sacrifice our ability to use any coordinate system since we have fixed the origin at
$p$. Clearly this is a problem for every non-trivial diffeomorphism, not merely translations. This
suggests we use the type system of the language to wrap the array, but this does not immediately
solve the problem.

Suppose we have a type
```julia
struct CartesianPoint
  data::SVector{4,Float64}
end
```
This allows our program to understand a certain class of coordinate systems (in this case
Cartesian), however, as we well know, even the group relating Cartesian coordinate systems is not
compact.  We could simply ignore the problem, and say it is up to the user to keep track of which
coordinate system each `CartesianPoint` is in, but this has some disturbing implications.  In
particular it becomes impossible _even in principle_ to compare two `CartesianPoint` objects: that
is, since `p = CartesianPoint([0,0,0,0])` does not fully specify its coordinate system, it is
impossible to say whether `q = CartesianPoint([0,0,0,0])` is the same point, or a different point in
a different coordinate system.

I have not found what I'd consider a fully satisfactory solution to this problem, and I believe that
one doesn't exist because comparing coordinate systems at compile time is either impractical or
downright impossible.  What I have come up with is an API that tries to prevent operations from
mixing objects with different representations, while checks that can only be performed at run-time,
such as comparison of the tangent space for different tensor objects and the parameters of
continuous symmetry groups, are left to the user.#footnote[Again, there simply seems to be no
alternative here.  It's prohibitively expensive to always perform those checks at run-time.]

== API rules
In this section we are referring to the _public_ API of the differential geometry portion of
`EikonProto.jl` which will ultimately need to be spun off into a different package (perhaps
`DiffGeo.jl`) than the rendering code.  Therefore, at time of writing, `EikonProto.jl` technically
violates these rules because the rendering code shares a namespace with the differential geometry
code.  Nevertheless, I try to satisfy the rules I describe here by obeying the ownership rules I am
about to describe.

Points and tensors in different coordinate systems are *not allowed* to exist in the same scope.
Technically, it is impossible to enforce this in Julia, so, more precisely what we mean is that all
differential geometry functions in the public API *must* return one of the following
- `nothing`
- A true scalar (invariant) real or complex number of `Number` type.
- A point in the _same_ coordinate system as the function arguments.
- A tensor in the _same_ coordinate system as the function arguments.

To achieve this, `Diffeomorphism` objects return wrapper objects which internally represent objects
in the new coordinates, but will transform back to the original coordinates when used with unwrapped
objects.  Scalar functions of wrapped objects of the same `Diffeomorphism` type can operate on each
other efficiently by internally using the unwrapped objects in the new coordinates.  As with the
tangent space, it is up to the user to ensure that the coordinate systems match exactly since their
group is non-compact and it's not realistic to do this at compile time in the general case.

Code utilizing unwrapped objects directly can be implemented using inner function calls, e.g. with
the `do` block syntax.  This is slightly dangerous since Julia's syntax allows objects from the
enclosing scope to be used in anonymous functions, but as far as I know it's not possible to be more
strict without some extremely laborious and unwieldy macro shenanigans.

== Rejected solution: coordinate hashes
An intriguing possibility that I rejected is to associate all coordinate systems with a hash.  For
example, a program can start by describing things in a coordinate system with an ID of `0x00` and
diffeomorphisms, when acting on these, will output objects with a new coordinate system that has a
hash depending on the diffeomorphismand the original coordinate hash.  While I thought this idea was
interesting enough to write down, there are some major problems with it:
- Hashing arbitrary diffeomorphisms will be quite tricky, we will have to somehow round or
  approximate it to get consistent hashing, and unless that is done extremely carefully, we will
  either get lots of slightly inequivalent systems when we did not intend to, or we will exacerbate
  floating point error by further coarse graining our coordinates (or some unholy combination of
  both of these).
- GPU's: not only will we spend clock cycles on hashing, which probalby wouldn't be all that
  efficient on the GPU under ideal circumstances, but we will have to use (at least) 64-bit integers
  for the hash.
- It doesn't seem realistic that this could be type stable.  We would surely need to compute some
  hashes at run-time, since we are going to sometimes need to compute the diffeomorphisms at
  run-time.  This problem is not Julia-specific, in a static language we probably wouldn't be able
  to do it at all.


= Some strange facts about dS
Following @Cot_escu_2017 we write the 10 conserved quantities in dS as
$ E, quad L_i, quad K_i, quad R_i $
$E$ and $L_i$ are the familiar energy and angular momentum, but $R_i$ and $K_i$, which collectively
take the place of the conserved quantities associated with spatial translations and boosts in
Minkowski space, are here called conjugate momenta.  The reason this is strange is that, while
in Minkowski space the vector field tangent to a geodesic can be described _only_ in terms of the
energy $E$ and momentum $P^i$, in de Sitter space it depends on the 7 total degrees of freedom of
$E$ and both conjugate momenta $K_i$ and $R_i$.

In static coordinates with metric
$ dif s^2 = 1/(x^2 + alpha^2) [ -dif t^2 + (delta_(i j) - (x^i
  x^j)/(x^2+alpha^2)) dif x^i dif x^j ]
$
the timelike and null geodesics are given by
$ x^i (t) = 1/E (K^i cosh(t / alpha) - R^i sinh(t / alpha)) $
The fact tht _both_ $K^i$ and $R^i$ terms are time-dependent is notable, since the
equivalent in Minkowski space is $x^i (t) = 1/E (K^i + P^i t)$.  This means that the coordinate
velocity $(d x^i)/(d t)$, which in Minkowski space depends _only_ on the momentum, in de Sitter
space depends on all 6 componenets of the conjugate momenta $K^i$ and $R^i$. Operationally, this
means that solving the geodesic equation in de Sitter space necessarily requires full boundary
conditions on the worldline itself, whereas in Minkowski space we have a class of non-intersecting
worldlines for each momentum $P^i$.

== Notes on geodesics in dS
- The highly symmetric nature of dS is obscured in the static coordinates, where it doesn't afford
  as many simplifications as one might expect.
- Spacelike geodesics in static and special static coordinates do _not_ have the same form as null
  or timelike geodesics, and we still need to compute them.
- As hinted at above, the boundary conditions on the geodesics are tricky to deal with.  You can use
  $p^a nabla_a (p_b p^b) = 0$ for geodesics to help relate the initial momenta in the forward and
  backward cases, but identifying the components of $p^a$ in the curve $op(x^mu)(t)$ can be slightly
  tricky and requires care.


= Solving Geodesic Equations
The action for a geodesic $gamma = op(x^mu)(s)$ is
$ S = integral_gamma dif s sqrt(g_(mu nu) dot(x)^mu dot(x)^nu) $
where $dot(x)^mu = (d x^mu)/(d s)$.  By sacrificing reparameterization invariance we can re-cast
this with the quadratic Lagrangian
$ S = 1/2 integral_gamma dif s med g_(mu nu) dot(x)^mu dot(x)^nu $<geod-quadr-act>
The canonical momentum is
$ p_mu = (partial L)/(partial dot(x)^mu) = g_(mu nu) dot(x)^nu = dot(x)_mu $
The Hamiltonian for this action is
$ H = p_mu dot(x)^mu - L = 1/2 g^(mu nu)p_mu p_nu $
Note that we have explicitly written $H$ in terms of the canonical momentum $p_mu$ which is _not_
the same thing as the contravariant momentum $p^mu$.  Therefore, to express $H$ in terms of $p_mu$,
we must write it in terms of the _inverse_ metric $g^(mu nu)$ in contrast to @geod-quadr-act and the
definition of $L$.

The first of Hamilton's equations gives us the expected definition of $p_mu$
$ dot(x)^mu = (partial H)/(partial p_mu) = p^mu $
while the second gives us
$ dot(p)_mu = dot.double(x)_mu &= \
  -(partial H)/(partial x^mu) &= - 1/2 diff_mu g^(alpha beta) p_alpha p_beta
$
By definition, only $g_(mu nu)$, not $p_mu$ depends on the canonical position $x^mu$.  The following
are then equivalent
$ - dot(p)_mu = 1/2 p_alpha p_beta diff_mu g^(alpha beta) = 1/2 diff_mu p^2 $
The right hand side $1/2 diff_mu p^2$ should be handled with great care as the dependence on the
canonical position $x^mu$ is made even more implicit, but it is useful to remember as a reminder
that it is not necessary to compute explicitly the rank 3 tensor $diff_mu g^(alpha beta)$.  To put
it another way, $p^2$ is a function of _both_ the canonical momentum $p_mu$ and the canonical
position $x^mu$.

== Numerical integration
Empirically I find that geodesic equations in black hole metrics are easy to solve numerically with
standard integration techniques.  However, doing this naively can result in significant errors in
conserved quantities.  This is most concerning for the invariant mass as photons will start to go
off-shell.  The standard approach to dealing with this is to utilize a so-called
#link("https://en.wikipedia.org/wiki/Symplectic_integrator")[symplectic integrator].  Unfortunately,
as far as I can tell nearly all symplectic integration algorithms assume that the Hamiltonian is
"separable", i.e. can be expressed as $H(x,p) = T(p) + V(x)$, which is of course not the case for
geodesics.

As of writing, the most straightforward and general method I can find for non-separable
Hamiltonian's is @Tao_2016.  The basic idea in this paper is as follows.  Given the original
Hamiltonian $H(q,p)$ define
$ bar(H)(q,p,x,y) := H(q,y) + H(x,p) + omega K(q,p,x,y) $<tao-hamiltonian>
where $omega in RR$ and
$ K(q,p,x,y) := norm(q - x)^2/2 + norm(p - y)^2/2 $
This effectively simulates a split Hamiltonian with the addition of a constraint term that sets $q =
x$, $p = y$.  The constraint term can be efficiently integrated due to the linearity of its
gradient.  The method is symplectic essentially because it consists of a series symplectic Euler
steps for each of its terms.  The result is an explicit integrator because the terms of
@tao-hamiltonian constructed so as to cause the implicit parts to drop out.


== Surrogate models<sec-surrogate>
Using the solution found by symplectic integration directly is impractical due to the computational
cost.  While simple polynomial splines are adequate for creating a surrogate model for a geodesic
solution with specific boundary conditions, we require a higher degree of generalization because we
consider it a design requirement that we cannot know a priori the boundary conditions of every
geodesic we must compute (i.e. a user should be able to choose a scene to render arbitrarily).  This
makes our interpolation task much more difficult.

There does not seem to be an enormous literature on surrogate models.  Most of it focuses on
modeling 1-dimensional functions and much of it involves likelihood fitting of Gaussian processes.
While there may be a way of applying this in our case, as of writing I cannot see a straightforward
generalization of any random process fitting methods to our case and likely developing a useful
technique would require extensive research.

More extensive and useful literature can be easily found on the subject of "physics-informed neural
networks" (PINN).  See @wang2023experts for an excellent and practically useful review.  These
techniques involve fitting neural network approximators to differential equations.  The basic
technique is straightforward and fairly obvious, but many special tricks are required to achieve
convergence to a good solution.  Of course, many of these tricks look quite different from the more
common techniques for training neural networks which are more focused on empirical loss
minimization.  More useful resources for addressing practical difficulties in training PINN's can be
found in @ref-jax-pi.

As of writing, there remain several open questions regarding the application of PINN's to our case.
For one thing, most of the literature discusses _solving_ equations directly rather than fitting a
surrogate to our solutions from the symplectic integrator.  This is fine as long as the solution is
nearly as good as the one obtained by traditional integration.  This may well be achievable, however
the literature mostly does not seem to address the kinds of strict symmetry requirements we will
have (e.g. keeping photons on-shell and maintaining the constants of motion).  We can of course add
symmetries to the loss function, but this may not be as simple as it appears, since objective term
weighting as a major issue for PINN's.

#TODO[will of course have a lot more to say after first attempts]


= Notes on Coordinates

== Polar Patches<polar-patches>
We will always want our polar coordinates to have two patches, rotated by $pi/2$ relative to each
other as to maximize the angular separation between singularities in each coordinate
system.#footnote[Even this isn't a real solution as it doesn't remove the singularity at $r=0$.]  To
implement this, we must compute how to transform betwee patches.  Both coordinate systems cover the
entire sphere with the exception of the two points at their poles.  To do this, we use the group
equation
$ op(R_z)(phi) op(R_y)(theta) = op(R_(z'))(phi') op(R_(y'))(theta') op(R_n)(psi) $
Where $op(R_n)(psi)$ is the rotation into the new axes.

To maximize the angular separation, we need only permute axes.  We choose the new polar axis to be
along the original $y$ axis, so that we exchange $x -> z$ and $z -> -x$ and $op(R_n)(psi) =
op(R_y)(pi slash 2)$.  We can then write
$ op(R_z')(-phi) op(R_y')(theta - pi/2) = op(R_z')(phi') op(R_y')(theta') $

It is simplest to work in the $"SL"(2,bb(C))$ representation of $"SO"(3)$.  Applying the above to a
spinor $vec(1,0)$ one obtains
$ e^(i phi'/2) cos(theta'/2) &= zeta_1 \
  -e^(-i phi'/2) sin(theta'/2) &= zeta_2
$<polar-coord-change>
where $zeta_1$ and $zeta_2$ are the spinor components obtained from the LHS.  One can compute
$phi'$ and $theta'$ by computing $zeta_1,zeta_2$ numerically and solving @polar-coord-change, being
very careful of branch cuts.

Since we define the polar angle $theta in [0, pi]$#footnote[In software we will change this to
$theta in [-pi/2, pi/2]$ so that the "origin" is not on the singularity, but I risk confusing the
crap out of myself if I do that here.], it is convenient to solve for $theta'$ using $arccos$.  To
this end, by squaring and subtracting @polar-coord-change, we have
$ cos(theta') = |zeta_1|^2 - |zeta_2|^2 $
Solving for $phi'$ is slightly trickier because there are so many ways to do it, and we have to be
very careful of branch cuts.  In my implementation I do this by dividing @polar-coord-change
$ e^(i phi) = -tan(theta'/2) zeta_1/zeta_2 $
One must be careful to choose the appropriate $log$ branch so that $phi in lr([0,
2pi))$#footnote[Similarly, in software this will be $phi in lr([-pi, pi))$ because it's inconvenient
to be so close to the branch cut, but again, I don't want to confuse myself by writing that
here.]

== Kerr-Schild
The Kerr metric can be written
$ dif s^2 = -dif t^2 + delta^(i j)dif x_i dif x_j + (2 G m r^3)/(r^4 + a^2 z^2)[dif t + hat(l)^i dif x_i]^2 $
or
$ g_(mu nu) = eta_(mu nu) +  l_mu l_nu $<kerr-schild-index>
and the inverse
$ g^(mu nu) = eta^(mu nu) - l^mu l^nu $
where $l^mu$ is a null vector given by
#let _denom = $r^2 + a^2$
$ l^mu := sqrt((2 G m r^3)/(r^4 + a^2 z^2)) hat(l)^mu wide wide
  hat(l)^mu := vec(-1, (r x + a y)/#_denom, (r y - a x)/#_denom, z/r)
$
and $r$ is defined by
$ z^2/r^2 + (x^2 + y^2)/(r^2 + a^2) = 1 $
where $a = J slash M$ is the angular momentum per unit mass.  Note that, rather confusingly, the
indices on $l^mu$ are being raised and lowered with $eta_(mu nu)$, however note that $l^mu$ is null
with respect to _both_ $g_(mu nu)$ and $eta_(mu nu)$.

The timelike Killing vector in these coordinates is simply $(1,0,0,0)$, while the azimuthal Killing
vector is $(0,0,y,-x)$.

A convenient orthonormal basis using these coordinates is found in @Maluf_2023.  Denoting the
Minkowski space tetrads as $lr(E^a)_mu$ (in Cartesian coordinates this is merely a fancy notation
for the identity matrix) we choose
$ lr(e^a)_mu = lr(E^a)_mu + 1/2 l^a l_mu $
where
$ l^a = lr(E^a)_mu l^mu $
(note $l^a l_a=0$) with the inverse
$ lr(e_a)^mu = lr(E_a)^mu - 1/2 l_a l^mu $


=== Kerr-Schild in Schwarzschild
In the Scwarzschild limit $a -> 0$ we have $k^i = x^i/r$ so that
$ dif s^2 = -dif t^2 + delta^(i j) dif x_i dif x_j + (2 G m)/r^2 [r dif t + x^i dif x_i]^2 $
This has identical form to @kerr-schild-index with
$ l^mu = sqrt((2 G m)/r) vec(-1, x/r, y/r, z/r) $
which is of course the same expression as for the Kerr spacetime with $a -> 0$.

From this it follows that the tetrads have the same form as in the Kerr case.

This is simply the cartesian form of the Eddington-Finkelstein coordinates and therefore extends all
the way to the singularity at $r=0$.

== Isotropic Schwarzschild
The polar coordinate singularity is very annoying to deal with numerically, so we always try to find
Cartesian coordinates.  Even though the Schwarzschild coordinates are incomplete, it would be nice
to convert them to cartesian.  To that end
$ dif s^2 = -(1 - (2 G m)/r)dif t^2 + (1 - (2 G m)/r)^(-1) ((dif r)/(dif rho))^2 dif rho^2
  + r^2 dif Omega^2
$
As conditions for isotropic coordinates, we require that
$ r^2 = A(rho) rho^2 \
  (1 - (2 G m)/r)^(-1) ((dif r)/(dif rho))^2 = A(rho)
$
Eliminating $A(rho)$ and solving, we find
$ r = rho (1 + (G m)/(2 rho))^2 $
We can then write the metric as
$ dif s^2 = - (1 - (2 G m)/r) dif t^2 + (1 + (G m)/rho)^4 (dif rho^2 + rho^2 dif Omega^2) $

Note that the horizon is at $rho_s := G m slash 2$.  We can write the metric more succinctly as
$ dif s^2 = - ((1 - rho_s slash rho)/(1 + rho_s slash rho))^2 dif t^2
  + (1 + rho_s/rho)^4 (dif rho^2 + rho^2 dif Omega^2)
$
In terms of cartesian coordinates (which we will write simply as $x$, hoping this won't lead to
confusion) $rho^2 = x_1^2 + x_2^2 + x_3^2$
$ dif s^2 = - ((1 - rho_s slash rho)/(1 + rho_s slash rho))^2 dif t^2
  + (1 + rho_s/rho)^4 delta^(i j) dif x_i dif x_j
$

== Kruskal-Szekeres
The metric is
$ dif s^2 = (4 r_s^3)/r e^(-r/r_s) (-dif T^2 + dif X^2) + r^2 dif Omega^2 $
where the $r$ is the $r$ of Schwarzschild fame.  It is related to $X$ and $T$ via
$ (r/r_s - 1) e^(-r/r_s) = X^2 - T^2 $

#TODO[Are isotropic Kruskal-Szekeres of any use?  Finding them is a big pain in the ass because we
have $dif X^2$ instead of $dif r^2$.]


= Notes on Colors
Computing display color values from physical radiance can be quite confusing.  The main reason I am
including this section is that I have repeatedly gotten myself confused over different RGB color
spaces, in particular sRGB, the linear CIE RGB colors, or something else, as many references simply
say "RGB" without being specific (usually they mean sRGB).

One of the most useful color spaces is described by the
#link("https://en.wikipedia.org/wiki/CIE_1931_color_space")[CIE 1931 XYZ] standard.  The coordinates
of this space are denoted $X,Y,Z$ and can be computed by
$ C = 1/N integral_0^oo dif omega med L(omega) med overline(c)(omega) $<color-match>
where $(C, overline(c)) in {(X, overline(x)), (Y, overline(y)), (Z, overline(z))}$, $L(omega)$ is
the spectral radiance and $N$ is a normalization constant.  The $overline(c)$ functions are called
"color matching" functions.  In this case they are positive definite and well approximated by a sum
of "asymmetric Gaussians" (see the wiki page for more detail).  Most references give @color-match in
terms of functions of the wavelength $lambda = (2 pi)/omega$ (we absorb the change of variable into
the definitions of the color matching functions).  This standard also defines RGB colors, which are
essentially the same thing with different color matching functions, but these are not commonly used.

We will sometimes encounter the so-called xyY coordinates where $c = C/(X+Y+Z)$ where $(c, C) in
{(x, X), (y, Y)}$.  Confusingly, some references like to pretend that $Y$ is merely the overall
"brightness" and cannot effect the perceived color, which is of course completely false (a large $Y$
value unsurprisingly looks green).  The inverse relations will also be useful
$ X = (Y x)/y #h(4em)
  Z = Y/y (1 - x - y)
$

As far as I can tell, the normalization of XYZ colors is basically arbitrary.  In software,
normalization is usually chosen such that the brightest white that can be shown by the display
hardware corresponds approximately to $"XYZ"(1,1,1)$.  The value can be checked precisely by
converting from $"sRGB"(1,1,1)$ (see below for more details on sRGB).

Another important linear color space is #link("https://en.wikipedia.org/wiki/LMS_color_space")[LMS
color] which uses color matching functions that are matched to the response of human color filter
(cone) cells (LMS is for "long, medium, short").  The human cone cell filter functions are roughly
Gaussian, with peaks at, roughly #text(fill: red)[$217 "ceV"$], #text(fill: green)[$232 "ceV"$] and
#text(fill: blue)[$288 "ceV"$].

The most common color space used in software is #link("https://en.wikipedia.org/wiki/SRGB")[sRGB
color].  From a physics perspective, sRGB is bizarre.  In particular, it is not linear in the
spectral distribution $L(omega)$.  The utility of sRGB lies in the fact that its brightness is
bounded above by $"sRGB"(1,1,1)$, which is supposed to be the brightest wight that can be produced
by the display hardware.  As a consequence, sRGB does not form a vector space.  Converting from XYZ
to sRGB requires a linear transformation followed by a piecewise non-linear transformation.
Fortunately, $"RGB"(1,0,0) approx "LMS"(1,0,0)$ for some normalization of LMS, and likewise for
green and blue.  Most color software packages seem to normalize LMS to make this true.  The
existence of an upper limit of brightness in sRGB also means that it can have a low precision
representation in software. Usually this means 8-bit fixed point numbers, with the interval mapped
evenly $[0, 2^8 - 1] arrow.bar [0, 1]$.  Hence, in the often encountered hex notation, `0x00`
corresponds to $0$ and `0xff` corresponds to $1$.

We will sometimes make use of the following notation for color coordinates
$ "XYZ"[f] = 
  vec(upright(X)[f], upright(Y)[f], upright(Z)[f])
  = integral_0^oo dif omega med
  f(omega) vec(overline(x)(omega), overline(y)(omega), overline(z)(omega))
$
and likewise for other coordinates such as LMS.

The Julia package `Colors.jl` provides the `colormatch` function which can be used to determine the
$X, Y, Z$ coordinates of a monochromatic spectral distribution $L_0 delta(lambda - lambda_0)$ for
some arbitrary but standard normalization.  It should be possible to use this to compute an integral
over a spectrum.

== Computing Redshift
For convenience, we define the redshift parameter $r = omega_e / omega_o = z + 1$ where $omega_e$ is the
emitted energy and $omega_o$ is the observed energy.

Often we will be given either a known spectrum such as the black body spectrum, or have to infer a
spectrum from image data which is initially described with color coordinates such as XYZ.  We would
like to be able to elide the integration needed to compute spectrum redshift in color coordinates.
Unfortunately, given an XYZ color computed from a spectral distribution $L(omega)$, there is no
general way of computing the XYZ color of the redshifted distribution $L(r omega)$ without direct
integration.  In what follows, we will list some approximations for special cases.

=== Black Body Spectrum
The spectral radiance of a black body is given by
$ B(omega,T) = omega^3 / (4 pi^3) 1/(e^(omega/T) - 1) $
If an object is emitting radiation in its rest frame with the spectrum $B(omega_e, T)$, where
$omega_e$ is the rest frame energy, then the observed energy $omega_o$ at redshift $z=r-1$ will have
the distribution
$ L(omega_o) = omega_o^3 / (4 pi ^3 r^3) 1/(e^(omega_o/(r T)) - 1) =
  1 / r^3 B(omega_o, T/r)
$
That is, the redshifted black body distribution is simply the rest frame black body distribution at
temperature $T/r$ scaled by the constant $1/r^3$.  This is useful since if we can compute
$"XYZ"[B(dot,T)]$ for the rest frame, then we can easily compute it for any redshift by scaling the
temperature and the coordinates.

Fortunately, this is a well-studied problem (although the literature uses some rather bizarre
conventions that will force us to have to do some actual work).  The curve giving the color
coordinates of the black body spectrum as a function of $T$ is known as the
#link("https://en.wikipedia.org/wiki/Planckian_locus")[Planckian locus].  On this wikipedia page
there is a cubic spline (of $1/T$) parameterization of this in $x,y$ coordinates.  In computing XYZ
from xyY, _all three components_ $X, Y, Z$ are directly proportional to $Y$.  Because of this, and
the fact that the overall normalization is arbitrary, the $x, y$ coordinates alone are enough to
uniquely determine the color up to an overall brightness.#footnote[I was initially very confused by
this, since $Y$ clearly depends on $T$.  However, since when converting from xyY $X prop Y$ and $Z prop Y$,
the $T$ dependence of $Y$ cancels and by choosing it we are effectively choosing normalization.]


= Atlases of `EikonProto.jl`

== Minkowski Cartesian: `MC`
*Representation:* `MinkowskCartesian{n}`

$ dif s^2 = -dif t^2 + delta^(i j) dif x_i dif x_j $
Nice and simple, not much to say about it.

== Minkowski Spherical Holonomic: `MSH`
*Representation:* `MinkowskiSphericalHolonomic{n}`

$ dif s^2 = -dif t^2 + dif r^2 + r^2 dif theta^2 + r^2 cos^2(theta) dif phi^2 $
Minkowski with spherical spatial coordinates where the polar angle $theta in [-pi/2, pi/2]$ and the
azimuth $phi in lr([-pi, pi))$.  Note the strange looking $cos^2(theta)$ in the metric due to the
unorthodox polar range.  These angle conventions are chosen because the point at $(theta, phi) = (0,
0)$ is free from any of the usual pathologies such as singularities or branch cuts, which are
definitely things we'd like to avoid in software.

This atlas has 2 patches, which we will refer to primed and unprimed.  The prime patch is obtained
form the unprime patch by a rotation about the $y$ axis by $pi/2$, so that $y$ coordinates are
preserved.  The axes are related by
#figure(table(
  columns: 2,
  [Unprime], [Prime],
  $x$, $z'$,
  $y$, $y'$,
  $z$, $-x'$,
), caption: [Relation of axes in the `MSH` atlas.]
)

Switching between patches is a big pain in the ass, and worryingly inefficient.  It is best
implemented by solving group equations in $"SL"(2,bb(C))$ which is described in @polar-patches.
Because of this, it is recommended to switch patches only when getting "pretty close" to the
coordinate singularity along the $z$ (or $z'$) axis, that is within an angular separation of $pi/8$
or less.

== Minkowski Spherical Holonmic 1: `MSH1`, `MSO1` (*incomplete*)
*Representation:* `MinkowskiSphericalHolonomic1{n}`

These are the same coordinates as `MSH` but with only one patch.  The atlas is therefore incomplete
as technically it is missing the entire $z$ axis.  Not sure if we'll ever really need this, but it
has a much simpler implementation than `MSH` so it's a good test case.


== 2-Sphere Polar Holonomic 1: `S2PH1`, `S2PO1` (*incomplete*) (_Euclidean_)
*Representation:* `Sphere2PolarHolonomic1`

$ dif s^2 = dif theta^2 + cos^2(theta) dif phi^2 $
This is simply $S^2$ with a single coordinate patch, with our usual polar coordinate convention.
This is needed primarily for mapping images onto hypersurfaces of conformal infinity (i.e.
"background" images).


#bibliography(full: true, style: "american-physics-society", ("refs.bib", "refs.yml"))
